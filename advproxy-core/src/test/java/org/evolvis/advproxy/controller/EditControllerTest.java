package org.evolvis.advproxy.controller;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import javax.portlet.PortletException;
import javax.portlet.PortletModeException;
import javax.portlet.ReadOnlyException;
import javax.portlet.ValidatorException;
import javax.portlet.WindowStateException;

import org.evolvis.advproxy.core.R;
import org.evolvis.advproxy.portlet.ProxyPreferences;
import org.evolvis.advproxy.portlet.controller.EditController;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.mock.web.portlet.MockActionRequest;
import org.springframework.mock.web.portlet.MockActionResponse;
import org.springframework.mock.web.portlet.MockPortletPreferences;
import org.springframework.mock.web.portlet.MockRenderRequest;
import org.springframework.mock.web.portlet.MockRenderResponse;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
//import org.springframework.beans.TestBean;

@SuppressWarnings("all")
@RunWith(JMock.class)
public class EditControllerTest {
	
	private Mockery mockery;
	private EditController controller;
	MockPortletPreferences preferences;	
	private MockRenderRequest request;
	private MockRenderResponse response;
	
	private MockActionRequest actionRequest;
	private MockActionResponse actionResponse;
	private ModelMap model;
	private ProxyPreferences proxy = new ProxyPreferences();
//	TestBean tb = new TestBean(); to insert into BeanPropertyBindigResult
	private Errors errors = new BeanPropertyBindingResult(new Object(), "emptyObject"); //TODO ???
	
	//Test Data
	final private String TEST_HOMPAGE = "http://www.test.de";
	final private String TEST_ALLOWED_URLS = "http://cfe-wsrp-shoech.lan.tarent.de:8080/.*\r\nhttp://.*.org/.*";
	final private String TEST_RESOURCE_URLS = ".*/resource.jpg\r\n.*/test.png";
	final private String TEST_MIN_HEIGHT = "1000";
	final private String TEST_MAX_HEIGHT = "5000";
	final private String TEST_JAVASCRIPT_EXCLUDES = ".*/jsSnippetOfBody.js\r\n.*/jsSnippetOfHead.js";
	final private String TEST_COOKIENAME = "testCookieName";
	final private String TEST_CLASS_SUFFIX = "testClassSuffix";
	final private String TEST_PROXY_HOST = "testProxyHost";
	final private String TEST_PROXY_PORT= "testProxyPort";
	final private String LINKED_WINDOW_IDS = "advProxyTest";	
	
	
	@Before	
	public void setup() {
		mockery = new JUnit4Mockery();

		controller = new EditController();
		preferences = new MockPortletPreferences();
		request = new MockRenderRequest();
		response = new MockRenderResponse();
		model = new ModelMap();
		
	}
	
	@Ignore
	@Test
	public void testHandleEditController() throws PortletException, IOException {
//		
//		//Basic Options		
//		preferences.setValue(R.PREFS.HOMEPAGE, TEST_HOMPAGE);
////		preferences.setValues(R.PREFS.ALLOWED_URLS, AdvproxyUtils.splitLines(TEST_ALLOWED_URLS));		
//		preferences.setValue(R.PREFS.FORCE_NEW_WINDOW, Boolean.toString(true));		
//		preferences.setValues(R.PREFS.CHECK_RESOURCE_URLS, AdvproxyUtils.splitLines(TEST_RESOURCE_URLS));
//		
//		//User Interface
//		preferences.setValue(R.PREFS.MIN_HEIGHT, TEST_MIN_HEIGHT);		
//		preferences.setValue(R.PREFS.MAX_HEIGHT, TEST_MAX_HEIGHT);	
//		preferences.setValue(R.PREFS.SHOW_ADDRESSBAR, Boolean.toString(true));		
//		preferences.setValue(R.PREFS.IS_ADDRESS_EDITABLE, Boolean.toString(true));	
//		preferences.setValue(R.PREFS.SHOW_GO_HOME_BUTTON, Boolean.toString(true));
//		
//		//HTML
//		preferences.setValue(R.PREFS.INCLUDE_JAVASCRIPT, Boolean.toString(true));
//		preferences.setValues(R.PREFS.JAVA_SCRIPT_EXCLUDES, AdvproxyUtils.splitLines(TEST_JAVASCRIPT_EXCLUDES));
//		preferences.setValue(R.PREFS.GENERATE_ADDITIONAL_COOKIES, Boolean.toString(true));
//		preferences.setValue(R.PREFS.SEND_USER_UUID, Boolean.toString(true));
//		preferences.setValue(R.PREFS.USER_UUID_COOKIENAME, TEST_COOKIENAME);
//		preferences.setValue(R.PREFS.UPDATE_FORMULAR_FIELDS, Boolean.toString(true));
//		preferences.setValue(R.PREFS.CLASS_REWRITE_ENABLED, Boolean.toString(true));
//		preferences.setValue(R.PREFS.ID_REWRITE_ENABLED, Boolean.toString(true));
//		preferences.setValue(R.PREFS.CLASS_REWRITE_FORCED, Boolean.toString(true));
//		preferences.setValue(R.PREFS.CLASS_SUFFIX, TEST_CLASS_SUFFIX);
//		
//		//Misc
//		preferences.setValue(R.PREFS.USE_PROXY, Boolean.toString(true));
//		preferences.setValue(R.PREFS.PROXY_HOST, TEST_PROXY_HOST);
//		preferences.setValue(R.PREFS.PROXY_PORT, TEST_PROXY_PORT);  //TODO check cause of int
//		preferences.setValue(R.PREFS.USE_INTERPORTLET_COMMUNICTION, Boolean.toString(true));
////		preferences.setValue(R.PREFS.PORTLET_WINDOW_ID, proxy.getPortletWindowId());
//		preferences.setValues(R.PREFS.LINKED_WINDOW_IDS, AdvproxyUtils.splitLines(LINKED_WINDOW_IDS));
//		
//		//Template
////		preferences.setValue(R.PREFS.OPEN_URL_TEMPLATE_PARAMETER_NAME, proxy.getOpenUrlTemplate());
////		preferences.setValue(R.PREFS.FORM_POST_URL_TEMPLATE_PARAMETER_NAME, proxy.getFormPostUrlTemplate());		
//
//		controller.handleEditRenderRequest(request, response, preferences, model);	
//		ProxyPreferences proxy = (ProxyPreferences) model.get("proxypreferences");
//		
//		
//		assertEquals(TEST_HOMPAGE, proxy.getHomepage());	
////		assertEquals(TEST_ALLOWED_URLS, proxy.getAllowedURLs());
//		assertTrue(proxy.getForceNewWindowOnNotAllowedURLs());
////		assertEquals(TEST_RESOURCE_URLS, proxy.getCheckResourceUrls());
//		
//		assertEquals(TEST_MIN_HEIGHT, proxy.getMinHeight());
//		assertEquals(TEST_MAX_HEIGHT, proxy.getMaxHeight());
//		assertTrue(proxy.getShowAddressbar());
//		assertTrue(proxy.getAddressEditable());
//		assertTrue(proxy.getShowGoHomeButtonInAddressbar());
//
//		assertTrue(proxy.getIncludeJavascript());
////		assertEquals(TEST_JAVASCRIPT_EXCLUDES, proxy.getHomepage());
//		assertTrue(proxy.getGenerateCookies());
//		assertTrue(proxy.getEnableUserUUID());		
//		assertEquals(TEST_COOKIENAME, proxy.getUUIDCookieName());
//		assertTrue(proxy.getUpdateFormFields());	
//		assertTrue(proxy.getClassRewriteEnabled());	
//		assertTrue(proxy.getIdRewriteEnabled());	
//		assertTrue(proxy.getClassRewriteForced());	
//		assertEquals(TEST_CLASS_SUFFIX, proxy.getClassSuffix());
//		
//		assertTrue(proxy.getEnableProxy());		
//		assertEquals(TEST_PROXY_HOST, proxy.getProxyHost());
//		assertEquals(TEST_PROXY_PORT, proxy.getProxyPort());
//		assertTrue(proxy.getInterportletCommunication());	
//		//WindowId
////		assertEquals(LINKED_WINDOW_IDS, proxy.getLinkedWindowIds());
//		
//		
	}
	
	@Ignore
	@Test
	public void testSaveSettings() throws PortletException, IOException {
//		proxy.setHomepage(TEST_HOMPAGE);
//		
//		controller.saveNewSettings(proxy, errors, actionRequest, actionResponse, preferences);
//		
//		assertEquals(TEST_HOMPAGE, preferences.getValue(R.PREFS.HOMEPAGE, null));
	}
	
	
}
