/*
 * advproxy Advanced servlet and portlet proxy Copyright (c) 2011 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License,version 2 as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.evolvis.advproxy.core.filters;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.apache.xerces.xni.parser.XMLDocumentFilter;
import org.apache.xerces.xni.parser.XMLInputSource;
import org.cyberneko.html.HTMLConfiguration;
import org.evolvis.advproxy.core.R;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test for {@link HTMLStripper} based on the old behaviour and the new task
 * https://dev.tarent.de/pm/t_follow.php/7633 which requires support for xpath
 * wildcard expressions like <code>//html/body/*</code>
 * @author Christoph Jerolimov
 */
public class HTMLStripperTest {
	@Test(expected = NullPointerException.class)
	public void testNullConstructor() {
		// assert only old behaviour
		new HTMLStripper(null);
	}
	
	@Test
	public void testIllegalXPathExpression() {
		// assert only old behaviour
		new HTMLStripper("<test>");
	}
	
	@Test
	public void testCorrectXPathExpression() {
		// assert only old behaviour
		new HTMLStripper("//html/body");
	}
	
	@Test
	public void testOldResourceBehaviour1() throws IOException {
		InputStream inputIS = HTMLStripperTest.class.getResourceAsStream("/HTMLStripperTest.html");
		InputStream expectedOutputIS = HTMLStripperTest.class.getResourceAsStream("/HTMLStripperTest_output.html");
		try {
			String input = IOUtils.toString(inputIS, R.UTF_8);
//			System.out.println("=================");
//			System.out.println(input);
			String expectedOutput = IOUtils.toString(expectedOutputIS, R.UTF_8);
//			System.out.println("=================");
//			System.out.println(expectedOutput);
			String output = parse("//html/body/div@id=main/div@id=content", input);
//			System.out.println("=================");
//			System.out.println(output);
			Assert.assertEquals(expectedOutput, output);
		} finally {
			IOUtils.closeQuietly(inputIS);
			IOUtils.closeQuietly(expectedOutputIS);
		}
	}

	@Test
	public void testOldResourceBehaviour2() throws IOException {
		InputStream inputIS = HTMLStripperTest.class.getResourceAsStream("/HTMLStripperTest2.html");
		InputStream expectedOutputIS = HTMLStripperTest.class.getResourceAsStream("/HTMLStripperTest2_output.html");
		try {
			String input = IOUtils.toString(inputIS, R.UTF_8);
//			System.out.println("=================");
//			System.out.println(input);
			String expectedOutput = IOUtils.toString(expectedOutputIS, R.UTF_8);
//			System.out.println("=================");
//			System.out.println(expectedOutput);
			String output = parse("//html/body/div/div@id=content", input);
//			System.out.println("=================");
//			System.out.println(output);
			Assert.assertEquals(expectedOutput, output);
		} finally {
			IOUtils.closeQuietly(inputIS);
			IOUtils.closeQuietly(expectedOutputIS);
		}
	}

	@Test
	public void testIncludeWholeBodyWithBodyTag() throws IOException {
		InputStream inputIS = HTMLStripperTest.class.getResourceAsStream("include-body-with-body-tag_input.html");
		InputStream expectedOutputIS = HTMLStripperTest.class.getResourceAsStream("include-body-with-body-tag_output.html");
		try {
			String input = IOUtils.toString(inputIS, R.UTF_8);
//			System.out.println("=================");
//			System.out.println(input);
			String expectedOutput = IOUtils.toString(expectedOutputIS, R.UTF_8);
//			System.out.println("=================");
//			System.out.println(expectedOutput);
			String output = parse("//html/body", input);
//			System.out.println("=================");
//			System.out.println(output);
			Assert.assertEquals(expectedOutput, output);
		} finally {
			IOUtils.closeQuietly(inputIS);
			IOUtils.closeQuietly(expectedOutputIS);
		}
	}

	@Test
	public void testIncludeWholeBodyWithoutBodyTag() throws IOException {
		InputStream inputIS = HTMLStripperTest.class.getResourceAsStream("include-body-without-body-tag_input.html");
		InputStream expectedOutputIS = HTMLStripperTest.class.getResourceAsStream("include-body-without-body-tag_output.html");
		try {
			String input = IOUtils.toString(inputIS, R.UTF_8);
//			System.out.println("=================");
//			System.out.println(input);
			String expectedOutput = IOUtils.toString(expectedOutputIS, R.UTF_8);
//			System.out.println("=================");
//			System.out.println(expectedOutput);
			String output = parse("//html/body/*", input);
//			System.out.println("=================");
//			System.out.println(output);
			Assert.assertEquals(expectedOutput, output);
		} finally {
			IOUtils.closeQuietly(inputIS);
			IOUtils.closeQuietly(expectedOutputIS);
		}
	}

	@Test
	public void testIncludeWholeBodyWithBodyTagAndOnlyWirdcards() throws IOException {
		InputStream inputIS = HTMLStripperTest.class.getResourceAsStream("include-body-with-body-tag_input.html");
		InputStream expectedOutputIS = HTMLStripperTest.class.getResourceAsStream("include-body-with-body-tag_output.html");
		try {
			String input = IOUtils.toString(inputIS, R.UTF_8);
//			System.out.println("=================");
//			System.out.println(input);
			String expectedOutput = "<HEAD></HEAD>" + IOUtils.toString(expectedOutputIS, R.UTF_8);
//			System.out.println("=================");
//			System.out.println(expectedOutput);
			String output = parse("/*/*", input);
//			System.out.println("=================");
//			System.out.println(output);
			Assert.assertEquals(expectedOutput, output);
		} finally {
			IOUtils.closeQuietly(inputIS);
			IOUtils.closeQuietly(expectedOutputIS);
		}
	}

	@Test
	public void testIncludeWholeBodyWithoutBodyTagAndOnlyWildcards() throws IOException {
		InputStream inputIS = HTMLStripperTest.class.getResourceAsStream("include-body-without-body-tag_input.html");
		InputStream expectedOutputIS = HTMLStripperTest.class.getResourceAsStream("include-body-without-body-tag_output.html");
		try {
			String input = IOUtils.toString(inputIS, R.UTF_8);
//			System.out.println("=================");
//			System.out.println(input);
			String expectedOutput = IOUtils.toString(expectedOutputIS, R.UTF_8);
//			System.out.println("=================");
//			System.out.println(expectedOutput);
			String output = parse("//*/*/*", input);
//			System.out.println("=================");
//			System.out.println(output);
			Assert.assertEquals(expectedOutput, output);
		} finally {
			IOUtils.closeQuietly(inputIS);
			IOUtils.closeQuietly(expectedOutputIS);
		}
	}

	@Test
	public void testAssertedLinebreakAfterEndelement() throws IOException {
		InputStream inputIS = HTMLStripperTest.class.getResourceAsStream("assert-linebreak-after-endelement_input.html");
		InputStream expectedOutputIS = HTMLStripperTest.class.getResourceAsStream("assert-linebreak-after-endelement_output.html");
		try {
			String input = IOUtils.toString(inputIS, R.UTF_8);
//			System.out.println("=================");
//			System.out.println(input);
			String expectedOutput = IOUtils.toString(expectedOutputIS, R.UTF_8);
//			System.out.println("=================");
//			System.out.println(expectedOutput);
			String output = parse("//html/body/*", input);
//			System.out.println("=================");
//			System.out.println(output);
			Assert.assertEquals(expectedOutput, output);
		} finally {
			IOUtils.closeQuietly(inputIS);
			IOUtils.closeQuietly(expectedOutputIS);
		}
	}

	@Test
	public void testAssertedLinebreakAfterEndelement2() throws IOException {
		InputStream inputIS = HTMLStripperTest.class.getResourceAsStream("assert-linebreak-after-endelement_input2.html");
		InputStream expectedOutputIS = HTMLStripperTest.class.getResourceAsStream("assert-linebreak-after-endelement_output2.html");
		try {
			String input = IOUtils.toString(inputIS, R.UTF_8);
//			System.out.println("=================");
//			System.out.println(input);
			String expectedOutput = IOUtils.toString(expectedOutputIS, R.UTF_8);
//			System.out.println("=================");
//			System.out.println(expectedOutput);
			String output = parse("//html/body/*", input);
//			System.out.println("=================");
//			System.out.println(output);
			Assert.assertEquals(expectedOutput, output);
		} finally {
			IOUtils.closeQuietly(inputIS);
			IOUtils.closeQuietly(expectedOutputIS);
		}
	}
	
	@Test
	public void testIncorrectExpectedBodyContent() throws IOException {
		InputStream inputIS = HTMLStripperTest.class.getResourceAsStream("incorrect-expected-bodycontent_input.html");
		InputStream expectedOutputIS = HTMLStripperTest.class.getResourceAsStream("incorrect-expected-bodycontent_output.html");
		try {
			String input = IOUtils.toString(inputIS, R.UTF_8);
//			System.out.println("=================");
//			System.out.println(input);
			String expectedOutput = IOUtils.toString(expectedOutputIS, R.UTF_8);
//			System.out.println("=================");
//			System.out.println(expectedOutput);
			String output = parse("//html/body/*", input);
//			System.out.println("=================");
//			System.out.println(output);
			Assert.assertEquals(expectedOutput, output);
		} finally {
			IOUtils.closeQuietly(inputIS);
			IOUtils.closeQuietly(expectedOutputIS);
		}
	}
	
	public String parse(String xpath, String html) throws IOException {
		StringReader reader = new StringReader(html);
		StringWriter writer = new StringWriter();
		
		final HTMLConfiguration parser = new HTMLConfiguration();
		parser.setProperty("http://cyberneko.org/html/properties/filters", new XMLDocumentFilter[] {
				new HTMLStripper(xpath),
				new AdvWriter(writer, R.UTF_8)
		});
		
		final XMLInputSource source = new XMLInputSource(null, "http://advproxy.evolvis.org", null, reader, R.UTF_8);
		parser.parse(source);
		
		return writer.toString();
	}
}
