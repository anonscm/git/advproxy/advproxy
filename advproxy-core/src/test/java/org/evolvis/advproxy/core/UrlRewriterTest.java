/*
 * advproxy
 * Advanced servlet and portlet proxy
 * Copyright (c) 2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
package org.evolvis.advproxy.core;

import junit.framework.Assert;

import org.junit.Test;

/**
 * TODO doc
 * 
 * @author Christoph Jerolimov, Tim Steffens, tarent GmbH
 */
public class UrlRewriterTest {
	@Test
	public void testBaseURL() {
		{
			PortletUrlRewriter ur = new PortletUrlRewriter("http://www.evolvis.org");
			Assert.assertEquals(
					"http://www.evolvis.org", ur.getBaseURL());
		}
		{
			PortletUrlRewriter ur = new PortletUrlRewriter("http://www.evolvis.org/");
			Assert.assertEquals(
					"http://www.evolvis.org", ur.getBaseURL());
		}
		{
			PortletUrlRewriter ur = new PortletUrlRewriter("http://www.evolvis.org/project");
			Assert.assertEquals(
					"http://www.evolvis.org", ur.getBaseURL());
		}
		{
			PortletUrlRewriter ur = new PortletUrlRewriter("http://www.evolvis.org/project/");
			Assert.assertEquals(
					"http://www.evolvis.org/project", ur.getBaseURL());
		}
		{
			PortletUrlRewriter ur = new PortletUrlRewriter("http://www.evolvis.org/test#");
			Assert.assertEquals(
					"http://www.evolvis.org", ur.getBaseURL());
		}
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testIllegalBaseURL() {
		new PortletUrlRewriter("/");
	}
}
