package org.evolvis.advproxy.core;

/**
 * Defines if an request result type if text or binary.
 */
public enum RequestResultType {
	/** Text */
	TEXT,
	/** Binary "inline content" like a picture */
	BINARY,
	/** Binary attachment like a download */
	BINARY_ATTACHEMENT;
}
