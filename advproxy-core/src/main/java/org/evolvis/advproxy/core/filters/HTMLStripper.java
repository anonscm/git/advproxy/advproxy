/*
 * advproxy Advanced servlet and portlet proxy Copyright (c) 2008 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License,version 2 as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.evolvis.advproxy.core.filters;

import java.util.LinkedList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.util.XMLAttributesImpl;
import org.apache.xerces.xni.Augmentations;
import org.apache.xerces.xni.QName;
import org.apache.xerces.xni.XMLAttributes;
import org.apache.xerces.xni.XMLResourceIdentifier;
import org.apache.xerces.xni.XMLString;
import org.apache.xerces.xni.XNIException;
import org.apache.xerces.xni.parser.XMLDocumentFilter;
import org.cyberneko.html.filters.DefaultFilter;
import org.evolvis.advproxy.core.PortletUrlRewriter;
import org.evolvis.advproxy.core.R;

/**
 * This class use an xpath expression to extract and html part of an html page.
 * It works like all {@link XMLDocumentFilter} stream based and so, it is NOT
 * possible to support the whole xpath power here. A small selfmade xpath
 * implementation supports only //tag/tag with wildcards /html/body/ * /strong.
 * Attributes selectors /html@id=hallo are only supported for the id and class
 * attribute.
 */
public class HTMLStripper extends DefaultFilter {

	private static final String TAGNAME_WILDCARD = "*";
	
	protected static Log log = LogFactory.getLog(HTMLStripper.class.getName());

	protected boolean isInValidatedBlock = false;
	protected String xPathExpression;
	protected LinkedList<Event> expression;
	protected LinkedList<Event> eventStack;
	
	/**
	 * Task 7633: If the expression end with an wildcard we do search
	 * for xpath-1 expression and change this flag to do not include
	 * start and end tag!
	 */
	protected boolean includeStartAndEndTag = true;
	
	protected PortletUrlRewriter urlRewriter;
	
	/**
	 * A new xml filter with the given xpath filter.
	 * 
	 * @param xPathExpression
	 */
	public HTMLStripper(String xPathExpression) {
		if (log.isTraceEnabled())
			log.trace("New HTMLStripper instance was created");
		if (log.isDebugEnabled())
			log.debug("Using xPathExpression" + xPathExpression);

		this.xPathExpression = xPathExpression;
		this.eventStack = new LinkedList<Event>();
		this.expression = new LinkedList<Event>();

		String[] parts = xPathExpression.split("\\/");
		for (String part : parts) {
			if (part == null || part.length() == 0)
				continue;
			String name = part;
			XMLAttributes attributes = null;
			if (part.contains("@")) {
				name = part.substring(0, part.indexOf("@"));
				String attributeName = part.substring(part.indexOf("@") + 1);
				String attributeValue = "";
				if (attributeName.contains("=")) {
					attributeValue = attributeName.substring(attributeName.indexOf("=") + 1);
					attributeName = attributeName.substring(0, attributeName.indexOf("="));
				}
				attributes = new XMLAttributesImpl();
				attributes.addAttribute(new QName(null, attributeName, attributeName, null), R.HTML.CDATA, attributeValue);
			}
			expression.add(new Event(name, attributes));
		}
		
		// Task 7633: If the expression end with an wildcard we do search
		// for xpath-1 expression and change flag to false to do not include
		// start and end tag!
		if ( !expression.isEmpty() && expression.getLast().localName.equals(TAGNAME_WILDCARD)) {
			this.includeStartAndEndTag = false;
			expression.removeLast();
		}
	}
	
	/**
	 * Delegate the call only to the parent if the xpath validation flag
	 * {@link #isInValidatedBlock} is true.
	 */
	@Override
	public void comment(XMLString text, Augmentations augs) throws XNIException {
		if (isInValidatedBlock) {
			super.comment(text, augs);			
		}
	}
	
	/**
	 * Delegate the call only to the parent if the xpath validation flag
	 * {@link #isInValidatedBlock} is true.
	 */
	@Override
	public void characters(XMLString text, Augmentations augs)
			throws XNIException {
		if (isInValidatedBlock) {
			super.characters(text, augs);			
		}
	}
	
	/**
	 * Delegate the call only to the parent if the xpath validation flag
	 * {@link #isInValidatedBlock} is true.
	 */
	@Override
	public void emptyElement(QName element, XMLAttributes attributes,
			Augmentations augs) throws XNIException {
		if (isInValidatedBlock) {
			super.emptyElement(element, attributes, augs);			
		}
	}
	
	/**
	 * Delegate the call only to the parent if the xpath validation flag
	 * {@link #isInValidatedBlock} is true.
	 */
	@Override
	public void endCDATA(Augmentations augs) throws XNIException {
		if (isInValidatedBlock) {
			super.endCDATA(augs);			
		}
	}
	
	/**
	 * Delegate the call only to the parent if the xpath validation flag
	 * {@link #isInValidatedBlock} is true.
	 */
	@Override
	public void endGeneralEntity(String name, Augmentations augs)
			throws XNIException {
		if (isInValidatedBlock) {
			super.endGeneralEntity(name, augs);			
		}
	}
	
	/**
	 * Delegate the call only to the parent if the xpath validation flag
	 * {@link #isInValidatedBlock} is true.
	 */
	@Override
	public void ignorableWhitespace(XMLString text, Augmentations augs)
			throws XNIException {
		if (isInValidatedBlock) {
			super.ignorableWhitespace(text, augs);			
		}
	}
	
	/**
	 * Delegate the call only to the parent if the xpath validation flag
	 * {@link #isInValidatedBlock} is true.
	 */
	@Override
	public void startCDATA(Augmentations augs) throws XNIException {
		if (isInValidatedBlock) { 
			super.startCDATA(augs);
		}
	}
	
	/**
	 * Delegate the call only to the parent if the xpath validation flag
	 * {@link #isInValidatedBlock} is true.
	 */
	@Override
	public void startGeneralEntity(String name, XMLResourceIdentifier id,
			String encoding, Augmentations augs) throws XNIException {
		if (isInValidatedBlock) {
			super.startGeneralEntity(name, id, encoding, augs);			
		}
	}
		
	/**
	 * Delegate the call only to the parent if the xpath validation flag
	 * {@link #isInValidatedBlock} is true.
	 * 
	 * After each new start element we revalidate the xpath expression against
	 * the element stach {@link #eventStack}.
	 */
	@Override
	public void startElement(QName element, XMLAttributes attributes, Augmentations augs) throws XNIException {
		eventStack.addLast(new Event(element.localpart, attributes));
		if (isInValidatedBlock) {
			super.startElement(element, attributes, augs);
		} else if (matchExpression()) {
			// Task 7633: If the expression ends with an wildcard we do NOT
			// add the starting element! (Because we search for xpath-1 expr.)
			if (this.includeStartAndEndTag) {
				super.startElement(element, attributes, augs);
			}
			isInValidatedBlock = true;
		}
	}
	
	/**
	 * Delegate the call only to the parent if the xpath validation flag
	 * {@link #isInValidatedBlock} is true.
	 * 
	 * Before the new start element were delegate
	 * we revalidate the xpath expression against
	 * the element stach {@link #eventStack}.
	 */
	@Override
	public void endElement(QName element, Augmentations augs) throws XNIException {
		eventStack.removeLast();
		if (isInValidatedBlock) {
			if (matchExpression()) {
				super.endElement(element, augs);
			} else {
				// Task 7633: If the expression end with an wildcard we do NOT
				// add the finish element! (Because we search for xpath-1 expr.)
				if (this.includeStartAndEndTag) {
					super.endElement(element, augs);
				}
				// Task 7633: Force a new line after finishing a block. This is
				// required because otherwise a new line between to blocks will
				// be removed which could chance the content sense since html
				// newlines are equal to spaces.
				// This was an alternative way to fix space problem which is
				// not more neccessary if is allowed to include also text
				// content with an //html/body/*
//				super.characters(NEW_LINE_XML_STRING, EMPTY_HTML_AUGMENTATION);
				isInValidatedBlock = false;
			}
		}
	}

	/**
	 * Check if the current element stack ({@link #eventStack}) matches the
	 * xpath {@link #expression}.
	 */
	protected boolean matchExpression() {
		if (eventStack.size() < expression.size()) {
			return false;
		}
		for (int pathIdx = 0; pathIdx < expression.size() && pathIdx < eventStack.size(); pathIdx++) {
			if (pathIdx >= eventStack.size() || !expression.get(pathIdx).matchLocalNames(eventStack.get(pathIdx))) {
				return false;
			}
			String exprClass = expression.get(pathIdx).classValue;
			if (exprClass != null) {
				String eventClass = eventStack.get(pathIdx).classValue;
				if (eventClass == null) {
					return false;						
				}
				if (!eventClass.equalsIgnoreCase(exprClass)) {
					return false;
				}				
			}
			String exprId = expression.get(pathIdx).idValue;
			if (exprId != null) {
				String eventId = eventStack.get(pathIdx).idValue;
				if (eventId == null) {
					return false;						
				}
				if (!eventId.equalsIgnoreCase(exprId)) {
					return false;
				}				
			}
			
		}
		return true;
	}
		
	class Event {
		String localName;
		String classValue;
		String idValue;
		
		public Event(String localName, XMLAttributes attributes) {
			this.localName = localName;
			if (attributes != null) {
				this.classValue = attributes.getValue(R.HTML.CLASS);
				this.idValue = attributes.getValue(R.HTML.ID);				
			}
		}
		
		@Override
		public String toString() {
			String str = localName;
			if (classValue != null)
				str += "@class=" + classValue;
			if (idValue != null)
				str += "@id=" + idValue;
			return str;
		}

		public boolean matchLocalNames(Event otherEvent) {
			return this.localName.equalsIgnoreCase(otherEvent.localName) ||
					localName.equals(TAGNAME_WILDCARD);
		}
		
		public boolean equals(Object obj) {
			Event other = (Event) obj;
			return (this.localName.equalsIgnoreCase(other.localName) && 
					(this.classValue == null && other.classValue == null || this.classValue.equals(other.classValue)) &&
					(this.idValue == null && other.idValue == null || this.idValue.equals(other.idValue)));
		}
	}
}
