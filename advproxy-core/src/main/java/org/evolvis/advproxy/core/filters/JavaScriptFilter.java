/*
 * advproxy Advanced servlet and portlet proxy Copyright (c) 2008 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License,version 2 as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.evolvis.advproxy.core.filters;

import java.io.IOException;
import java.io.Writer;
import java.util.regex.Pattern;

import javax.portlet.PortletPreferences;

import org.apache.xerces.xni.Augmentations;
import org.apache.xerces.xni.QName;
import org.apache.xerces.xni.XMLAttributes;
import org.apache.xerces.xni.XMLString;
import org.apache.xerces.xni.XNIException;
import org.cyberneko.html.filters.DefaultFilter;
import org.evolvis.advproxy.core.PortletComponent;
import org.evolvis.advproxy.core.PortletUrlRewriter;
import org.evolvis.advproxy.core.R;
import org.evolvis.advproxy.core.ResourceUrlsChecker;

/**
 * Filters all 'src' attributes inside a script element. If the URL value 
 * of the 'src' attributes matches a ResourceUrl-Pattern inside the 
 * ResourceUrlsChecker, the value will be rewritten to a Portlet-ResourceURL.
 * 
 * Also checks if the included *.js files are not excluded.
 * 
 * @author Sabine Hoecher
 * @author Tino Rink
 */
public class JavaScriptFilter extends DefaultFilter implements PortletComponent {
	
	protected Writer headInclude;
	
	protected PortletUrlRewriter portletUrlRewriter;
	
	//parser is in a script snippet
	private boolean isInScript;
	
	//JavaScript is enabled
	private boolean includeJS;
	
	//parser is in head
	private boolean isInHead = false;

	//excluded javascriptURLs of preferences
	private String[] excludes;
	
	//endtags must be removed if starttags were removed
	private boolean removeTag;
	
	/** Checks if an URL is a resourceurl */
	private ResourceUrlsChecker resourceUrlsChecker;
	
	public JavaScriptFilter(Writer headInclude, PortletUrlRewriter portletUrlRewriter, ResourceUrlsChecker resourceUrlsChecker) {
		this.portletUrlRewriter = portletUrlRewriter;
		this.headInclude = headInclude;
		this.resourceUrlsChecker = resourceUrlsChecker;
	}
	
	/**
	 * Handler vor start elements which saves if the current context is in
	 * a script element ({@link #isInScript} or in the header
	 * ({@link #isInHead}).
	 */
	@Override
	public void startElement(QName element, XMLAttributes attributes,
			Augmentations augs) throws XNIException {
		
		if (!R.HTML.SCRIPT.equals(element.localpart)) {
			if (R.HTML.HEAD.equals(element.localpart)) {
				isInHead = true;
			}
			super.startElement(element, attributes, augs);
		}
		else { //localpart is script	
			if (includeJS) { //write JS
				try {
					processJS(element, attributes, augs);
				} catch (IOException e) {
					throw new XNIException(e);
				}
			} else {
				removeTag = true;
				isInScript = true;
			}
		}
	}
	
	/**
	 * Handler for the end elements. See {@link #startElement(QName, XMLAttributes, Augmentations)}
	 * for more finrmations.
	 */
	@Override
	public void endElement(QName element, Augmentations augs)
			throws XNIException {
		if (isInScript && R.HTML.SCRIPT.equals(element.localpart) && isInHead) {
			try {
				headInclude.write("</script>");
			} catch (IOException e) {
				throw new XNIException(e);
			}
			isInScript = false;
		} else if (isInHead && R.HTML.HEAD.equals(element.localpart)) {
			isInHead = false;
		}
		if (removeTag) {
			removeTag = false;
			isInScript = false;
			return;
		} else {
			super.endElement(element, augs);
		}
	}
	
	
	/**
	 * writing javascript functions, etc
	 */
	@Override
	public void characters(XMLString text, Augmentations augs)
			throws XNIException {
		
		if (!removeTag) {
			if (includeJS && isInScript) {
				try {
					headInclude.write(text.toString());
				} catch (IOException e) {
					throw new XNIException(e);
				}
			}
			if (includeJS || !isInScript) { // javascript parts should only be written if javaScript is enabled
				super.characters(text, augs);
			}
		}
	}
	
	
	@Override
	public void comment(XMLString text, Augmentations augs) throws XNIException {
		if (includeJS && isInScript) {
			try {
				headInclude.write(text.toString());
			} catch (IOException e) {
				throw new XNIException(e);
			}
		}
		super.comment(text, augs);
	}
	

	/**
	 * checks if a given src-attribute was excluded in preferences
	 */
	protected boolean matchesExcludes(String src) {
		if (src == null) {
			return false;			
		}
		if (excludes != null) {
			for (String regex : excludes) {
				if (Pattern.matches(regex.trim(), src)) {
					return true;				
				}
			}
		}
		return false;
	}
	
	
	/**
	 * JavaScript must be included so we have to write it down
	 */
	private void processJS(QName element, XMLAttributes attributes, Augmentations augs) throws IOException {
		if (isInHead) {
			processHeadInclude(element, attributes, augs);
		} else {
			processBodyInclude(element, attributes, augs);
		}
	}

	
	/**
	 * returns true if external resource is allowed, if there is a not allowed resource we have to remove the whole tag
	 */
	private boolean isResourceAllowed(XMLAttributes attributes) {

		if(attributes.getIndex(R.HTML.SRC) != -1) {
			if (!matchesExcludes(attributes.getValue(attributes.getIndex(R.HTML.SRC)))) {
				return true;
			} else {
				removeTag = true;
			}
		}
		return false;
	}
	
	
	/**
	 * we have to build a "headInclude" cause the script of head-element should be set to beginning of the portlet
	 */
	private void processHeadInclude(QName element, XMLAttributes attributes, Augmentations augs) throws IOException {
		
		if (attributes.getIndex(R.HTML.SRC) != -1){
			if (isResourceAllowed(attributes)) { // external source
				headInclude.write("<script type=\"text/javascript\"");
				
				final int idInx = attributes.getIndex("id");
				if (idInx != -1) {
					headInclude.write(" id=" + "\"" + attributes.getValue(idInx) + "\"");
				}
				
                int charsetInx = attributes.getIndex(R.HTML.CHARSET);
                if (charsetInx != -1) {
                	headInclude.write(" charset=" + "\"" + attributes.getValue(charsetInx) + "\"");
                }
				
				String url = portletUrlRewriter.updateScriptURL(attributes.getValue(attributes.getIndex(R.HTML.SRC)));
				if (resourceUrlsChecker.isResourceUrl(url)) {
					url = portletUrlRewriter.createResourceRequestURL(url, R.ACTION_FORMGETURL, null);
				}
				headInclude.write(" src=\"" + url + " \"></script>");
				super.startElement(element, attributes, augs);
			}
		} else {
			headInclude.write("<script type=\"text/javascript\"");
			
			final int idInx = attributes.getIndex("id");
			if (idInx != -1) {
				headInclude.write(" id=" + "\"" + attributes.getValue(idInx) + "\"");
			}
			
            int charsetInx = attributes.getIndex(R.HTML.CHARSET);
            if (charsetInx != -1) {
            	headInclude.write(" charset=" + "\"" + attributes.getValue(charsetInx) + "\"");
            }
			
			// no external sources
			headInclude.write(">");
			isInScript = true;
			super.startElement(element, attributes, augs);
		}
	}
	

	/**
	 * rewrites the script parts of the former body if resource is allowed
	 */
	private void processBodyInclude(QName element, XMLAttributes attributes,
			Augmentations augs) {

		final String src = attributes.getValue(R.HTML.SRC);
		final int srcIdx = attributes.getIndex(R.HTML.SRC);

		if (srcIdx > -1 && src != null) {
			if (isResourceAllowed(attributes)) { // external source
				String url = this.portletUrlRewriter.updateScriptURL(src);

				if (resourceUrlsChecker.isResourceUrl(url)) {
					url = portletUrlRewriter.createResourceRequestURL(url, R.ACTION_FORMGETURL, null);
				}
				
				if (!src.equals(url)) {
					attributes.setValue(srcIdx, url);
				}
				super.startElement(element, attributes, augs);
			} else {
				removeTag = true;
//				isInScript = true;
			}
		} else {
			super.startElement(element, attributes, augs);
		}
	}


	public void setUp(PortletPreferences preferences) {
		includeJS = Boolean.valueOf(preferences.getValue(R.PREFS.INCLUDE_JAVASCRIPT, null));
		excludes = preferences.getValues(R.PREFS.JAVA_SCRIPT_EXCLUDES, new String[]{});
	}

}
