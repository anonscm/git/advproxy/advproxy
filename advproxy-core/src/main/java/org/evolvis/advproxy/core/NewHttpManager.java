/*
 * advproxy Advanced servlet and portlet proxy Copyright (c) 2008 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License,version 2 as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.evolvis.advproxy.core;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.params.AllClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.ExecutionContext;
import org.apache.http.protocol.HttpContext;

/**
 * HttpManager copies from?? :D
 */
public class NewHttpManager {

	private static SchemeRegistry schemeRegistry;
	static {
		schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(
		         new Scheme("http", 80, PlainSocketFactory.getSocketFactory()));
		schemeRegistry.register(
		         new Scheme("https", 443, SSLSocketFactory.getSocketFactory()));
	}

	protected Log log = LogFactory.getLog(getClass());
	
	private AllowedUrlsAndXPaths allowedUrlsAndXPaths;
	
	private URI requestUrl;
	
	private URI refererUrl;
	
	private Map<String, String[]> requestParam = new HashMap<String, String[]>();
	
	private boolean useCookies;
	
	private List<Cookie> additionalCookies = new ArrayList<Cookie>();
	
	private boolean useProxy;
	
	private String proxyHost;
	
	private int proxyPort;
	
	private boolean isMultipartRequest;
	
	private List<FileItem> multipartFileItems = Collections.emptyList();
	
	private String userAgent;
	
	public AllowedUrlsAndXPaths getAllowedUrlsAndXPaths() {
		return allowedUrlsAndXPaths;
	}

	public void setAllowedUrlsAndXPaths(AllowedUrlsAndXPaths allowedUrlsAndXPaths) {
		this.allowedUrlsAndXPaths = allowedUrlsAndXPaths;
	}

	public URI getRequestUrl() {
		return requestUrl;
	}

	public void setRequestUrl(URI url) {
		this.requestUrl = fixUrl(url);
	}
	
	public URI getRefererUrl() {
		return refererUrl;
	}

	public void setRefererUrl(URI refererUrl) {
		this.refererUrl = fixUrl(refererUrl);
	}

	public Map<String, String[]> getRequestParam() {
		return requestParam;
	}

	public void setRequestParam(Map<String, String[]> requestParam) {
		this.requestParam = requestParam;
	}

	public boolean isUseCookies() {
		return useCookies;
	}

	public void setUseCookies(boolean useCookies) {
		this.useCookies = useCookies;
	}

	public List<Cookie> getAdditionalCookies() {
		return additionalCookies;
	}

	public void setAdditionalCookies(List<Cookie> additionalCookies) {
		this.additionalCookies = additionalCookies;
	}

	public boolean isUseProxy() {
		return useProxy;
	}

	public void setUseProxy(boolean useProxy) {
		this.useProxy = useProxy;
	}

	public String getProxyHost() {
		return proxyHost;
	}

	public void setProxyHost(String proxyHost) {
		this.proxyHost = proxyHost;
	}

	public int getProxyPort() {
		return proxyPort;
	}

	public void setProxyPort(int proxyPort) {
		this.proxyPort = proxyPort;
	}

	public boolean isMultipartRequest() {
		return isMultipartRequest;
	}

	public void setMultipartRequest(boolean isMultipartRequest) {
		this.isMultipartRequest = isMultipartRequest;
	}

	public List<FileItem> getMultipartFileItems() {
		return multipartFileItems;
	}

	public void setMultipartFileItems(List<FileItem> multipartFileItems) {
		this.multipartFileItems = multipartFileItems;
	}
	
	public String getUserAgent() {
		return userAgent;
	}
	
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public AdvHttpResponse processUrl(HttpMethodeType methodeType, String charset) throws HttpException, IOException {
		
		if (charset == null) 
			throw new IllegalArgumentException("Charset must not be null.");


		HttpContext localContext = new BasicHttpContext();
		ThreadSafeClientConnManager cm = new ThreadSafeClientConnManager(schemeRegistry);
		
		HttpParams httpparams = getDefaultHttpParams(charset);

		DefaultHttpClient httpClient = new DefaultHttpClient(cm, httpparams);
		httpClient.setHttpRequestRetryHandler(new DefaultHttpRequestRetryHandler(3, true));
		
		httpClient.setRedirectStrategy(new AdvproxyRedirectStrategy(allowedUrlsAndXPaths));
		
		if (log.isDebugEnabled()) {
			log.debug("HTTP METHODE\nType: " + methodeType + "\nEncoding:" + charset + "\nURL:" + requestUrl + "\nReferer:" + refererUrl);
		}
		
		// proxy settings
		if (useProxy && proxyHost != null && proxyPort != 0) {
			HttpHost proxy = new HttpHost(proxyHost, proxyPort);
			httpClient.getParams().setParameter(AllClientPNames.DEFAULT_PROXY, proxy);
		}
		
		final HttpRequestBase httpRequest;
		final HttpResponse httpResponse;
		
		// GET
		if (HttpMethodeType.GET.equals(methodeType)) {
			httpRequest = new HttpGet(requestUrl);
			final HttpParams params = new BasicHttpParams();
			for (Map.Entry<String, String[]> param : requestParam.entrySet()) {
				for (String value : param.getValue()) {
					params.setParameter(param.getKey(), value);
				}
			}
			httpRequest.setParams(params);
		}
		//POST
		else {
			// Handle Redirects
			final HttpPost postMethod = new HttpPost();
			httpRequest = postMethod;
			
			if (isMultipartRequest) {
				
				final MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, null, Charset.forName(charset));
				
				for (final Iterator<FileItem> itemIterator = multipartFileItems.iterator(); itemIterator.hasNext(); ) {
					
					final FileItem currentFileItem = itemIterator.next();
					
					// Simple form field handling
					if( currentFileItem.isFormField()) {
						multipartEntity.addPart(currentFileItem.getFieldName(), new StringBody(currentFileItem.getString(charset), Charset.forName(charset)));
					}
					// Files
					else {
						multipartEntity.addPart(currentFileItem.getFieldName(), new InputStreamBody(currentFileItem.getInputStream(), currentFileItem.getName()));
					}
				}
				postMethod.setEntity(multipartEntity);
			}
			else {
				// prepare POST parameters
				List<NameValuePair> pairs = new LinkedList<NameValuePair>();
				for (Map.Entry<String, String[]> param : requestParam.entrySet()) {
					for (String value : param.getValue()) {
						pairs.add(new BasicNameValuePair(param.getKey(), value));
					}
				}
				HttpEntity requestBody = new UrlEncodedFormEntity(pairs);
				postMethod.setEntity(requestBody);
			}
		}

		httpRequest.setURI(requestUrl);
		
		if (refererUrl != null) {
			httpRequest.addHeader(R.HTTP_REFERER_NAME, refererUrl.toString());
		}
		httpRequest.addHeader("User-Agent", userAgent);
		
		// if setCookies parameter is empty, set cookies from the current call
		final CookieStore cookieStore = new BasicCookieStore();
		if (useCookies) {
			for (Cookie aCookie : getAdditionalCookies()) {
				cookieStore.addCookie(aCookie);
			}
		}
		httpClient.setCookieStore(cookieStore);
		
		httpResponse = executeClient(httpClient, httpRequest, localContext);			
	
		if (log.isDebugEnabled()) {
			log.debug("HTTP query " + httpRequest.getMethod() + " " + httpRequest.getURI().toString() + " returns " + httpResponse.getStatusLine());				
		}

		// this object holds the final request after all redirects
		String requestPath = null;

		// There seem to be cases where the attribute ExecutionContext.HTTP_REQUEST is not a HttpUriRequest. If so,
		// we use a workaround. I changed the behaviour because the former behaviour did not work with activated proxy and
		// HTTP 302 relocations.
		if (!(localContext.getAttribute(ExecutionContext.HTTP_REQUEST) instanceof HttpUriRequest)) {
			requestPath = httpRequest.getURI().getPath();
		} else {
			HttpUriRequest finalReq = (HttpUriRequest) localContext
					.getAttribute(ExecutionContext.HTTP_REQUEST);
			// If using proxy finalReq contains host + path, we isolate path here
			requestPath = finalReq.getURI().getPath();
		}
		
		HttpHost finalTargetHost = (HttpHost) localContext.getAttribute(
		        ExecutionContext.HTTP_TARGET_HOST);

		URI finalURI = URI.create(finalTargetHost.toURI() + requestPath);
		final RequestResultType resultType = getRequestResultType(httpResponse);

		// determinate new or changed cookies from resonse	
		final List<Cookie> newOrChangesResponseCookies = new ArrayList<Cookie>();
		newOrChangesResponseCookies.addAll(cookieStore.getCookies());
		newOrChangesResponseCookies.removeAll(additionalCookies);
		
		final AdvHttpResponse response = new AdvHttpResponse();
		response.setResponseHeaders(httpResponse.getAllHeaders());
		response.setResponseEntity(httpResponse.getEntity());
		response.setStatusCode(httpResponse.getStatusLine().getStatusCode());
		response.setFinalRequestUri(finalURI);
		response.setResultType(resultType);
		response.setCookies(newOrChangesResponseCookies);	
		
		return response;
	}
	
	private HttpParams getDefaultHttpParams(String charset) {
		HttpParams httpparams = new BasicHttpParams();		
		httpparams.setParameter(AllClientPNames.CONNECTION_TIMEOUT, Default.CONNECTION_TIMEOUT);
		httpparams.setParameter(AllClientPNames.COOKIE_POLICY, CookiePolicy.BEST_MATCH);
		httpparams.setParameter(AllClientPNames.HANDLE_REDIRECTS, Boolean.TRUE);
		httpparams.setParameter(AllClientPNames.HTTP_CONTENT_CHARSET, charset);
		httpparams.setParameter(AllClientPNames.HTTP_ELEMENT_CHARSET, charset);
		return httpparams;
	}

	private URI fixUrl(URI url) {
		
		if (url == null) {
			return null;
		}
		
		if (url.getScheme() == null) {
			url = URI.create("http://" + url.toString());
		}
		
		return url;
	}
	
	private HttpResponse executeClient(HttpClient client, HttpUriRequest request, HttpContext context)
		throws IOException, NotAllowedRedirectException {
		
		try {
			return client.execute(request, context);
		}
		catch (IOException ioe) {
			if (ioe instanceof ClientProtocolException) {
				ClientProtocolException protocolException = (ClientProtocolException) ioe;
				if (protocolException.getCause() instanceof NotAllowedRedirectException) {
					throw (NotAllowedRedirectException) protocolException.getCause();
				}
			}
			throw ioe;
		}
	}
	
	private RequestResultType getRequestResultType(HttpResponse response) {

		final Header contentMimeType = response.getFirstHeader(R.HTTP_CONTENT_TYPE_NAME);
		final Header contentDisposition = response.getFirstHeader(R.HTTP_CONTENT_DISPOSITION_NAME);
		
		final String contentMimeTypeValue = contentMimeType != null ? contentMimeType.getValue() : null; 
		final String contentDispositionValue = contentDisposition != null ? contentDisposition.getValue() : null;
		
		if ((contentMimeTypeValue == null
				|| contentMimeTypeValue.startsWith("text/")
				|| contentMimeTypeValue.startsWith("application/xml")
				|| contentMimeTypeValue.startsWith("application/xhtml")
				|| contentMimeTypeValue.startsWith("application/xhtml+xml")
			) && (contentDispositionValue == null || !contentDispositionValue.startsWith("attachment;"))
		){
			return RequestResultType.TEXT;
		}
		else if (contentDispositionValue != null && contentDispositionValue.startsWith("attachment;")) {
			return RequestResultType.BINARY_ATTACHEMENT;
		}
		else {
			return RequestResultType.BINARY;
		}
	}
	
	public static List<Cookie> transformCookies(URI requestUrl, javax.servlet.http.Cookie[] cookiesFromUser) {
		final List<Cookie> transformedCookies = new ArrayList<Cookie>();
		if (cookiesFromUser != null) {
			for (javax.servlet.http.Cookie sCookie : cookiesFromUser) {
				BasicClientCookie aCookie = new BasicClientCookie(sCookie.getName(), sCookie.getValue());
				aCookie.setPath("/");
				aCookie.setValue(sCookie.getValue());
				aCookie.setDomain(requestUrl.getHost());
				transformedCookies.add(aCookie);
			}
		}
		return transformedCookies;
	}
	
	/**
	 * transforms a org.apache.http.cookie.Cookie to a javax.servlet.http.Cookie
	 */
	public static javax.servlet.http.Cookie transformFromApacheToServletCookie(
			URI requestUrl, Cookie apacheCookie) {
		final javax.servlet.http.Cookie servletCookie = new javax.servlet.http.Cookie(apacheCookie.getName(), apacheCookie.getValue());
		servletCookie.setPath("/");
		servletCookie.setValue(apacheCookie.getValue());
		servletCookie.setDomain(requestUrl.getHost());
		return servletCookie;
	}
}
