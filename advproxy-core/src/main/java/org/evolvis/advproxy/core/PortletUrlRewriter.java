/*
 * advproxy Advanced servlet and portlet proxy Copyright (c) 2008 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License,version 2 as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.evolvis.advproxy.core;

import javax.portlet.ActionRequest;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceURL;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Portlet specific version of {@link UrlRewriter}.
 * 
 * @author Christoph Jerolimov, Tim Steffens, Michael Kutz tarent GmbH
 *
 */
public class PortletUrlRewriter {
	
	private static Log log = LogFactory.getLog(PortletUrlRewriter.class.getName());
	
	private RenderRequest request;
	private RenderResponse response;
	private String userUUIDCookieName;

	/** <code>http://</code>, <code>https://</code> or null. */
	private String baseURLProtocol;
	/** Server incl. port (for example <code>evolvis.org:8080</code> */
	private String baseURLServer;
	/** Path starting with and ending without a slash. */
	private String baseURLPath;
	
	/**
	 * Create a new url rewriter instance.
	 * 
	 * @param baseUrl
	 * @param encoding
	 */
	public PortletUrlRewriter(String baseUrl) {
		if (log.isTraceEnabled())
			log.trace("New instance of UrlRewriter created with base url " + baseUrl);
		this.updateBaseURL(baseUrl);
	}
	
	/**
	 * Create a new url rewriter instance.
	 * 
	 * @param request
	 * @param response
	 * @param baseUrl
	 * @param encoding
	 * @param userUUIDCookieName
	 */
	public PortletUrlRewriter(
			RenderRequest request,
			RenderResponse response,
			String baseUrl,
			String userUUIDCookieName) {
		if (log.isTraceEnabled())
			log.trace("New instance of UrlRewriter created with base url " + baseUrl);
		this.updateBaseURL(baseUrl);
		this.request = request;
		this.response = response;
		this.userUUIDCookieName = userUUIDCookieName;
	}
	
	/**
	 * Returns a {@link String} reprasentation of {@link PortletURL} where the
	 * parameters __advproxy_url and actionName are set
	 * 
	 * @param actionName
	 *            used for setting the value of the
	 *            {@link ActionRequest#ACTION_NAME} parameter
	 * @param url
	 *            used for setting the value of __advproxy_url
	 * @return {@link String} reprasentation of {@link PortletURL}
	 */
	protected String createActionRequestURL(String actionName, String url) {
		PortletURL portletURL = response.createActionURL();
		portletURL.setParameter(ActionRequest.ACTION_NAME, actionName);
		portletURL.setParameter(R.PORTLET_URL_PARAM_NAME, url);
		return portletURL.toString();
	}
	
	/**
	 * Returns a {@link String} reprasentation of {@link PortletURL} where the
	 * parameters __advproxy_url and actionName are set
	 * 
	 * @param actionName
	 *            used for setting the value of the
	 *            {@link ActionRequest#ACTION_NAME} parameter
	 * @param url
	 *            used for setting the value of __advproxy_url
	 * @param referer
	 * @return {@link String} reprasentation of {@link PortletURL}
	 */
	public  String createActionRequestURL(String actionName, String url, String referer) {
		PortletURL portletURL = response.createActionURL();
		portletURL.setParameter(ActionRequest.ACTION_NAME, actionName);
		portletURL.setParameter(R.PORTLET_REFERER_PARAM_NAME, referer);
		portletURL.setParameter(R.PORTLET_URL_PARAM_NAME, url);
		return portletURL.toString();
	}
	
	/**
	 * Return a resource {@link PortletURL} where the
	 * parameters __advproxy_url and actionName are set
	 * 
	 * @param url the original resource url.
	 * @param resourceID the original resource id.
	 * @param referer
	 * @return
	 */
	public String createResourceRequestURL(String url, String resourceID, String referer) {
		ResourceURL resourceURL = response.createResourceURL();
		resourceURL.setParameter(R.PORTLET_URL_PARAM_NAME, url);
		
		if (resourceID != null) {
			resourceURL.setResourceID(resourceID);
		}
		
		if (referer != null) {
			resourceURL.setParameter(R.PORTLET_REFERER_PARAM_NAME, referer);
		}
		
		if (userUUIDCookieName != null && request.getRemoteUser() != null) {
			resourceURL.setParameter(userUUIDCookieName, request.getRemoteUser());
		}
		
		return resourceURL.toString();
	}
	
	/**
	 * Adds the protocol prefix and the server domainname to the url. This
	 * method is used by all protected updateURL method beside updateBaseURL.
	 * Interprets the url with the given encoding. 
	 * 
	 * @param url
	 * @param encoding Encoding of the url string param
	 * @return
	 */
	public String simpleUpdateURL(String url) {
		if (log.isTraceEnabled())
			log.trace("simpleUpdateURL() was called");
		String updatedUrl;
		if (url == null) {
			if (log.isDebugEnabled())
				log.debug("The url was null so simpleUpdateURL() returns null");
			return null;
		} else {
			// remove linebreaks and html characters
			url = url.replace("\n", "").replace("&auml;", "\u00E4").replace("&ouml;", "\u00F6")
			.replace("&uuml;", "\u00FC").replace("&Auml;", "\u00C4").replace("&Ouml;", "\u00D6")
			.replace("&Uuml;", "\u00DC").replace("&szlig;", "\u00DF").replace("&amp;", "&");
			
			/* TODO mpelze
			 * 
			 * Commented decoding of the URL because I don't understand why we do this.
			 * This seems to cause an error in EUDLR Berlin.
			 * 
			 * As I see it the proxy portlet decodes all URLs that are in the HTML code
			 * that the portlet got from the remote site. The question is: Why? I see no reason for
			 * that.
			 */
			/*try {
				// in case the urls on the site are already url-encoded, decode url
				url = URLDecoder.decode(url, encoding);
			} catch (IllegalArgumentException e) {
				if (log.isErrorEnabled()) {	
					log.error("Error while decoding URL: " + url, e);
				}
			} catch (UnsupportedEncodingException e) {
				if (log.isWarnEnabled()) {
					log.warn("Charset encoding is not supported in URL: " + url, e);
				}
			}*/
			
			if (url.startsWith("/")) {
				updatedUrl = baseURLProtocol + baseURLServer + url;
			} else if (!url.contains("://")) {
				updatedUrl = getBaseURL() + "/" + url;
			} else {
				updatedUrl = url;
			}
		}
		if (log.isDebugEnabled())
			log.debug("The url:\n" + url + "\nwas changed to:\n" + updatedUrl);
		return updatedUrl;
	}
	
	/**
	 * Create a string representation of a new {@link PortletURL} which wrapps
	 * the given url to an form GET url.
	 * 
	 * @param url
	 * @return
	 */
	public String updateFormGetUrl(String url) {
		return createActionRequestURL(R.ACTION_FORMGETURL, simpleUpdateURL(url));
	}

	/**
	 * Create a string representation of a new {@link PortletURL} which wrapps
	 * the given url to an form POST url.
	 * 
	 * @param url
	 * @return
	 */
	public String updateFormPostUrl(String url) {
		return createActionRequestURL(R.ACTION_FORMPOSTURL, simpleUpdateURL(url));
	}

	/**
	 * Create a string representation of a new {@link PortletURL} which wrapps
	 * the given url to an A-TAG-LINK url.
	 * 
	 * @param url
	 * @return
	 */
	public String updateLinkURL(String url) {
		return createActionRequestURL(R.ACTION_OPENURL, simpleUpdateURL(url));
	}

	/**
	 * Create a string representation of a new {@link PortletURL} which wrapps
	 * the given url to an IMG-SRC link.
	 * 
	 * @param url
	 * @return
	 */
	public String updateImageURL(String url) {
		if (log.isTraceEnabled())
			log.trace("updateImageURL() was called");
		return simpleUpdateURL(url);
	}

	/**
	 * Create a string representation of a new {@link PortletURL} which wrapps
	 * the given url to a SCRIPT-SRC link.
	 * 
	 * @param url
	 * @return
	 */
	public String updateScriptURL(String url) {
		if (log.isTraceEnabled())
			log.trace("updateScriptURL() was called");
		return simpleUpdateURL(url);
	}

	/**
	 * Return the current base url as absolute URI including protocal and server.
	 * 
	 * @return
	 */
	public String getBaseURL() {
		if (log.isTraceEnabled())
			log.trace("getBaseURL() was called");
		if (baseURLProtocol == null || baseURLServer == null || baseURLPath == null) {
			log.warn("base protocol, server url, or path was null");
			return null;
		}
		if (log.isDebugEnabled())
			log.debug("Base url is " + baseURLProtocol + baseURLServer + baseURLPath);
		return baseURLProtocol + baseURLServer + baseURLPath;
	}
	
	/**
	 * Handler to respect the URL of an <base href=""> Tag! Saves the parts
	 * of an base url to {@link #baseURLProtocol}, {@link #baseURLServer}
	 * and {@link #baseURLPath}.
	 * 
	 * @param url
	 * @return The current url wrapped with the new base url parameters.
	 */
	public String updateBaseURL(String url) {
		if (log.isTraceEnabled())
			log.trace("updateBaseURL() was called");
		baseURLProtocol = null;
		baseURLServer = null;
		baseURLPath = null;
		if (url == null || url.length() == 0)
			return null;

		// Anchors are ignored
		if (url.contains("#"))
			url = url.substring(0, url.indexOf("#"));
		String protocol;
		String server;
		if (url.startsWith("http://")) {
			protocol = "http://";
			if (url.indexOf("/", 7) >= 0) {
				server = url.substring(7, url.indexOf("/", 7));
				url = url.substring(url.indexOf("/", 7));
			} else {
				server = url.substring(7);
				url = "";
			}
		} else if (url.startsWith("https://")) {
			protocol = "https://";
			if (url.indexOf("/", 8) >= 0) {
				server = url.substring(8, url.indexOf("/", 8));
				url = url.substring(url.indexOf("/", 8));
			} else {
				server = url.substring(8);
				url = "";
			}
		} else {
			baseURLProtocol = null;
			baseURLServer = null;
			baseURLPath = null;
			IllegalArgumentException e = new IllegalArgumentException("Base URL must start with one of the protocols http:// or https://, but URL is " + url);
			if (log.isErrorEnabled())
				log.error("Wrong base url", e);
			throw e;
		}

		// cut off file resource and trailing slash
		if (url.endsWith("/") || (!url.endsWith("/") && url.contains("/"))) {
			url = url.substring(0,  url.lastIndexOf("/"));
		}
		
		baseURLProtocol = protocol;
		baseURLServer = server;
		baseURLPath = url;
		return getBaseURL();
	}
}
