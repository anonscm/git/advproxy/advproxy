/*
 * advproxy Advanced servlet and portlet proxy Copyright (c) 2008 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License,version 2 as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.evolvis.advproxy.core;

import javax.xml.namespace.QName;

/**
 * Global advproxy resource names / keys.
 */
public final class R {
	/** portlet action parameter name */
	public static final String PORTLET_ACTION = "portletAction";
	/** portlet acton value */
	public static final String ACTION_SAVESETTINGS = "saveSettings";
	/** portlet acton value */
	public static final String ACTION_OPENURL = "openURL";
	/** portlet acton value */
	public static final String ACTION_OPENRESOURCE = "openResource";
	/** portlet acton value */
	public static final String ACTION_FORMPOSTURL = "formPostURL";
	/** portlet acton value */
	public static final String ACTION_FORMGETURL = "formGetURL";
	/** portlet acton value */
	public static final String ACTION_OPENPREVIEW = "openPreview";
	/** portlet prefix */
	public static final String JAVAX_PORTLET_PREFIX = "javax.portlet.";
	/** portlet prefix */
	public static final String JAVAX_PORTLET_ACTION = "javax.portlet.action";
	/** advproxy prefix */
	public static final String ADVPROXY_PORTLET_PREFIX = "org.evolvis.advproxy.portlet.";	
	/** portlet property key */
	public static final String POST_PARAMETER_MAP = "postParameterMap";
	/** portlet property key */
	public static final String HTTP_REQUEST_METHOD = "httpRequestMethod";
	/** portlet property key */
	public static final String VALIDATION_ERRORS_PARAM_NAME = "validationErrors";
	/** open url event name */
	public static final QName OPEN_URL_EVENT_QNAME = new QName("http://advproxy.evolvis.org/", "OpenURL");
	/** internal property key */
	public static final String RUNTIME_EXCEPTION = ADVPROXY_PORTLET_PREFIX + "runtimeexception";
	/** internal property key */
	public static final String PORTLET_REFERER_PARAM_NAME = ADVPROXY_PORTLET_PREFIX + "referer";
	/** internal property key */
	public static final String PORTLET_URL_PARAM_NAME = ADVPROXY_PORTLET_PREFIX + "url";
	/** internal property key */
	public static final String PORTLET_HTTP_RESPONSE = ADVPROXY_PORTLET_PREFIX + "response";
	/** go button name */
	public static final String PORTLET_BUTTON_GO_NAME = "go";
	/** url property name */
	public static final String PORTLET_BYNARY_FILE_RESOURCE_URL_NAME = "resourceUrl";
	/** resource id property name */
	public static final String PORTLET_RESOURCE_ID_PARAM_NAME = "resourceID";

	/** configuration advproxy parameter name */
	public static final String EDIT_ELEMENT_NAME_PREFERENCES_URL = "preferencesUrl";
	/** configuration advproxy parameter name */
	public static final String EDIT_ELEMENT_NAME_PORTLET_NAMESPACE = "portlet_namespace";
	/** configuration advproxy parameter name */
	public static final String EDIT_ELEMENT_NAME_GENERATE_ADDITIONAL_COOKIES = "generateAdditionalCookies";
	/** configuration advproxy parameter name */
	public static final String EDIT_ELEMENT_NAME_SEND_USER_UUID = "sendUserUUID";
	/** configuration advproxy parameter name */
	public static final String EDIT_ELEMENT_NAME_USER_UUID_COOKIE_NAME = "userUUIDCookieName";
	/** configuration advproxy parameter name */
	public static final String EDIT_ELEMENT_NAME_APPEND_USER_UUID_TO_RESOURCEURL = "appendUserUUIDToResourceURL";
	/** configuration advproxy parameter name */
	public static final String EDIT_ELEMENT_NAME_SUBMIT = "submit";

	/** http header name for the referer page */
	public static final String HTTP_REFERER_NAME = "Referer";
	/** http header name for the content type */
	public static final String HTTP_CONTENT_TYPE_NAME = "Content-Type";	
	/** http header name for the attachment attributes */
	public static final String HTTP_CONTENT_DISPOSITION_NAME = "Content-Disposition";
	/** http header name for an redirect location url */
	public static final String HTTP_LOCATION = "Location";
	/** utf-8 const */
	public static final String UTF_8 = "UTF-8";
	/** auto const */
	public static final String AUTO = "auto";
	/** status code property key */
	public static final String STATUS_CODE = "status_code";

	/** response code property key */
	public static final String RESPONSE_TYPE = "response_type";
	/** response content property key */
	public static final String RESPONSE_CONTENT = "response_content";
	/** response redirect url property key */
	public static final String FINAL_REQUEST_URI = "final_request_uri";
	/** request result type property name */
	public static final String REQUEST_RESULT_TYPE = "request_result_type";
	/** request result html content property name */
	public static final String REQUEST_RESULT_HTML = "request_result_html";
	
	/**
	 * HTML tag and attribute names.
	 * 
	 * All tags in upper-, and all attributes in lowercase!
	 */
	public static final class HTML {
		/** A tag */
		public static final String A = "A";
		/** BASE tag */
		public static final String BASE = "BASE";
		/** HTML tag */
		public static final String HTML = "HTML";
		/** BODY tag */
		public static final String BODY = "BODY";
		/** FORM tag */
		public static final String FORM = "FORM";
		/** SCRIPT tag */
		public static final String SCRIPT = "SCRIPT";
		/** IMG tag */
		public static final String IMG = "IMG";
		/** INPUT tag */
		public static final String INPUT = "INPUT";
		/** META tag */
		public static final String META = "META";
		/** LINK tag */
		public static final String LINK = "LINK";
		/** HEAD tag */
		public static final String HEAD = "HEAD";

		/** method attribute */
		public static final String METHOD = "method";
		/** method value post */
		public static final String POST = "post";
		/** action attribute */
		public static final String ACTION = "action";
		/** src attribute */
		public static final String SRC = "src";
		/** href attribute */
		public static final String HREF = "href";
		/** target attribute */
		public static final String TARGET = "target";
		/** target value _blank */
		public static final String _BLANK = "_blank";
		/** name attribute */
		public static final String NAME = "name";
		/** meta value */
		public static final String REFRESH = "refresh";
		/** meta value */
		public static final String CONTENT = "content";
		/** meta value */
		public static final String HTTP_EQUIV = "http-equiv";
		/** meta value */
		public static final String URL_ = "url=";
		/** class attribute */
		public static final String CLASS = "class";
		/** id attribute */
		public static final String ID = "id";
		/** for attribute */
		public static final String FOR = "for";
		/** type attribute */
		public static final String TYPE = "type";
		/** value attribute */
		public static final String VALUE = "value";		
		/** value? */
		public static final String EXTERN = "extern";
		/** type value hidden */
		public static final String HIDDEN = "hidden";
		/** cdata attribute */
		public static final String CDATA = "CDATA";
		/** charset attribute */
		public static final String CHARSET = "charset";
		/** accept-charset attribut */
		public static final String ACCEPT_CHARSET = "accept-charset";
	}
	
	/**
	 * Preference view consts.
	 */
	public static final class PREFS {
		/** url */
		public static final String JSP_EDIT_REL_URL = "/jsp/edit.jsp";
		/** url */
		public static final String JSP_VIEW_FINISH_REL_URL = "/jsp/view_finish.jsp";
		/** url */
		public static final String JSP_VIEW_BEGINN_REL_URL = "/jsp/view_beginn.jsp";
		/** parameter */
		public static final String DEFAULT_ENCODING = "defaultEncoding";

		//Basic Options
		/** parameter */
		public static final String HOMEPAGE = "homepage";
		/** parameter */
		public static final String ALLOWED_URLS = "allowedURLs";
		/** parameter */
		public static final String FORCE_NEW_WINDOW = "forceNewWindowOnNotAllowedURLs";
		/** parameter */
		public static final String CHECK_RESOURCE_URLS = "checkResourceUrls";
		
		//User Interface
		/** parameter */
		public static final String MAX_HEIGHT = "maxHeight";
		/** parameter */
		public static final String MIN_HEIGHT = "minHeight";
		/** parameter */
		public static final String SHOW_ADDRESSBAR = "showAddressbar";
		/** parameter */
		public static final String IS_ADDRESS_EDITABLE = "isAddressEditable";
		/** parameter */
		public static final String SHOW_GO_HOME_BUTTON = "showGoHomeButtonInAddressbar";
		
		//HTML
		/** parameter */
		public static final String INCLUDE_JAVASCRIPT = "includeJavaScript";
		/** parameter */
		public static final String JAVA_SCRIPT_EXCLUDES = "javaScriptExcludesFilter";
		/** parameter */
		public static final String GENERATE_ADDITIONAL_COOKIES = "generateAdditionalCookies";
		/** parameter */
		public static final String SEND_USER_UUID = "sendUserUUID";
		/** parameter */
		public static final String USER_UUID_COOKIENAME = "userUUIDCookieName";		
		/** parameter */
		public static final String APPEND_USER_UUID_TO_RESOURCEURL = "appendUserUUIDToResourceURL";
		/** parameter */
		public static final String UPDATE_FORMULAR_FIELDS = "updateFormularFields";
		/** parameter */
		public static final String CLASS_REWRITE_ENABLED = "isClassRewriteEnabled";
		/** parameter */
		public static final String ID_REWRITE_ENABLED = "isIdRewriteEnabled";
		/** parameter */
		public static final String NAME_REWRITE_ENABLED = "isNameRewriteEnabled";
		/** parameter */
		public static final String CLASS_REWRITE_FORCED = "isClassRewriteForced";
		/** parameter */
		public static final String CLASS_SUFFIX = "classSuffix";
		
		//Misc
		/** parameter */
		public static final String USE_PROXY = "useProxy";
		/** parameter */
		public static final String PROXY_PORT = "proxyPort";
		/** parameter */
		public static final String PROXY_HOST = "proxyHost";
		/** parameter */
		public static final String USE_INTERPORTLET_COMMUNICTION = "useInterportletCommuniction";
		/** parameter */
		public static final String PORTLET_WINDOW_ID = "portletWindowId";
		/** parameter */
		public static final String LINKED_WINDOW_IDS = "linkedWindowIDs";
		/** parameter */
		public static final String USER_AGENT = "userAgent";
		
		//Template
		/** parameter */
		public static final String OPEN_URL_TEMPLATE_PARAMETER_NAME = "openUrlTemplate";
		/** parameter */
		public static final String FORM_POST_URL_TEMPLATE_PARAMETER_NAME = "formPostUrlTemplate";
		/** parameter */
		public static final String RESOURCE_GET_URL_TEMPLATE_PARAMETER_NAME = "resourceGetUrlTemplate";
		/** parameter */
		public static final String RESOURCE_POST_URL_TEMPLATE_PARAMETER_NAME = "resourcePostUrlTemplate";
	}
	
}
