/*
 * advproxy Advanced servlet and portlet proxy Copyright (c) 2008 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License,version 2 as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.evolvis.advproxy.core;

import javax.portlet.PortletPreferences;

/**
 * Portlet component which could be initialized if the portlet was started.
 */
public interface PortletComponent {
	
	/**
	 * Setup method for the portlet component.
	 * 
	 * @param preferences
	 */
	public void setUp(PortletPreferences preferences);
	
}
