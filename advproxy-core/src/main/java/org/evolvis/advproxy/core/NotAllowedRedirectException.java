package org.evolvis.advproxy.core;

import org.apache.http.client.RedirectException;

/**
 * TODO: docu
 * 
 * @author Sabine Hoecher <s.hoecher@tarent.de>
 * @author Tino Rink <t.rink@tarent.de>
 */
public class NotAllowedRedirectException extends RedirectException {

	private static final long serialVersionUID = -3797502466434185185L;

	private final String notAllowedUrl;
	
	/**
	 * Create a new instance with given msg and url.
	 * 
	 * @param msg
	 * @param notAllowedUrl
	 */
	public NotAllowedRedirectException(String msg, String notAllowedUrl) {
		super(msg);
		this.notAllowedUrl = notAllowedUrl;
	}

	/**
	 * Create a new instance with given msg, url and cause exception.
	 * @param msg
	 * @param notAllowedUrl
	 * @param throwable
	 */
	public NotAllowedRedirectException(String msg, String notAllowedUrl, Throwable throwable) {
		super(msg, throwable);
		this.notAllowedUrl = notAllowedUrl;
	}
	
	/**
	 * The url which is not allowed.
	 * @return
	 */
	public String getNotAllowedUrl() {
		return notAllowedUrl;
	}
}
