package org.evolvis.advproxy.core;

import java.net.URI;

import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.ProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultRedirectStrategy;
import org.apache.http.protocol.HttpContext;

/**
 * TODO:
 * - repairing
 * - location checking 
 * 
 * @author Sabine Hoecher <s.hoecher@tarent.de>
 * @author Tino Rink <t.rink@tarent.de>
 */
public class AdvproxyRedirectStrategy extends DefaultRedirectStrategy {

	private final AllowedUrlsAndXPaths allowedUrlsAndXPaths;
	
	public AdvproxyRedirectStrategy(AllowedUrlsAndXPaths allowedUrls) {
		if (allowedUrls == null) {
			throw new IllegalArgumentException("allowedUrls must not be null");
		}
		this.allowedUrlsAndXPaths = allowedUrls;
	}
	
	public AllowedUrlsAndXPaths getAllowedUrlsAndXPaths() {
		return allowedUrlsAndXPaths;
	}
	
	/**
	 * Check if the location header is allowed for an redirect.
	 */
	@Override
	public URI getLocationURI(HttpRequest request, HttpResponse response,
			HttpContext context) throws ProtocolException {
		Header locationHeader = response.getFirstHeader("location");

		String uriString = null;
		if (locationHeader != null) {
			uriString = locationHeader.getValue();
			uriString = uriString.replace(" ", "%20");
		}
		
		if (!getAllowedUrlsAndXPaths().isAllowedUrl(uriString)) {
			throw new NotAllowedRedirectException("not allowed url=" + uriString, uriString);
		}
		
		response.setHeader("location", uriString);
		return super.getLocationURI(request, response, context);
	}

	/**
	 * Check the http headers if the current http code defines a redirect.
	 */
	@Override
    public boolean isRedirected(
            final HttpRequest request,
            final HttpResponse response,
            final HttpContext context) throws ProtocolException {
		
        if (response == null) {
            throw new IllegalArgumentException("HTTP response may not be null");
        }

        int statusCode = response.getStatusLine().getStatusCode();
        String method = request.getRequestLine().getMethod();
        Header locationHeader = response.getFirstHeader("location");
        
        switch (statusCode) {
        
	        case HttpStatus.SC_MOVED_TEMPORARILY:
	            return (method.equalsIgnoreCase(HttpGet.METHOD_NAME)
	            		|| method.equalsIgnoreCase(HttpHead.METHOD_NAME)
	            		|| method.equalsIgnoreCase(HttpPost.METHOD_NAME)) && locationHeader != null;
	            
	        case HttpStatus.SC_MOVED_PERMANENTLY:
	        case HttpStatus.SC_TEMPORARY_REDIRECT:
	            return method.equalsIgnoreCase(HttpGet.METHOD_NAME)
	            	|| method.equalsIgnoreCase(HttpHead.METHOD_NAME)
	            	|| method.equalsIgnoreCase(HttpPost.METHOD_NAME);
	            
	        case HttpStatus.SC_SEE_OTHER:
	            return true;
	            
	        default:
	        	return false;
        } //end of switch
    }
}
