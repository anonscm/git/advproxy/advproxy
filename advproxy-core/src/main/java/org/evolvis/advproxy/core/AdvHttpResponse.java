package org.evolvis.advproxy.core;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.cookie.Cookie;
import org.apache.http.util.EntityUtils;

/**
 * AdvHttpResponse returned by the {@link NewHttpManager#processUrl(HttpMethodeType, String)}
 */
public class AdvHttpResponse {

	private int statusCode;

	private Header[] responseHeaders;
	
	private HttpEntity responseEntity;
	
	private RequestResultType resultType;
	
	/**
	 * The final request uri after all possible redirects
	 */
	private URI finalRequestUri;
	
	private List<Cookie> cookies;

	/**
	 * Set the response content object.
	 * 
	 * @param responseEntity
	 */
	public void setResponseEntity(HttpEntity responseEntity) {
		this.responseEntity = responseEntity;
	}

	/**
	 * Set the response header object.
	 * 
	 * @param responseHeaders
	 */
	public void setResponseHeaders(Header[] responseHeaders) {
		this.responseHeaders = responseHeaders;
	}

	/**
	 * Return the response header object.
	 * 
	 * @return
	 */
	public Header[] getResponseHeaders() {
		return responseHeaders;
	}

	/**
	 * Set an internal status code.
	 * 
	 * @param statusCode
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * Return the internal status code.
	 * 
	 * @return
	 */
	public int getStatusCode() {
		return statusCode;
	}

	/**
	 * Set {@link AdvHttpResponse#finalRequestUri}
	 * 
	 * @param uri
	 */
	public void setFinalRequestUri(URI uri) {
		this.finalRequestUri = uri;
	}

	/**
	 * @return {@link AdvHttpResponse#finalRequestUri}
	 */
	public URI getFinalRequestUri() {
		return finalRequestUri;
	}

	/**
	 * Return the result type enum (text, binary).
	 * 
	 * @return
	 */
	public RequestResultType getResultType() {
		return resultType;
	}

	/**
	 * Set the result type enum (text, binary).
	 * 
	 * @param resultType
	 */
	public void setResultType(RequestResultType resultType) {
		this.resultType = resultType;
	}

	/**
	 * Set an internal cookie list.
	 * 
	 * @param cookies
	 */
	public void setCookies(List<Cookie> cookies) {
		this.cookies = cookies;
	}

	/**
	 * Return the internal cookie list.
	 * 
	 * @return
	 */
	public List<Cookie> getCookies() {
		return cookies;
	}
	
	/**
	 * Obtains the Content-Type header, if known. It can include a charset attribute.
	 * @return the Content-Type header value or null if the type is unknown
	 */
	public String getContentType() {
		final Header contentType = responseEntity.getContentType();
		return contentType != null ? contentType.getValue() : null;
	}
	
	/**
	 * Obtains the Content-Encoding header, if known.
	 * @return the Content-Type header value or null if the encoding is unknown
	 */
	public String getContentEncoding() {
		final Header contentEncoding = responseEntity.getContentEncoding();
		return contentEncoding != null ? contentEncoding.getValue() : null;
	}
	
	/**
	 * Tells the length of the content, if known.
	 * @return the number of bytes of the content, or a negative number if unknown. 
	 * If the content length is known but exceeds Integer.MAX_VALUE, a negative number is returned.
	 */
	public int getContentLength() {
		final long length = responseEntity.getContentLength();
		return length > Integer.MAX_VALUE ? -1 : Long.valueOf(length).intValue();
	}
	
	/**
	 * Helper method to return the response as string via the
	 * {@link EntityUtils#toString(HttpEntity)} method.
	 * 
	 * @return
	 * @throws ParseException
	 * @throws IOException
	 */
	public String getResponseBodyAsString() throws ParseException, IOException {
		return EntityUtils.toString(responseEntity);
	}
	
	/**
	 * Helper method to return the response as byte array via the
	 * {@link IOUtils#toByteArray(java.io.InputStream)} method.
	 * 
	 * @return
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	public byte[] getResponseBodyAsBytes() throws IllegalStateException, IOException {
		return IOUtils.toByteArray(responseEntity.getContent());
	}

	/**
	 * Write the response content to the given output stream.
	 * 
	 * @param outstream
	 * @throws IOException
	 */
	public void writeResponseBodyToStream(OutputStream outstream) throws IOException {
		responseEntity.writeTo(outstream);
	}
	
	/**
	 * Close the response inputstream if possible.
	 * 
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	public void freeResources() throws IllegalStateException, IOException {
		if (responseEntity != null && responseEntity.getContent() != null) {
			responseEntity.getContent().close();
		}
	}
}