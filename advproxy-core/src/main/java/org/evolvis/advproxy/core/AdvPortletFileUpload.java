package org.evolvis.advproxy.core;

import java.io.IOException;
import java.util.List;

import javax.portlet.ClientDataRequest;

import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileUploadBase;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.portlet.PortletFileUpload;

/**
 * Wrapper for the original {@link PortletFileUpload} class. This class wraps
 * all given {@link ClientDataRequest} with the advproxy portlet delegate
 * {@link AdvPortletRequestContext}.
 */
public class AdvPortletFileUpload extends PortletFileUpload {
	/**
	 * Default constructor like the parent class.
	 */
	public AdvPortletFileUpload() {
		super();
	}
	
	/**
	 * Constructor like the parent class.
	 * 
	 * @param fileItemFactory
	 */
	public AdvPortletFileUpload(FileItemFactory fileItemFactory) {
		super(fileItemFactory);
	}
	
	/**
	 * Wraps the given request with an new instance of the delegate wrapper
	 * class {@link AdvPortletRequestContext}.
	 * 
	 * @param request
	 * @return
	 */
	public static final boolean isMultipartContent(ClientDataRequest request) {
		return FileUploadBase.isMultipartContent(new AdvPortletRequestContext(
				request));
	}

	/**
	 * Wraps the given request with an new instance of the delegate wrapper
	 * class {@link AdvPortletRequestContext}.
	 * 
	 * @param request
	 * @return
	 * @throws FileUploadException
	 */
	@SuppressWarnings("rawtypes")
	public List parseRequest(ClientDataRequest request)
			throws FileUploadException {
		return parseRequest(new AdvPortletRequestContext(request));
	}

	/**
	 * Wraps the given request with an new instance of the delegate wrapper
	 * class {@link AdvPortletRequestContext}.
	 * 
	 * @param request
	 * @return
	 * @throws FileUploadException
	 * @throws IOException
	 */
	public FileItemIterator getItemIterator(ClientDataRequest request)
			throws FileUploadException, IOException {
		return super.getItemIterator(new AdvPortletRequestContext(request));
	}
}
