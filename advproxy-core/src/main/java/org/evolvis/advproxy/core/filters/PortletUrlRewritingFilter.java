/*
 * advproxy Advanced servlet and portlet proxy Copyright (c) 2008 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License,version 2 as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.evolvis.advproxy.core.filters;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.xni.Augmentations;
import org.apache.xerces.xni.QName;
import org.apache.xerces.xni.XMLAttributes;
import org.apache.xerces.xni.XNIException;
import org.cyberneko.html.filters.DefaultFilter;
import org.evolvis.advproxy.core.PortletUrlRewriter;
import org.evolvis.advproxy.core.R;

/**
 * Adds LDS project specific handling to {@link PortletUrlRewritingFilter}.
 */
public class PortletUrlRewritingFilter extends DefaultFilter {
	
	protected static Log log = LogFactory.getLog(PortletUrlRewritingFilter.class.getName());
	
	/** Used for rewriting URLs, such that they can be opened in a advProxy */
	protected PortletUrlRewriter urlRewriter;
	
	public PortletUrlRewritingFilter(PortletUrlRewriter urlRewriter) {
		this.urlRewriter = urlRewriter;
	}
	
	@Override
	public void startElement(QName element, XMLAttributes attributes, Augmentations augs) throws XNIException {
		if (log.isDebugEnabled())
			log.debug("Found a start element: " + element.localpart);

		if (R.HTML.BASE.equals(element.localpart)) {
			String src = attributes.getValue(R.HTML.SRC);		
			int srcIdx = attributes.getIndex(R.HTML.SRC);
			if (srcIdx > -1 && src != null) {
				String url = this.urlRewriter.updateBaseURL(src);
				if (!src.equals(url)) {
					attributes.setValue(srcIdx, url);
				}
			}			
		}
		
		super.startElement(element, attributes, augs);
	}
}
