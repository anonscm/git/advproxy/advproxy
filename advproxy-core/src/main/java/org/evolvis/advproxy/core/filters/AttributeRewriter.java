/*
 * advproxy Advanced servlet and portlet proxy Copyright (c) 2008 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License,version 2 as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.evolvis.advproxy.core.filters;

import javax.portlet.PortletPreferences;

import org.apache.xerces.xni.Augmentations;
import org.apache.xerces.xni.QName;
import org.apache.xerces.xni.XMLAttributes;
import org.apache.xerces.xni.XNIException;
import org.cyberneko.html.filters.DefaultFilter;
import org.evolvis.advproxy.core.PortletComponent;
import org.evolvis.advproxy.core.R;

/**
 * Rewrites HTML elements so that their class, id and name attributes are appended by a suffix.
 * (Suffix given in Edit-Mode)
 * 
 * @author Timo Pick, tarent GmbH
 * @author Sabine Höcher, tarent GmbH
 *
 */
public class AttributeRewriter extends DefaultFilter implements PortletComponent {
		
	protected boolean isClassRewriteEnabled = true;
	protected boolean isIdRewriteEnabled = true;
	protected boolean isNameRewriteEnabled = true;
	protected boolean isClassRewriteForced = true;
	protected String classSuffix = null;
	
	public void setUp(PortletPreferences preferences) {
		this.isClassRewriteEnabled = Boolean.valueOf(preferences.getValue(R.PREFS.CLASS_REWRITE_ENABLED, null));
		this.isIdRewriteEnabled = Boolean.valueOf(preferences.getValue(R.PREFS.ID_REWRITE_ENABLED, null));
		this.isNameRewriteEnabled = Boolean.valueOf(preferences.getValue(R.PREFS.NAME_REWRITE_ENABLED, null));
		this.isClassRewriteForced = Boolean.valueOf(preferences.getValue(R.PREFS.CLASS_REWRITE_FORCED, null));
		this.classSuffix = preferences.getValue(R.PREFS.CLASS_SUFFIX, null);
	}
	
	/**
	 * Delegate to {@link #processAttributes(XMLAttributes)}.
	 */
	@Override
	public void startElement(QName element, XMLAttributes attributes, Augmentations augs) throws XNIException {
		processAttributes(attributes);
		super.startElement(element, attributes, augs);
	}

	/**
	 * Delegate to {@link #processAttributes(XMLAttributes)}.
	 */
	@Override
	public void emptyElement(QName element, XMLAttributes attributes, Augmentations augs) throws XNIException {
		processAttributes(attributes);
		super.emptyElement(element, attributes, augs);
	}
	
	private void processAttributes(XMLAttributes attributes) {
		
		int classIdx = -1;
		int idIdx = -1;
		int nameIdx = -1;
		
		for (int i=0; i<attributes.getLength(); i++) {
			if (isClassRewriteEnabled && R.HTML.CLASS.equals(attributes.getLocalName(i))) {
				classIdx = i;
			}
			if (isIdRewriteEnabled && (R.HTML.ID.equals(attributes.getLocalName(i)) || R.HTML.FOR.equals(attributes.getLocalName(i)))) {
				idIdx = i;
			}
			if (isNameRewriteEnabled && (R.HTML.NAME.equals(attributes.getLocalName(i)))) {
				nameIdx = i;
			}
		}

		if (classIdx > -1) { // class attribute found, append suffix to every class selector in value
			String newValue = "";
			if (attributes.getValue(classIdx) != null) {
				String[] classValues = attributes.getValue(classIdx).split(" ");
				for (int i=0; i<classValues.length; i++) {
					if (i == 0) {
						newValue += classValues[i] + "_" + classSuffix;					
					} else {
						newValue += " " + classValues[i] + "_" + classSuffix;					
					}
				}
				attributes.setValue(classIdx, newValue);
			}
		} else if (isClassRewriteEnabled && isClassRewriteForced && classSuffix != null) { // no class attribute found, force creation
			attributes.addAttribute(new QName(null, R.HTML.CLASS, R.HTML.CLASS, null), "CDATA", classSuffix);
		}
		
		if (idIdx > -1) { // id attribute found
			String newValue = "";
			if (attributes.getValue(idIdx) != null) {
				newValue += attributes.getValue(idIdx) + "_" + classSuffix;
				attributes.setValue(idIdx, newValue);
			} 
		}
		
		if (nameIdx > -1) { // name attribute found
			String newValue = "";
			if (attributes.getValue(nameIdx) != null) {
				newValue += attributes.getValue(nameIdx) + "_" + classSuffix;
				attributes.setValue(nameIdx, newValue);
			} 
		}
	}
}
