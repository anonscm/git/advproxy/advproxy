package org.evolvis.advproxy.core.filters;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.xni.Augmentations;
import org.apache.xerces.xni.QName;
import org.apache.xerces.xni.XMLAttributes;
import org.apache.xerces.xni.XNIException;
import org.cyberneko.html.filters.DefaultFilter;
import org.evolvis.advproxy.core.AllowedUrlsAndXPaths;
import org.evolvis.advproxy.core.PortletUrlRewriter;
import org.evolvis.advproxy.core.R;
import org.evolvis.advproxy.core.ResourceUrlsChecker;

import javax.portlet.PortletPreferences;

/**
 * Handles special cases for links
 * - splits urls and anchors for rewriting the url and suspends the suffix to the anchor
 * - special handling for mailto attribute
 * 
 * @author Tino Rink
 * @author Sabine Höcher
 *
 */
public class LinkRewritingFilter extends DefaultFilter {
	
	protected static Log log = LogFactory.getLog(ImgSrcRewritingFilter.class
			.getName());

	/** Used for rewriting URLs, such that they can be opened in a advProxy */
	private PortletUrlRewriter urlRewriter;
	/** Checks if an URL is a resourceurl */
	private ResourceUrlsChecker resourceUrlsChecker;
	private AllowedUrlsAndXPaths allowedUrls;
	private PortletPreferences preferences;

	/**
	 * Create a new link rewrite filter.
	 * 
	 * @param urlRewriter
	 * @param resourceUrlsChecker
	 * @param allowedUrls
	 * @param preferences
	 */
	public LinkRewritingFilter(PortletUrlRewriter urlRewriter,
			ResourceUrlsChecker resourceUrlsChecker, AllowedUrlsAndXPaths allowedUrls, PortletPreferences preferences) {
		this.urlRewriter = urlRewriter;
		this.resourceUrlsChecker = resourceUrlsChecker;
		this.allowedUrls = allowedUrls;
		this.preferences = preferences;
	}
	
	/**
	 * Search for links (a tag with href attribute). All other start elements
	 * will be delegate with the super class.
	 */
	@Override
    public void startElement(QName element, XMLAttributes attributes, Augmentations augs)
    throws XNIException {

		if (log.isDebugEnabled()) log.debug("Found a start element: " + element.localpart);
	
		if (R.HTML.A.equals(element.localpart)) {
			String href = attributes.getValue(R.HTML.HREF);
			
			int hrefIdx = attributes.getIndex(R.HTML.HREF);
			
			if (hrefIdx > -1 && href != null) {
				
				if (!isJavaScriptLink(href) && !isAnchor(href) && !isMail(href)) {
					
					String simpleUpdateURL; 
					String anchor = "";
					
					//if href contains an anchor we have to split url and anchor (url#anchorName) to eventually add a suffix
					if (containsAnchor(href)) {
						simpleUpdateURL = urlRewriter.simpleUpdateURL(href.substring(0, href.indexOf("#")));
						anchor = setSuffixToAnchor(href.substring(href.indexOf("#")));
					}
					else {
						simpleUpdateURL = urlRewriter.simpleUpdateURL(href);
					}

					if (isMarkedAsExternal(attributes) || isUrlNotAllowed(simpleUpdateURL)) {

						// make external links absolute and add target="_blank"
						if (log.isTraceEnabled()) {
							log.trace("Processing external link: " + href);
						}
						// if href contains an anchor we have to suspend url and anchor (url#anchorName)
						if (containsAnchor(href)) {
							simpleUpdateURL = simpleUpdateURL.concat(anchor);
						}

						attributes.setValue(hrefIdx, simpleUpdateURL); // make absolute

						int targetIdx = attributes.getIndex(R.HTML.TARGET);

						if (targetIdx > -1) {
							attributes.setValue(targetIdx, R.HTML._BLANK);
						}
						else {
							attributes.addAttribute(new QName(null,
									R.HTML.TARGET, R.HTML.TARGET, null),
									R.HTML.CDATA, R.HTML._BLANK);
						}
					}
					else {
						
						String url;
						if (resourceUrlsChecker.isResourceUrl(simpleUpdateURL)){
							url = urlRewriter.createResourceRequestURL(simpleUpdateURL, R.ACTION_FORMGETURL, null);
						}
						else {
							url = this.urlRewriter.updateLinkURL(simpleUpdateURL);							
						}
						// if href contains an anchor we have to suspend url and anchor (url#anchorName)
						if (containsAnchor(href)) {
							url = url.concat(anchor);
						}				
						
						if (!href.equals(url)) {
							attributes.setValue(hrefIdx, url);
						}
					}
				}
				else if (isAnchor(href)) {
					href = setSuffixToAnchor(href);
					attributes.setValue(hrefIdx, href);
				}
				else {
					if (log.isTraceEnabled()) {
						log.trace("Leaving <a> element with email unprocessed:" + href);
					}
				}
			}
		}
		
		super.startElement(element, attributes, augs);
	}

    private boolean isUrlNotAllowed(String simpleUpdateURL) {
        return !this.allowedUrls.isAllowedUrl(simpleUpdateURL);
    }

    private boolean isMarkedAsExternal(XMLAttributes attributes) {
        String classValue = attributes.getValue(R.HTML.CLASS);
        String externalCssClassName = determineExternalCssClassName(attributes);
        return ((classValue != null) && classValue.contains(externalCssClassName));
    }

    /**
     * Determine name of class to mark links as external with, depending on the configuration
     * option to add CSS class suffixes
     * @param attributes
     * @return The class name that, if present, indicates that a links should be treated as external
     */
    private String determineExternalCssClassName(XMLAttributes attributes) {
        boolean addCssClassSuffix = Boolean.parseBoolean(preferences.getValue(R.PREFS.CLASS_REWRITE_ENABLED, null));
        String cssClassRewriteSuffix = preferences.getValue(R.PREFS.CLASS_SUFFIX, "");
        return (addCssClassSuffix && !"".equals(cssClassRewriteSuffix)) ? R.HTML.EXTERN + "_" + cssClassRewriteSuffix : R.HTML.EXTERN;
    }

    /**
	 * Returns true, if and only if {@code href} is a java script instruction,
	 * i.e. it starts with 'javascript:'.
	 */
	private boolean isJavaScriptLink(String href) {
		return href == null ? false : href.trim().toLowerCase().startsWith("javascript:");
	}
	
	/**
	 * returns true, if href is an anchor (#anchor)
	 */
	private boolean isAnchor(String href) {
		return href == null ? false : href.trim().toLowerCase().startsWith("#");
	}
	
	/**
	 * returns true, if href contains an anchor (url#anchor)
	 */
	private boolean containsAnchor(String href) {
		return href == null ? false : href.trim().toLowerCase().contains("#");
	}
	
	/**
	 * returns true, if href contains a mailto-attribute
	 */
	private boolean isMail(String href) {
		return href == null ? false : href.trim().toLowerCase().startsWith("mailto:");
	}
	
	/**
	 * @return anchor with suffix if a suffix is set and the ids or name-attributes are changed, 
	 * otherwise only the anchor is returned
	 */
	private String setSuffixToAnchor(String anchor) {
		
		//suffix should only be added, if ids or names of anchor-elements (<a>) are rewritten
		if (Boolean.parseBoolean(preferences.getValue(R.PREFS.ID_REWRITE_ENABLED, null)) || 
			Boolean.parseBoolean(preferences.getValue(R.PREFS.NAME_REWRITE_ENABLED, null))) {
		
			String suffix = preferences.getValue(R.PREFS.CLASS_SUFFIX, null).trim();
			
			if (suffix != null && !suffix.isEmpty()) {
				anchor = anchor + "_" + suffix;
			}
		}
		return anchor;
	}
	
}
