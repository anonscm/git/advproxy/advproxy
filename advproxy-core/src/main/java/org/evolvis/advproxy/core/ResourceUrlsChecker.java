package org.evolvis.advproxy.core;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * TODO
 * 
 * This Implementation is threadsafe!
 * 
 * @author Sabine Hoecher
 * @author Tino Rink
 */
public class ResourceUrlsChecker {

	private final List<Pattern> resourceUrlPatterns = new ArrayList<Pattern>();
	
	/**
	 * Create a new instance with the given resource url patterns.
	 * 
	 * @param resourceUrlSigns
	 */
	public ResourceUrlsChecker(String[] resourceUrlSigns) {
		for (final String resourceUrlSign : resourceUrlSigns) {
			final Pattern resourceUrlPattern = Pattern.compile(resourceUrlSign);
			resourceUrlPatterns.add(resourceUrlPattern);
		}
	}
	
	/**
	 * Returns true if the incoming Url is defined as resourceUrl
	 */
	public boolean isResourceUrl(String url) {
		for (final Pattern resourceUrlPattern : resourceUrlPatterns) {
			if (resourceUrlPattern.matcher(url).matches()) {
				return true;
			}
		}
		return false;
	}
	
}
