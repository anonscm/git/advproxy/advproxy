/*
 * advproxy Advanced servlet and portlet proxy Copyright (c) 2008 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License,version 2 as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.evolvis.advproxy.core.filters;

import java.io.IOException;
import java.io.Writer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.xni.Augmentations;
import org.apache.xerces.xni.QName;
import org.apache.xerces.xni.XMLAttributes;
import org.apache.xerces.xni.XNIException;
import org.cyberneko.html.filters.DefaultFilter;
import org.evolvis.advproxy.core.AllowedUrlsAndXPaths;
import org.evolvis.advproxy.core.PortletUrlRewriter;
import org.evolvis.advproxy.core.R;

/**
 * Handler for meta tags which could rewrite the current page via meta-refresh.
 * 
 * @author Timo Pick, tarent GmbH
 */
public class MetaRefreshFilter extends DefaultFilter {
			
	/** Used for rewriting URLs such that they can be opened in an advProxy. */
	protected PortletUrlRewriter urlRewriter;

	protected final Writer lazyInclude;
	
	protected AllowedUrlsAndXPaths allowedUrls;
	
	protected static Log log = LogFactory.getLog(MetaRefreshFilter.class.getName());

	/**
	 * @param lazyInclude
	 * @param xPathExpression
	 * @param convertRefreshToJS true, if a meta tag with http-equiv="refresh" should be converted to a java script reload.
	 * @param allowedUrls 
	 */
	public MetaRefreshFilter(Writer lazyInclude, AllowedUrlsAndXPaths allowedUrls, PortletUrlRewriter urlRewriter) {
		if (log.isTraceEnabled())
			log.trace("New HTMLStripper instance was created");
		this.lazyInclude = lazyInclude;
		this.urlRewriter = urlRewriter;
		this.allowedUrls = allowedUrls;
	}
	
	/**
	 * Search for an meta tag with refresh attribute. All other elements will
	 * be delegate via the super class.
	 */
	@Override
	public void startElement(QName element, XMLAttributes attributes, Augmentations augs) throws XNIException {
		if (R.HTML.META.equals(element.localpart)) {
			String http_equiv = attributes.getValue(R.HTML.HTTP_EQUIV);
			if (R.HTML.REFRESH.equals(http_equiv)) {
				String content = attributes.getValue(R.HTML.CONTENT);
				if (content != null) {
					int timeoutIntMs = -1;
					if (content.indexOf(';') > -1) {
						String timeoutStringOrg = content.substring(0, content.indexOf(';')).trim();
						// timeout in milliseconds 
						try  {
							timeoutIntMs = 1000 * Integer.parseInt(timeoutStringOrg);							
						} catch (NumberFormatException e) {
							timeoutIntMs = 1000;
							log.warn("Could not parse refresh time from http header. Page will not be refreshed.", e);
						}
					}
					int urlStartIndex = content.toLowerCase().indexOf(R.HTML.URL_);
					if (urlStartIndex != -1) {
						urlStartIndex += R.HTML.URL_.length();
						String newLocation = content.substring(urlStartIndex).trim();
						if (newLocation != null) {
							newLocation = newLocation.replace("'", "");
						}
						try {
							lazyInclude.write("<script type=\"text/javascript\">");
							lazyInclude.write("function orgEvolvisAdvProxyOpenUrl() {");
							newLocation = this.urlRewriter.simpleUpdateURL(newLocation);
							if (this.allowedUrls.isAllowedUrl(newLocation)) {
								newLocation = urlRewriter.updateLinkURL(newLocation);
								lazyInclude.write("  window.location.href='" + newLocation + "';");
							} else { // in a new window, otherwise
								lazyInclude.write("  " + "var newWindow = window.open('" + newLocation + "', '_blank');");
								lazyInclude.write("  " + "newWindow.focus();");
							}
							lazyInclude.write("}");
							lazyInclude.write("window.setTimeout(\"orgEvolvisAdvProxyOpenUrl()\", " + timeoutIntMs + ");");
							lazyInclude.write("</script>");	
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}
		super.startElement(element, attributes, augs);
	}
}
