/*
 * advproxy Advanced servlet and portlet proxy Copyright (c) 2008 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License,version 2 as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.evolvis.advproxy.core.filters;

import org.apache.xerces.xni.Augmentations;
import org.apache.xerces.xni.QName;
import org.apache.xerces.xni.XMLAttributes;
import org.apache.xerces.xni.XNIException;
import org.cyberneko.html.filters.DefaultFilter;
import org.evolvis.advproxy.core.PortletUrlRewriter;
import org.evolvis.advproxy.core.R;
import org.evolvis.advproxy.core.ResourceUrlsChecker;

/**
 * Form url rewrite filter.
 */
public class FormRewriter extends DefaultFilter {
	
	protected PortletUrlRewriter urlRewriter;

	private ResourceUrlsChecker resourceUrlsChecker;
	
	public FormRewriter(PortletUrlRewriter urlRewriter, ResourceUrlsChecker resourceUrlsChecker) {
		this.urlRewriter = urlRewriter;
		this.resourceUrlsChecker = resourceUrlsChecker;
	}

	/**
	 * Update the form url.
	 */
	@Override
	public void startElement(QName element, XMLAttributes attributes, Augmentations augs) throws XNIException {
		
		// if form element
		if (R.HTML.FORM.equals(element.localpart)) {

			final int actionIdx = attributes.getIndex(R.HTML.ACTION);
			final int methodIdx = attributes.getIndex(R.HTML.METHOD);
			final int acceptcharsetIdx = attributes.getIndex(R.HTML.ACCEPT_CHARSET);
			
			final String method = attributes.getValue(R.HTML.METHOD);
			
			String targetURL;			
			if (actionIdx != -1) {
				targetURL = attributes.getValue(actionIdx);
			} else {
				targetURL = "";
			}
			
			targetURL =  urlRewriter.simpleUpdateURL(targetURL);
			
			if (resourceUrlsChecker.isResourceUrl(targetURL)) {
				if (R.HTML.POST.equals(method)) {
					// build a "formPostURL" resource URL 
					targetURL = this.urlRewriter.createResourceRequestURL(targetURL, R.ACTION_FORMPOSTURL, urlRewriter.getBaseURL());
				}
				else {
					// build a "formGetURL" resource URL
					targetURL = this.urlRewriter.createResourceRequestURL(targetURL, R.ACTION_FORMGETURL, urlRewriter.getBaseURL());
				}
			}
			else {
				if (R.HTML.POST.equals(method)) {
					// build a "formPostURL" action URL 
					targetURL = this.urlRewriter.createActionRequestURL(R.ACTION_FORMPOSTURL, targetURL, urlRewriter.getBaseURL());
				}
				else {
					// build a "formGetURL" action URL
					targetURL = this.urlRewriter.createActionRequestURL(R.ACTION_FORMGETURL, targetURL, urlRewriter.getBaseURL());
				}
			}
			
			// set form action url e.g.: <form action='page base url' ...>
			if (actionIdx != -1) {
				attributes.setValue(actionIdx, targetURL);
			} else {
				attributes.addAttribute(new QName(null, R.HTML.ACTION, R.HTML.ACTION, null), R.HTML.CDATA, targetURL);				
			}
			
			// set from method to post e.g.: <form method='post' ...>
			if (methodIdx != -1) {
				attributes.setValue(methodIdx, R.HTML.POST);
			} else {
				attributes.addAttribute(new QName(null, R.HTML.METHOD, R.HTML.METHOD, null), R.HTML.CDATA, R.HTML.POST);		
			}
			
			// set the accepted-charset of html-page to utf-8
			if (acceptcharsetIdx != -1) {
				if (attributes.getValue(acceptcharsetIdx) != R.UTF_8) {
					attributes.setValue(acceptcharsetIdx, R.UTF_8);
				}
			} else {
				attributes.addAttribute(new QName(null, R.HTML.ACCEPT_CHARSET, R.HTML.ACCEPT_CHARSET, null), R.HTML.CDATA, R.UTF_8);
			}
		}
		
		// continue parsing the form element
		super.startElement(element, attributes, augs);
	}
}
