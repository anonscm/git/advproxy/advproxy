package org.evolvis.advproxy.core;

import java.io.IOException;
import java.io.InputStream;

import javax.portlet.ClientDataRequest;

import org.apache.commons.fileupload.RequestContext;

/**
 * Delegate/wrapping class which transform an
 * {@link ClientDataRequest} to an {@link RequestContext}!
 * For example it transport the content inputstream from
 * {@link ClientDataRequest#getPortletInputStream()} to
 * {@link RequestContext#getInputStream()}.
 */
public class AdvPortletRequestContext implements RequestContext {

    private ClientDataRequest request;

    /**
     * Constructor with the wrapped request.
     * 
     * @param request
     */
    public AdvPortletRequestContext(ClientDataRequest request) {
        this.request = request;
    }

    /**
     * Return the character encoding based on the {@link ClientDataRequest}.
     */
    @Override
    public String getCharacterEncoding() {
        return request.getCharacterEncoding();
    }

    /**
     * Return the content type based on the {@link ClientDataRequest}.
     */
    @Override
    public String getContentType() {
        return request.getContentType();
    }

    /**
     * Return the content length based on the {@link ClientDataRequest}.
     */
    @Override
    public int getContentLength() {
        return request.getContentLength();
    }

    /**
     * Return the content inputstream based on the {@link ClientDataRequest}.
     */
    @Override
    public InputStream getInputStream() throws IOException {
        return request.getPortletInputStream();
    }

    /**
     * ToString implementation for easier debugging.
     */
    @Override
    public String toString() {
        return "ContentLength=" + this.getContentLength() + ", ContentType=" + this.getContentType();
    }
}
