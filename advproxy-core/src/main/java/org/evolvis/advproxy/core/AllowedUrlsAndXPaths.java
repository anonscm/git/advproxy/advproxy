/*
 * advproxy Advanced servlet and portlet proxy Copyright (c) 2008 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License,version 2 as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.evolvis.advproxy.core;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.regex.Pattern;


/**
 * Holds a list of patterns to identify which urls are allowed to be displayed inside
 * this advProxy instance. For each entry a XPath expression associated
 * to allow URL pattern specific content selection.
 * 
 * @author Bahtiar Gadimov <bahtiar@gadimov.de>, Tim Steffens, tarent GmbH
 */
public class AllowedUrlsAndXPaths {

	public static final long serialVersionUID = -877062737964763669L;

	private final LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();

	/**
	 * Create an new url to xpath configuration pattern holder.
	 * 
	 * @param values
	 */
	public AllowedUrlsAndXPaths(String[] values) {
		if (values != null) {
			for (String urlAndXpath : values) {
				if (urlAndXpath.contains("|")) {
					String url = urlAndXpath.substring(0, urlAndXpath.lastIndexOf('|') );
					String xpath = urlAndXpath.substring(urlAndXpath.lastIndexOf('|')+1);
					this.map.put(url, xpath);
				} else {
					this.map.put(urlAndXpath, "//html/body/*");
				}
			}
		}
	}
	
	/**
	 * Return a sorted set of the urls "keys".
	 * 
	 * @return
	 */
	public Set<String> getUrls() {
		return this.map.keySet();
	}

	/**
	 * Return a sorted set of the xpath "values".
	 * 
	 * @return
	 */
	public Collection<String> getXPaths() {
		return this.map.values();
	}
	
	/**
	 * Return true, if and only if this URL can be matched with the pattern
	 * configured for valid URLs, i.e. the URL is allowed.
	 */
	public boolean isAllowedUrl(String url) {
		Set<String> set = this.map.keySet();
		for (String key : set) {
			if (Pattern.matches(key, url)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the XPath expression configured for {@code url} found by selecting
	 * the expression associated with the first matching url pattern in the
	 * allowed URL list.
	 */
	public String getXPath(String url) {
		Set<String> keySet = this.map.keySet();
		for (String key : keySet) {
			if (Pattern.matches(key, url)) {
				return map.get(key);
			}
		}
		return null;
	}
}
