package org.evolvis.advproxy.core;

/**
 * Default values for different global advproxy use cases.
 */
public class Default {
	/**
	 * Default connection timeout.
	 */
	public static final int CONNECTION_TIMEOUT = 10000;
	/**
	 * Default use proxy value (false).
	 */
	public static final String USE_PROXY = "false";
	/**
	 * Default proxy port (8080).
	 */
	public static final String PROXY_PORT = "8080";
	/**
	 * Default content charset encoding (UTF-8).
	 */
	public static final String CHARSET = "UTF-8";
	/**
	 * Default encoding option (detect automatically).
	 */
	public static final String ENCODING = "auto";
	/**
	 * Default for include script tags (false).
	 */
	public static final String INCLUDE_JAVASCRIPT = "false";
	/**
	 * Default for include style tags (true).
	 */
	public static final String INCLUDE_CSS = "true";
	/**
	 * Default for generate additional cookies (false).
	 */
	public static final String GENERATE_ADDITIONAL_COOKIES = "false";
	/**
	 * Default for sending uuid to the original page (false).
	 */
	public static final String SEND_USER_UUID = "false";
	/**
	 * Default for sending uuid to the original page (request name: UUID).
	 */
	public static final String USER_UUID_COOKIENAME = "UUID";
	/**
	 * Default for sending (append) uuid to resource urls (false).
	 */
	public static final String APPEND_USER_UUID_TO_RESOURCEURL = "false";
	/**
	 * Default user agent which will be send to the original page (Mozilla/5.0 AdvProxyPortlet).
	 */
	public static final String USER_AGENT = "Mozilla/5.0 AdvProxyPortlet";
}
