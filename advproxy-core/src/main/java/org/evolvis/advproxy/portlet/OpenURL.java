/*
 * advproxy
 * Advanced servlet and portlet proxy
 * Copyright (c) 2008 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
package org.evolvis.advproxy.portlet;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;


/**
 * With The OpenURL event will a proxy portlet coult send a URL to another
 * instance to show the &quot;new&quot; there.
 * 
 * <pre>
 * &lt;event-definition&gt;
 *   &lt;qname xmlns:event=&quot;http://advproxy.evolvis.org/&quot;&gt;event:OpenURL&lt;/qname&gt;
 * &lt;/event-definition&gt;
 * </pre>
 * 
 * @author Christoph Jerolimov, tarent GmbH
 */

@XmlRootElement
public class OpenURL implements Serializable {
    private static final long serialVersionUID = -1968894994279353048L;
    
	private String url;
	private String windowID;

	public OpenURL() {
		// nothing
	}
	
	public OpenURL(String url) {
		setUrl(url);
	}
	
	public OpenURL(String url, String windowID) {
		setUrl(url);
		setWindowID(windowID);
	}
	
	public void setUrl(String url) {
	    this.url = url;
    }
	
	public String getUrl() {
	    return url;
    }
	
	public String getWindowID() {
		return windowID;
	}

	public void setWindowID(String windowID) {
		this.windowID = windowID;
	}
}
