package org.evolvis.advproxy.portlet;

public enum HtmlHrefTarget {
	BLANK("_blank"),
	PARENT("_parent"),
	SELF("_self"),
	TOP("_top");
	
	private final String stringRepresentation;
	
	HtmlHrefTarget(String target) {
		stringRepresentation = target;
	}
	
	public String toString() {
		return stringRepresentation;
	}
}
