/*
 * advproxy Advanced servlet and portlet proxy Copyright (c) 2008 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License,version 2 as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.evolvis.advproxy.portlet;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletPreferences;
import javax.servlet.http.Cookie;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.xni.parser.XMLDocumentFilter;
import org.apache.xerces.xni.parser.XMLInputSource;
import org.cyberneko.html.HTMLConfiguration;
import org.evolvis.advproxy.core.AllowedUrlsAndXPaths;
import org.evolvis.advproxy.core.Default;
import org.evolvis.advproxy.core.PortletComponent;
import org.evolvis.advproxy.core.PortletUrlRewriter;
import org.evolvis.advproxy.core.R;
import org.evolvis.advproxy.core.ResourceUrlsChecker;
import org.evolvis.advproxy.core.filters.AdvWriter;
import org.evolvis.advproxy.core.filters.AttributeRewriter;
import org.evolvis.advproxy.core.filters.FormRewriter;
import org.evolvis.advproxy.core.filters.HTMLStripper;
import org.evolvis.advproxy.core.filters.ImgSrcRewritingFilter;
import org.evolvis.advproxy.core.filters.JavaScriptFilter;
import org.evolvis.advproxy.core.filters.LinkRewritingFilter;
import org.evolvis.advproxy.core.filters.MetaRefreshFilter;
import org.evolvis.advproxy.core.filters.PortletUrlRewritingFilter;

/**
 * A html client wrapper (without http communication) that could parse
 * (wrap cyberneko) and transform html content in context of the advproxy
 * portlet which requires valid xhtml content.
 */
public class HTMLClient implements PortletComponent {	

	protected final Log log = LogFactory.getLog(getClass());
	
	protected boolean ignoreSSLErrors = false;

	protected boolean includeJavaScript = true;
	
	protected String javaScriptExcludes;
	
	protected String finalRequestUrl;
	
	private String htmlResponse;
	
	private Cookie[] userCookies;
	
	protected AllowedUrlsAndXPaths allowedUrls;
	
	protected PortletUrlRewriter urlRewriter;
	
	protected ResourceUrlsChecker resourceUrlsChecker;
	
	protected boolean updateFormularFields;
	
	protected PortletPreferences preferences;

	/**
	 * Create a new instance of an html client to parse and transform the given
	 * html content. Holds a list of allowed urls and other configuration
	 * objects.
	 * 
	 * @param htmlResponse
	 * @param allowedUrls
	 * @param urlRewriter
	 * @param resourceUrlsChecker
	 * @throws IOException
	 */
	public HTMLClient(String htmlResponse, AllowedUrlsAndXPaths allowedUrls, PortletUrlRewriter urlRewriter, ResourceUrlsChecker resourceUrlsChecker) throws IOException {
		this.htmlResponse = htmlResponse;
		this.allowedUrls = allowedUrls;
		this.urlRewriter = urlRewriter;
		this.resourceUrlsChecker = resourceUrlsChecker;
	}
	
	public String getFinalRequestUrl() {
		return finalRequestUrl;
	}

	public void setFinalRequestUrl(String finalRequestUrl) {
		this.finalRequestUrl = finalRequestUrl;
	}

	public Cookie[] getUserCookies() {
		return userCookies;
	}

	public void setUserCookies(Cookie[] userCookies) {
		this.userCookies = userCookies;
	}

	/**
	 * Parse the html content and write the transformed html to the given
	 * writer.
	 * 
	 * @param writer
	 * @param writerCharacterEncoding
	 * @throws IOException
	 */
	public void parse(Writer writer, String writerCharacterEncoding) throws IOException {

		final Writer headInclude = new StringWriter();
		final Writer bodyInclude = new StringWriter();
		
		log.debug("advproxy will now include the source code of" + finalRequestUrl + " here");

		// setting cookies via PortletResponse or HttpServletResponse does not work
		// see http://www.liferay.com/de/community/wiki/-/wiki/Main/Portlet+to+Portlet+Communication
		// setting cookies from server response via JavaScript
		setCookiesFromServer(headInclude);
		
		// setup filterchain
		List<XMLDocumentFilter> filterChain = new ArrayList<XMLDocumentFilter>();
		filterChain.add(new MetaRefreshFilter(headInclude, allowedUrls, urlRewriter));

		JavaScriptFilter jsFilter = new JavaScriptFilter(headInclude, urlRewriter, resourceUrlsChecker);			
		jsFilter.setUp(preferences);
		filterChain.add(jsFilter);
		
		final HTMLStripper htmlClipper = new HTMLStripper(allowedUrls.getXPath(finalRequestUrl));
		filterChain.add(htmlClipper);
		
		final AttributeRewriter attributeFilter = new AttributeRewriter();
		attributeFilter.setUp(preferences);
		
		filterChain.add(attributeFilter);
		filterChain.add(new PortletUrlRewritingFilter(urlRewriter));
		filterChain.add(new FormRewriter(urlRewriter, resourceUrlsChecker));
		filterChain.add(new ImgSrcRewritingFilter(urlRewriter, resourceUrlsChecker));
		filterChain.add(new LinkRewritingFilter(urlRewriter, resourceUrlsChecker, allowedUrls, preferences));
		filterChain.add(new AdvWriter(bodyInclude, writerCharacterEncoding));
//		filterChain.add(new org.cyberneko.html.filters.Writer(bodyInclude, writerCharacterEncoding));


		final HTMLConfiguration parser = new HTMLConfiguration();
		parser.setProperty("http://cyberneko.org/html/properties/filters", filterChain.toArray(new XMLDocumentFilter[filterChain.size()]));
		
		final XMLInputSource source = new XMLInputSource(null, "http://advproxy.evolvis.org", null, new StringReader(htmlResponse), R.UTF_8);
		parser.parse(source);
		
		writer.write("<!-- advproxy will include " + finalRequestUrl + " here... -->");
		writer.write("\n<div class=\"__advproxy\">");
		
		writer.write(headInclude.toString());
		
		writer.write(bodyInclude.toString());
		
		writer.write("\n</div>");
		writer.write("<!-- advproxy finish include " + finalRequestUrl + " here! -->");
	}


	public void setUp(PortletPreferences preferences) {
		this.preferences = preferences;
		this.includeJavaScript = Boolean.parseBoolean(preferences.getValue(R.PREFS.INCLUDE_JAVASCRIPT, Default.INCLUDE_JAVASCRIPT));
		this.javaScriptExcludes = preferences.getValue(R.PREFS.JAVA_SCRIPT_EXCLUDES, null);
		this.updateFormularFields = Boolean.valueOf(preferences.getValue(R.PREFS.UPDATE_FORMULAR_FIELDS, null));
	}
	
//	public static byte[] getBytes(InputStream inputStream, int length) throws IOException {
//        byte[] data = new byte[length];
//        inputStream.read(data);
//        inputStream.close();
//        return data;
//	}

	private void setCookiesFromServer(Writer writer) throws IOException {
		if (userCookies != null && userCookies.length > 0) {
			writer.append("<script type=\"text/javascript\">");
			for (Cookie aCookie : userCookies) {
				writer.append("document.cookie=\"" + aCookie.getName() + "=" + aCookie.getValue() + "\";\n");
			}
			writer.append("</script>");
		}
	}
}
