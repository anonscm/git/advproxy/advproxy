package org.evolvis.advproxy.portlet.controller;

import java.net.URI;

import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;

import org.apache.http.cookie.Cookie;
import org.evolvis.advproxy.core.AdvHttpResponse;
import org.evolvis.advproxy.core.HttpMethodeType;
import org.evolvis.advproxy.core.NewHttpManager;
import org.evolvis.advproxy.core.R;
import org.evolvis.advproxy.core.RequestResultType;
import org.evolvis.advproxy.portlet.OpenURL;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;

@Controller("EventMappingController")
@RequestMapping(value="VIEW")
public class EventMappingController extends AbstractController {

	@EventMapping
	public void processEvent(EventRequest request, EventResponse response) throws Exception {

		String charenc = extractCharenc(request);
		
		// get event
		Event event = request.getEvent();

		if (R.OPEN_URL_EVENT_QNAME.equals(event.getQName()) && isUseInterportletCommunication(request.getPreferences())) {
			OpenURL openUrlEvent = (OpenURL) event.getValue();
			
			// get event values
			final String url = openUrlEvent.getUrl();
			String windowId = openUrlEvent.getWindowID();

			log.debug("processEvent OpenURL: " + url);

			if (windowId != null && windowId.equals(request.getWindowID()) && url != null && url.trim().length() != 0) {
				
				final URI targetURI = URI.create(url);
				
				final NewHttpManager httpManager = getDefaultHttpManager(request, targetURI);
				
				AdvHttpResponse method = null;
				try {
					method = httpManager.processUrl(HttpMethodeType.GET, charenc);
					
					if (targetURI != null) {
						response.setRenderParameter(R.PORTLET_URL_PARAM_NAME, targetURI.toString());						
					}
					response.setRenderParameter(R.PORTLET_ACTION, R.ACTION_OPENURL);
					response.setRenderParameter(R.STATUS_CODE, String.valueOf(method.getStatusCode()));
					if (method.getFinalRequestUri() != null) {
						response.setRenderParameter(R.FINAL_REQUEST_URI, method.getFinalRequestUri().toString());						
					}
					if (method.getResultType() != null) {
						response.setRenderParameter(R.REQUEST_RESULT_TYPE, method.getResultType().toString());						
					}
					
					for (Cookie cookie : method.getCookies()) {
						response.addProperty(NewHttpManager.transformFromApacheToServletCookie(getPortletRequestBaseUrl(request), cookie));
					}
					
					if (RequestResultType.TEXT.equals(method.getResultType())) {
						String responseBody = method.getResponseBodyAsString();
						if (responseBody != null) {
							response.setRenderParameter(R.REQUEST_RESULT_HTML, responseBody);							
						}
					}
				}
				finally {
					if (method != null) method.freeResources();
				}
			}
		} 
	}
}
