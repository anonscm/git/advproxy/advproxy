package org.evolvis.advproxy.portlet.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletURL;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceURL;
import javax.portlet.ValidatorException;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;

import javax.validation.Valid;

import org.evolvis.advproxy.core.R;
import org.evolvis.advproxy.portlet.ProxyPreferences;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * Controller for preferences in edit-mode
 * @author Sabine Höcher
 */
@Controller
@RequestMapping("EDIT")
public class EditController extends AbstractController {
	
	public static final String TARGETURL_PLACE_HOLDER = "TARGETURL";

	@RenderMapping
	public String viewEditMode(RenderRequest request, RenderResponse response, PortletPreferences preferences, ModelMap model)
			throws PortletModeException, ReadOnlyException, ValidatorException, WindowStateException, IOException {
		
		PortletURL renderUrl = response.createActionURL();
		renderUrl.setParameter(ActionRequest.ACTION_NAME, "savePreferences");
		model.addAttribute("renderUrl", renderUrl.toString());
		
		if (!model.containsKey("proxypreferences")) {
			model.addAttribute("proxypreferences", loadPreferences(preferences, request));
		}
		
		// Templates 
		model.addAttribute(R.PREFS.OPEN_URL_TEMPLATE_PARAMETER_NAME, addURLTemplateOpenUrl(request, response));		
		model.addAttribute(R.PREFS.FORM_POST_URL_TEMPLATE_PARAMETER_NAME, addURLTemplateFormPostUrl(request, response));
		model.addAttribute(R.PREFS.RESOURCE_GET_URL_TEMPLATE_PARAMETER_NAME, addURLTemplateResourceGetUrl(request, response));
		model.addAttribute(R.PREFS.RESOURCE_POST_URL_TEMPLATE_PARAMETER_NAME, addURLTemplateResourcePostUrl(request, response));
		
		return "edit";		
	}

	@ActionMapping(value = "savePreferences")
	public void handleEditRenderRequest(PortletPreferences preferences, 
			@ModelAttribute("proxypreferences") @Valid ProxyPreferences proxypreferences, Errors errors)
		throws PortletModeException, ReadOnlyException, ValidatorException, IOException, WindowStateException {

		if (!errors.hasErrors()) {
			setPreferences(preferences, proxypreferences);
		} 
	}

	/**
	 * gets the preferences for edit-mode
	 */
	private ProxyPreferences loadPreferences(PortletPreferences preferences, RenderRequest renderRequest) {
		ProxyPreferences p = new ProxyPreferences();

		// Basic Options
		p.setHomepage(preferences.getValue(R.PREFS.HOMEPAGE, null));
		p.setAllowedURLs(ArrayToString(preferences.getValues(R.PREFS.ALLOWED_URLS, new String[] {})));
		p.setForceNewWindowOnNotAllowedURLs(Boolean.parseBoolean(preferences.getValue(R.PREFS.FORCE_NEW_WINDOW, null)));
		p.setCheckResourceUrls(ArrayToString(preferences.getValues(R.PREFS.CHECK_RESOURCE_URLS, new String[] {})));

		// User Interface
		p.setMinHeight(preferences.getValue(R.PREFS.MIN_HEIGHT, null));
		p.setMaxHeight(preferences.getValue(R.PREFS.MAX_HEIGHT, null));
		p.setShowAddressbar(Boolean.parseBoolean(preferences.getValue(R.PREFS.SHOW_ADDRESSBAR, null)));
		p.setAddressEditable(Boolean.parseBoolean(preferences.getValue(R.PREFS.IS_ADDRESS_EDITABLE, null)));
		p.setShowGoHomeButtonInAddressbar(Boolean.parseBoolean(preferences.getValue(R.PREFS.SHOW_GO_HOME_BUTTON, null)));

		// HTML
		p.setIncludeJavascript(Boolean.parseBoolean(preferences.getValue(R.PREFS.INCLUDE_JAVASCRIPT, null)));
		p.setExcludedJavascriptURLs(ArrayToString(preferences.getValues(R.PREFS.JAVA_SCRIPT_EXCLUDES, new String[] {})));
		p.setGenerateCookies(Boolean.parseBoolean(preferences.getValue(R.PREFS.GENERATE_ADDITIONAL_COOKIES, null)));
		p.setEnableUserUUID(Boolean.parseBoolean(preferences.getValue(R.PREFS.SEND_USER_UUID, null)));
		p.setUUIDCookieName(preferences.getValue(R.PREFS.USER_UUID_COOKIENAME, null));
		p.setAppendUserUUIDToResourceURL(Boolean.parseBoolean(preferences.getValue(R.PREFS.APPEND_USER_UUID_TO_RESOURCEURL, null)));
		p.setUpdateFormFields(Boolean.parseBoolean(preferences.getValue(R.PREFS.UPDATE_FORMULAR_FIELDS, null)));
		p.setClassRewriteEnabled(Boolean.parseBoolean(preferences.getValue(R.PREFS.CLASS_REWRITE_ENABLED, null)));
		p.setIdRewriteEnabled(Boolean.parseBoolean(preferences.getValue(R.PREFS.ID_REWRITE_ENABLED, null)));
		p.setNameRewriteEnabled(Boolean.parseBoolean(preferences.getValue(R.PREFS.NAME_REWRITE_ENABLED, null)));
		p.setClassRewriteForced(Boolean.parseBoolean(preferences.getValue(R.PREFS.CLASS_REWRITE_FORCED, null)));
		p.setClassSuffix(preferences.getValue(R.PREFS.CLASS_SUFFIX, null));

		//Misc
		p.setEnableProxy(Boolean.parseBoolean(preferences.getValue(R.PREFS.USE_PROXY, null)));
		p.setProxyHost(preferences.getValue(R.PREFS.PROXY_HOST, null));
		p.setProxyPort(preferences.getValue(R.PREFS.PROXY_PORT, null));
		p.setInterportletCommunication(Boolean.parseBoolean(preferences.getValue(R.PREFS.USE_INTERPORTLET_COMMUNICTION, null)));
		p.setPortletWindowId(renderRequest.getWindowID());
		p.setLinkedWindowIds(ArrayToString(preferences.getValues(R.PREFS.LINKED_WINDOW_IDS, new String[]{})));
		p.setUserAgent(preferences.getValue(R.PREFS.USER_AGENT, null));
		
		return p;
	}

	/**
	 * sets the new preferences changed in edit-mode
	 */
	private void setPreferences(PortletPreferences preferences, ProxyPreferences proxy) throws ReadOnlyException, ValidatorException, IOException {
		
		//Basic Options
		preferences.setValue(R.PREFS.HOMEPAGE, proxy.getHomepage());
		preferences.setValues(R.PREFS.ALLOWED_URLS, splitLines(proxy.getAllowedURLs()));		
		preferences.setValue(R.PREFS.FORCE_NEW_WINDOW, Boolean.toString(proxy.getForceNewWindowOnNotAllowedURLs()));		
		preferences.setValues(R.PREFS.CHECK_RESOURCE_URLS, splitLines(proxy.getCheckResourceUrls()));
		
		//User Interface
		preferences.setValue(R.PREFS.MIN_HEIGHT, proxy.getMinHeight());		
		preferences.setValue(R.PREFS.MAX_HEIGHT, proxy.getMaxHeight());	
		preferences.setValue(R.PREFS.SHOW_ADDRESSBAR, Boolean.toString(proxy.getShowAddressbar()));		
		preferences.setValue(R.PREFS.IS_ADDRESS_EDITABLE, Boolean.toString(proxy.getAddressEditable()));	
		preferences.setValue(R.PREFS.SHOW_GO_HOME_BUTTON, Boolean.toString(proxy.getShowGoHomeButtonInAddressbar()));
		
		//HTML
		preferences.setValue(R.PREFS.INCLUDE_JAVASCRIPT, Boolean.toString(proxy.getIncludeJavascript()));
		preferences.setValues(R.PREFS.JAVA_SCRIPT_EXCLUDES, splitLines(proxy.getExcludedJavascriptURLs()));
		preferences.setValue(R.PREFS.GENERATE_ADDITIONAL_COOKIES, Boolean.toString(proxy.getGenerateCookies()));
		preferences.setValue(R.PREFS.SEND_USER_UUID, Boolean.toString(proxy.getEnableUserUUID()));
		preferences.setValue(R.PREFS.USER_UUID_COOKIENAME, proxy.getUUIDCookieName());
		preferences.setValue(R.PREFS.APPEND_USER_UUID_TO_RESOURCEURL, Boolean.toString(proxy.getAppendUserUUIDToResourceURL()));
		preferences.setValue(R.PREFS.UPDATE_FORMULAR_FIELDS, Boolean.toString(proxy.getUpdateFormFields()));
		preferences.setValue(R.PREFS.CLASS_REWRITE_ENABLED, Boolean.toString(proxy.getClassRewriteEnabled()));
		preferences.setValue(R.PREFS.ID_REWRITE_ENABLED, Boolean.toString(proxy.getIdRewriteEnabled()));
		preferences.setValue(R.PREFS.NAME_REWRITE_ENABLED, Boolean.toString(proxy.getNameRewriteEnabled()));
		preferences.setValue(R.PREFS.CLASS_REWRITE_FORCED, Boolean.toString(proxy.getClassRewriteForced()));
		preferences.setValue(R.PREFS.CLASS_SUFFIX, proxy.getClassSuffix());
		
		//Misc
		preferences.setValue(R.PREFS.USE_PROXY, Boolean.toString(proxy.getEnableProxy()));
		preferences.setValue(R.PREFS.PROXY_HOST, proxy.getProxyHost());
		preferences.setValue(R.PREFS.PROXY_PORT, proxy.getProxyPort());
		preferences.setValue(R.PREFS.USE_INTERPORTLET_COMMUNICTION, Boolean.toString(proxy.getInterportletCommunication()));
		preferences.setValue(R.PREFS.PORTLET_WINDOW_ID, proxy.getPortletWindowId());
		preferences.setValues(R.PREFS.LINKED_WINDOW_IDS, splitLines(proxy.getLinkedWindowIds()));
		preferences.setValue(R.PREFS.USER_AGENT, proxy.getUserAgent());
		
		preferences.store();
	}
	
	/**
	 * Creates Template for action urls (openUrl).
	 */
	private String addURLTemplateOpenUrl(RenderRequest request, RenderResponse response) 
			throws PortletModeException, WindowStateException {
		
		final PortletURL openUrl = response.createActionURL();
		openUrl.setWindowState(WindowState.NORMAL);
		openUrl.setPortletMode(PortletMode.VIEW);
		openUrl.setParameter(R.JAVAX_PORTLET_ACTION, R.ACTION_OPENURL);
		openUrl.setParameter(R.PORTLET_URL_PARAM_NAME, TARGETURL_PLACE_HOLDER);
		return fixUrlTemplatesForWSRP(openUrl.toString());
	}
	
	/**
	 * Creates Template for action urls (oformPostUrl).
	 */
	private String addURLTemplateFormPostUrl(RenderRequest request, RenderResponse response) 
			throws PortletModeException, WindowStateException {
		
		final PortletURL formPostUrl = response.createActionURL();
		formPostUrl.setWindowState(WindowState.NORMAL);
		formPostUrl.setPortletMode(PortletMode.VIEW);
		formPostUrl.setParameter(R.JAVAX_PORTLET_ACTION, R.ACTION_FORMPOSTURL);
		formPostUrl.setParameter(R.PORTLET_URL_PARAM_NAME, TARGETURL_PLACE_HOLDER);
		return fixUrlTemplatesForWSRP(formPostUrl.toString());
	}	
	
	/**
	 * Creates Template for resource urls (getUrl).
	 */
	private String addURLTemplateResourceGetUrl(RenderRequest request, RenderResponse response) 
			throws PortletModeException, WindowStateException {
		
		final ResourceURL resourceGetUrl = response.createResourceURL();
		resourceGetUrl.setParameter(R.PORTLET_RESOURCE_ID_PARAM_NAME, R.ACTION_FORMGETURL);
		resourceGetUrl.setParameter(R.PORTLET_URL_PARAM_NAME, TARGETURL_PLACE_HOLDER);
		return fixUrlTemplatesForWSRP(resourceGetUrl.toString());
	}		
	
	/**
	 * Creates Template for resource urls (postUrl).
	 */
	private String addURLTemplateResourcePostUrl(RenderRequest request, RenderResponse response) 
			throws PortletModeException, WindowStateException {
		
		final ResourceURL resourcePostUrl = response.createResourceURL();
		resourcePostUrl.setParameter(R.PORTLET_RESOURCE_ID_PARAM_NAME, R.ACTION_FORMPOSTURL);
		resourcePostUrl.setParameter(R.PORTLET_URL_PARAM_NAME, TARGETURL_PLACE_HOLDER);
		return fixUrlTemplatesForWSRP(resourcePostUrl.toString());
	}	


	private String ArrayToString(String[] array) {
		String StringOfArray = "";
		if (array != null) {
			for (int i = 0; i < array.length; i++) {
				StringOfArray = StringOfArray + array[i] + "\r\n";
			}
		}
		return StringOfArray;
	}

	/**
	 * Splits {@code in} at the line breaks, removes those lines only
	 * consisting of white spaces and returns an array containing
	 * the remaining lines (in the order they occurred in the String).
	 */
	private String[] splitLines(String in) {
		if (in == null || in.length() == 0)
			return null;
		List<String> out = new ArrayList<String>(Arrays.asList(in.split("[\r\n]")));
		for (Iterator<String> it = out.iterator(); it.hasNext();)
			if (it.next().trim().length() == 0)
				it.remove();
		if (out.isEmpty())
			return null;
		return out.toArray(new String[out.size()]);
	}
	
	/**
	 * fixes the templates for WSRP which turns viewmode to editmode and inserts a portal_server_url
	 */
	private String fixUrlTemplatesForWSRP(String template) {
		
		if (template.contains("p_p_mode=edit")) {
			template = template.replace("p_p_mode=edit", "p_p_mode=view");
		}
		if (template.contains("portal_server_url")) {
			String secondPart = template.substring(template.lastIndexOf("portal_server_url"));
			template = template.replace(secondPart.subSequence(0, secondPart.indexOf("&")+1), "");
		}
		
		return template;
	}
}
