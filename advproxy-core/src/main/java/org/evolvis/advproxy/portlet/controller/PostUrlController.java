package org.evolvis.advproxy.portlet.controller;

import java.net.URI;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.apache.http.cookie.Cookie;
import org.evolvis.advproxy.core.AdvHttpResponse;
import org.evolvis.advproxy.core.HttpMethodeType;
import org.evolvis.advproxy.core.NewHttpManager;
import org.evolvis.advproxy.core.R;
import org.evolvis.advproxy.core.RequestResultType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;

@Controller("PostUrlController")
@RequestMapping(value="VIEW")
public class PostUrlController extends AbstractController {
	
	@ActionMapping(params={R.JAVAX_PORTLET_ACTION + "=" + R.ACTION_FORMPOSTURL})
	public void formPostURL(ActionRequest request, ActionResponse response) throws Exception {
		
		String url = getUrlFromRequest(request);
		String referer = request.getParameter(R.PORTLET_REFERER_PARAM_NAME);	
		String charenc = extractCharenc(request);

		final Map<String, String[]> params = getNonPortletSpecificParameter(request);
		
		final NewHttpManager httpManager = getDefaultHttpManager(request, URI.create(url));
		
		if (isMultipartRequest(request)) {
			setupMultipartRequest(request, httpManager);
		}
		
		AdvHttpResponse method = null;
		try {
			method = httpManager.processUrl(HttpMethodeType.POST, charenc);
		
			response.setRenderParameter(R.PORTLET_URL_PARAM_NAME, url);	
			if (referer != null) {
				response.setRenderParameter(R.PORTLET_REFERER_PARAM_NAME, referer);				
			}
			response.setRenderParameter(R.HTTP_REQUEST_METHOD, R.HTML.POST);
			response.setRenderParameter(R.PORTLET_ACTION, R.ACTION_FORMPOSTURL);
			response.setRenderParameters(params);
			
			response.setRenderParameter(R.STATUS_CODE, String.valueOf(method.getStatusCode()));
			if (method.getFinalRequestUri() != null) {
				response.setRenderParameter(R.FINAL_REQUEST_URI, method.getFinalRequestUri().toString());				
			}
			if (method.getResultType() != null) {
				response.setRenderParameter(R.REQUEST_RESULT_TYPE, method.getResultType().toString());				
			}
			
			for (Cookie cookie : method.getCookies()) {
				response.addProperty(NewHttpManager.transformFromApacheToServletCookie(getPortletRequestBaseUrl(request), cookie));
			}
	
			if (RequestResultType.TEXT.equals(method.getResultType())) {
				String responseBody = method.getResponseBodyAsString();
				if (responseBody != null) {
					response.setRenderParameter(R.REQUEST_RESULT_HTML, responseBody);
				}
			}
		}
		finally {
			if (method != null) method.freeResources();
		}
	}
	
	private Map<String, String[]> getNonPortletSpecificParameter(	ActionRequest request) {
		String paramName;
		Enumeration<String> names = request.getParameterNames();
		Map<String, String[]> params = new LinkedHashMap<String, String[]>();
		while (names.hasMoreElements()) {
			paramName = names.nextElement();
			if (!(paramName.startsWith(R.JAVAX_PORTLET_PREFIX) || paramName.startsWith(R.ADVPROXY_PORTLET_PREFIX))) {
				params.put(paramName, request.getParameterValues(paramName));
			}
		}
		return params;
	}
}
