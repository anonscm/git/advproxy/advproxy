package org.evolvis.advproxy.portlet.controller;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.MessageFormat;

import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceURL;

import org.apache.http.HttpException;
import org.apache.http.HttpStatus;
import org.evolvis.advproxy.core.AllowedUrlsAndXPaths;
import org.evolvis.advproxy.core.HttpMethodeType;
import org.evolvis.advproxy.core.PortletUrlRewriter;
import org.evolvis.advproxy.core.R;
import org.evolvis.advproxy.core.RequestResultType;
import org.evolvis.advproxy.core.ResourceUrlsChecker;
import org.evolvis.advproxy.core.AdvHttpResponse;
import org.evolvis.advproxy.portlet.Config;
import org.evolvis.advproxy.portlet.HTMLClient;
import org.evolvis.advproxy.portlet.HtmlHrefTarget;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * Portlet controller for the normal "adv portlet" which renders a "browser
 * content" with url, home and go button and mainly the 3rd party web content.
 */
@Controller("ViewController")
@RequestMapping(value="VIEW")
public class ViewController extends AbstractController {
	
	public static final String REQ_PARAM_REQUEST_ERROR = "requestError";
	public static final String REQ_PARAM_IS_TEXT_CONTENT = "isTextContent";
	public static final String REQ_PARAM_TEXT_CONTENT = "textContent";
	public static final String REQ_PARAM_HTTP_STATUS_NOT_OK = "HttpStatusNotOK";
	public static final String REQ_PARAM_METHOD_FAILED = "methodFailed";
	public static final String REQ_PARAM_IS_URL_NOT_ALLOWED = "isURLNotAllowed";
	public static final String REQ_PARAM_URL_NOT_ALLOWED_MESSAGE = "URLNotAllowedMessage";
	public static final String REQ_PARAM_ALLOWED_ARE_MESSAGE = "allowedAreMessage";
	public static final String REQ_PARAM_ALLOWED_URLS = "allowedURLs";
	public static final String REQ_PARAM_TEXT_CONTENT_EXCEPTION_STACKTRACE = "textContentExceptionStackTrace";
	public static final String REQ_PARAM_FILE_LINK = "fileLink";
	public static final String REQ_PARAM_OPEN_EXTERNAL_MESSAGE = "openExternalMessage";
	public static final String REQ_PARAM_IS_ATTACHMENT = "isAttachment";

	/**
	 * Handles the view request.
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @param session
	 * @param statusCode
	 * @param finalRequestUrl
	 * @param resultType
	 * @param resultHtml
	 * @return
	 * @throws Exception
	 */
	@RenderMapping
	public ModelAndView handleRenderRequest(RenderRequest request, RenderResponse response, ModelMap model, PortletSession session,
			@RequestParam(value = R.STATUS_CODE, required = false) Integer statusCode, 
			@RequestParam(value = R.FINAL_REQUEST_URI, required = false) String finalRequestUrl,
			@RequestParam(value = R.REQUEST_RESULT_TYPE, required = false) RequestResultType resultType,
			@RequestParam(value = R.REQUEST_RESULT_HTML, required = false) String resultHtml) 
			throws Exception {
		
		ModelAndView mav = new ModelAndView("view");

		Config conf = new Config(RB_PREFIX, request.getLocale());
		
		// if render-phase was called without action or some params are missing
		if (statusCode == null || finalRequestUrl == null || resultType == null) {
			
			AdvHttpResponse httpResponse = null;
			try {
				httpResponse = getMethodUrlForNoAction(request);
				statusCode = httpResponse.getStatusCode();
				finalRequestUrl = httpResponse.getFinalRequestUri().toString();
				resultType = httpResponse.getResultType();
				if (RequestResultType.TEXT.equals(httpResponse.getResultType())) {
					resultHtml = httpResponse.getResponseBodyAsString();
				}
			}
			finally {
				if (httpResponse != null) httpResponse.freeResources();
			}
		}
		
		// set url to view in addressbar
		request.setAttribute(R.PORTLET_URL_PARAM_NAME, finalRequestUrl);

		AllowedUrlsAndXPaths allowedUrls = getAllowedUrls(request);
		
		if (statusCode != HttpStatus.SC_OK) { //an error occurred
			String statusErrorText = createStatusErrorTextResponse(request, response, finalRequestUrl, resultHtml, allowedUrls);
			handleHttpStatusNotOk(mav, statusCode, conf, finalRequestUrl, resultHtml, statusErrorText);
		}
		else if (!allowedUrls.isAllowedUrl(finalRequestUrl)) { //status ok, but url is not allowed
			handleUrlNotAllowed(mav, conf, finalRequestUrl, request, response);
		}
		else { //status ok, url allowed
			log.debug("Returned page is internal.");
			
			if (RequestResultType.TEXT.equals(resultType)) {
				createTextResponse(mav, request, response, finalRequestUrl, resultHtml, allowedUrls);
			}
			else { // handle binary file types by presenting a link
				createResourceUrlForBinaryFile(mav, response, finalRequestUrl, conf, resultType);
			}
		}
		
		return mav;
	}
	
	/**
	 * Returns the default page that is defined via edit mode
	 */
	private AdvHttpResponse getMethodUrlForNoAction(RenderRequest request)
		throws HttpException, IOException {
		
		// get url from preferences
		final URI defaultURL = URI.create(request.getPreferences().getValue(R.PREFS.HOMEPAGE, null)); // no encoding here is utf8 anyway
		
		return getDefaultHttpManager(request, defaultURL).processUrl(HttpMethodeType.GET, R.UTF_8);
	}

	private void handleHttpStatusNotOk(ModelAndView mav, int statusCode, Config conf, String url, String resultHtml, String statusErrorText) {
		mav.addObject(REQ_PARAM_HTTP_STATUS_NOT_OK, Boolean.TRUE);
		
		log.error("HTTP status is not " + HttpStatus.SC_OK + ", was: " + statusCode, null);
		
		mav.addObject(REQ_PARAM_REQUEST_ERROR, conf.REQUEST_ERROR);
		mav.addObject(REQ_PARAM_METHOD_FAILED, MessageFormat.format(conf.METHOD_FAIL, statusCode, url));
		mav.addObject(REQ_PARAM_TEXT_CONTENT, createStatusCodeNotOk(statusCode, resultHtml, conf) + statusErrorText);
		mav.addObject(REQ_PARAM_IS_TEXT_CONTENT, Boolean.TRUE);

	}

	private void handleUrlNotAllowed(ModelAndView mav, Config conf, String url, RenderRequest request, RenderResponse response) {
		mav.addObject(REQ_PARAM_IS_URL_NOT_ALLOWED, Boolean.TRUE);
		
		log.debug("Returned page is external.");

		String formattedUrlstub = MessageFormat.format(conf.URL_NOT_ALLOWED, createHtmlLink(null, url, HtmlHrefTarget.BLANK, url));
		mav.addObject(REQ_PARAM_URL_NOT_ALLOWED_MESSAGE, formattedUrlstub);
		mav.addObject(REQ_PARAM_ALLOWED_ARE_MESSAGE, conf.URL_ALLOWED_ARE);
		mav.addObject(REQ_PARAM_ALLOWED_URLS, getAllowedURLString(request, response));
	}

	private void createTextResponse(ModelAndView mav, RenderRequest request, RenderResponse response, String finalRequestUrl, String htmlResponse, AllowedUrlsAndXPaths allowedUrls) 
			throws Exception {
		
		PortletUrlRewriter urlRewriter = new PortletUrlRewriter(
				request,
				response,
				finalRequestUrl,
				getAppendUserUUIDResourceURLParameterName(request));
		HTMLClient pc = new HTMLClient(
				htmlResponse,
				allowedUrls,
				urlRewriter,
				getResourceUrlsChecker(request));
		
		pc.setFinalRequestUrl(finalRequestUrl);
		pc.setUserCookies(request.getCookies());
		pc.setUp(request.getPreferences());
		StringWriter stringWriter = new StringWriter();
		pc.parse(stringWriter, R.UTF_8);
		String resultTextContent = stringWriter.toString();
		
//		final String resultTextContent = request.getParameter(R.RESPONSE_CONTENT);
		
		mav.addObject(REQ_PARAM_TEXT_CONTENT, resultTextContent);
		mav.addObject(REQ_PARAM_IS_TEXT_CONTENT, Boolean.TRUE);
	}

	private void createResourceUrlForBinaryFile(ModelAndView mav, RenderResponse response, String finalRequestUrl, Config conf, RequestResultType resultType) {
		
		final ResourceURL fileLink = response.createResourceURL();
		fileLink.setResourceID(R.ACTION_FORMGETURL);
		fileLink.setParameter(R.PORTLET_URL_PARAM_NAME, finalRequestUrl);

		String urlstub = createHtmlLink(R.PORTLET_BYNARY_FILE_RESOURCE_URL_NAME, fileLink.toString(), null, conf.REQUEST_URL);
		String formattedUrlstub = MessageFormat.format(conf.OPEN_EXTERNAL, urlstub);
		
		mav.addObject(REQ_PARAM_FILE_LINK, fileLink.toString());
		mav.addObject(REQ_PARAM_OPEN_EXTERNAL_MESSAGE, formattedUrlstub);
		mav.addObject(REQ_PARAM_IS_ATTACHMENT, RequestResultType.BINARY_ATTACHEMENT.equals(resultType));
	}

	private String getAllowedURLString(RenderRequest request, RenderResponse response) {
		String allowedURLString = "";
		String allowedURLs[] = request.getPreferences().getValues(R.PREFS.ALLOWED_URLS, null);
		
		if (allowedURLs == null || allowedURLs.length < 1) {
			return allowedURLString;
		}
			
		allowedURLString +="<ul>";
		for (String regex : allowedURLs) {
			allowedURLString +="<li><em>";
			allowedURLString +=response.encodeURL(regex);
			allowedURLString +="</em></li>";
		}
		allowedURLString +="</ul>";
			
		return allowedURLString;
	}
	/**
	 * Creates a anchor string containing href that acts as a link.
	 * It has the following format: <a id="id" href="href" target="target">text</a>.
	 * @param id If not null the id attribute of the link will be set.
	 * @param href hyper reference
	 * @param target if not null, the target attribute of the link will be set.
	 * @param text
	 * @return
	 */
	private String createHtmlLink(String id, String href, HtmlHrefTarget target, String text) {
		String link = "<a ";
		
		if( null != id && id.trim().length() > 0 ) {
			link += "id=\"" + id + "\" ";
		}

		link += "href=\"" + href +"\" ";
		
		if( null != target ) {
			link += "target=\"" + target + "\" ";
		}
		
		link += ">" + text + "</a>";
		
		return link;
	}
	
	private String createStatusCodeNotOk(Integer statusCode, String resultHtml, Config conf) {
		return "<div class=\"portlet-msg-error\"><p> " + conf.STATUS_CODE + ": " + statusCode + " </div>";
		
	}
	
	private String createStatusErrorTextResponse(RenderRequest request, RenderResponse response, String finalRequestUrl, String htmlResponse, AllowedUrlsAndXPaths allowedUrls) 
			throws Exception {
		
		PortletUrlRewriter urlRewriter = new PortletUrlRewriter(
				request,
				response,
				finalRequestUrl,
				getAppendUserUUIDResourceURLParameterName(request));
		String checkResourceUrls[] = request.getPreferences().getValues(R.PREFS.CHECK_RESOURCE_URLS, new String[]{});
		
		if( null == checkResourceUrls ) {
			checkResourceUrls = new String[]{};
		}
		
		final ResourceUrlsChecker resourceUrlsChecker = new ResourceUrlsChecker(checkResourceUrls);
		final HTMLClient pc = new HTMLClient(htmlResponse, allowedUrls, urlRewriter, resourceUrlsChecker);
		pc.setFinalRequestUrl(finalRequestUrl);
		pc.setUserCookies(request.getCookies());
		pc.setUp(request.getPreferences());
		StringWriter stringWriter = new StringWriter();
		pc.parse(stringWriter, R.UTF_8);
		String resultTextContent = stringWriter.toString();
		
		return resultTextContent;

	}
}
