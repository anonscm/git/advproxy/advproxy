package org.evolvis.advproxy.portlet.controller;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.Enumeration;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.apache.http.cookie.Cookie;
import org.evolvis.advproxy.core.AdvHttpResponse;
import org.evolvis.advproxy.core.HttpMethodeType;
import org.evolvis.advproxy.core.NewHttpManager;
import org.evolvis.advproxy.core.R;
import org.evolvis.advproxy.core.RequestResultType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;

@Controller("GetUrlController")
@RequestMapping(value="VIEW")
public class GetUrlController extends AbstractController {

	@ActionMapping(params={R.JAVAX_PORTLET_ACTION + "=" + R.ACTION_FORMGETURL})
	public void formGetURL(ActionRequest request, ActionResponse response) throws Exception {
		
		// find out which charset is used
		String charenc = extractCharenc(request);
		
		// extract base URL
		String baseUrl = getUrlFromRequest(request);
		
		String query = extractQueryString(request, charenc);
		
		URI uri = URI.create(baseUrl + "?" + query); // FIXME if an "#" is in the query it gets lost in here
		
		final NewHttpManager httpManager = getDefaultHttpManager(request, uri);
		
		if (isMultipartRequest(request)) {
			setupMultipartRequest(request, httpManager);
		}
		
		AdvHttpResponse method = null;
		try {
			method = httpManager.processUrl(HttpMethodeType.GET, charenc);
			
			response.setRenderParameter(R.PORTLET_URL_PARAM_NAME, uri.toString());
			response.setRenderParameter(R.PORTLET_ACTION, R.ACTION_FORMGETURL);
			
			response.setRenderParameter(R.STATUS_CODE, String.valueOf(method.getStatusCode()));
			if (method.getFinalRequestUri() != null) {
				response.setRenderParameter(R.FINAL_REQUEST_URI, method.getFinalRequestUri().toString());				
			}
			if (method.getResultType() != null) {
				response.setRenderParameter(R.REQUEST_RESULT_TYPE, method.getResultType().toString());				
			}
			
			for (Cookie cookie : method.getCookies()) {
				response.addProperty(NewHttpManager.transformFromApacheToServletCookie(getPortletRequestBaseUrl(request), cookie));
			}
	
			if (RequestResultType.TEXT.equals(method.getResultType())) {
				String responseBody = method.getResponseBodyAsString();
				if (responseBody != null) {
					response.setRenderParameter(R.REQUEST_RESULT_HTML, responseBody);					
				}
			}
		}
		finally {
			if (method != null) method.freeResources();
		}
	}
	
	/**
	 * Extracts query string (already decoded in contrast to base url!)
	 */
	private String extractQueryString(ActionRequest request, String charenc) throws UnsupportedEncodingException {
		StringBuilder querySB = new StringBuilder();
		Enumeration<String> names = request.getParameterNames();
		while (names.hasMoreElements()) {
			String key = names.nextElement();
			if (key.startsWith(R.JAVAX_PORTLET_PREFIX) || key.startsWith(R.ADVPROXY_PORTLET_PREFIX))
				continue;
			if (querySB.length() > 0) querySB.append("&");
			querySB.append(URLEncoder.encode(key, charenc) + "=" + URLEncoder.encode(request.getParameter(key), charenc));
		}
		return querySB.toString();
	}
}
