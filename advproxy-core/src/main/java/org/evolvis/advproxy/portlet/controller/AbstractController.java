package org.evolvis.advproxy.portlet.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.portlet.ClientDataRequest;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.evolvis.advproxy.core.AdvPortletFileUpload;
import org.evolvis.advproxy.core.AllowedUrlsAndXPaths;
import org.evolvis.advproxy.core.Default;
import org.evolvis.advproxy.core.NewHttpManager;
import org.evolvis.advproxy.core.R;
import org.evolvis.advproxy.core.ResourceUrlsChecker;

public class AbstractController {

	public static final String RB_DIR = "translations";
	public static final String RB_PREFIX = RB_DIR + "/portlet";
	
	protected final Log log = LogFactory.getLog(getClass());
	
	protected NewHttpManager getDefaultHttpManager(PortletRequest request, URI targetUrl) {
		
		final NewHttpManager httpManager = new NewHttpManager();
		
		final String referer = request.getParameter(R.PORTLET_REFERER_PARAM_NAME);
		if (referer != null && referer.trim().length() > 0) {
			httpManager.setRefererUrl(URI.create(referer));
		}
		
		if (useProxy(request)) {
			setupProxy(request, httpManager);
		}
		
		if (useCookies(request)) {
			setupAdditionalCookies(request, httpManager, targetUrl);
		}
		
		if (useUserUUIDCookie(request)) {
			setupUserUUIDCookie(request, httpManager, targetUrl);
		}
		
		httpManager.setAllowedUrlsAndXPaths(getAllowedUrls(request));
		httpManager.setRequestUrl(targetUrl);
		httpManager.setRequestParam(request.getParameterMap());
		httpManager.setUserAgent(getUserAgent(request));
		
		return httpManager;
	}
	
	protected ResourceUrlsChecker getResourceUrlsChecker(PortletRequest request) {
		String checkResourceUrls[] = request.getPreferences().getValues(R.PREFS.CHECK_RESOURCE_URLS, new String[]{});
		if (checkResourceUrls == null) {
			checkResourceUrls = new String[]{};
		}
		return new ResourceUrlsChecker(checkResourceUrls);
	}
	
	protected boolean useCookies(PortletRequest request) {
		return Boolean.parseBoolean(request.getPreferences().getValue(R.PREFS.GENERATE_ADDITIONAL_COOKIES, Default.GENERATE_ADDITIONAL_COOKIES));
	}
	
	protected void setupAdditionalCookies(PortletRequest request, NewHttpManager httpManager, URI url) {
		final List<Cookie> additionalCookies = NewHttpManager.transformCookies(url, request.getCookies());
		httpManager.setUseCookies(true);
		httpManager.setAdditionalCookies(additionalCookies);
	}
	
	/**
	 * Return the parameter name for the resource url. Or null if the parameter
	 * may not be appended.
	 * 
	 * @param request
	 * @return parameter or null
	 */
	protected String getAppendUserUUIDResourceURLParameterName(PortletRequest request) {
		if (
				Boolean.parseBoolean(request.getPreferences().getValue(R.PREFS.SEND_USER_UUID, Default.SEND_USER_UUID)) &&
				Boolean.parseBoolean(request.getPreferences().getValue(R.PREFS.APPEND_USER_UUID_TO_RESOURCEURL, Default.APPEND_USER_UUID_TO_RESOURCEURL))) {
			return request.getPreferences().getValue(R.PREFS.USER_UUID_COOKIENAME, Default.USER_UUID_COOKIENAME);
		} else {
			return null;
		}
	}
	
	protected boolean useUserUUIDCookie(PortletRequest request) {
		return Boolean.parseBoolean(request.getPreferences().getValue(R.PREFS.SEND_USER_UUID, Default.SEND_USER_UUID));
	}
	
	protected void setupUserUUIDCookie(PortletRequest request, NewHttpManager httpManager, URI url) {
		String userUUIDCookieName = getUserUUIDCookieName(request);
		String userUUIDCookieValue = getUserUUIDCookieValue(request);
		
		if (userUUIDCookieValue != null) {
			final BasicClientCookie uuidCookie = new BasicClientCookie(userUUIDCookieName, userUUIDCookieValue);
			uuidCookie.setPath("/");
			uuidCookie.setDomain(url.getHost());
			
			httpManager.getAdditionalCookies().add(uuidCookie);
		}
	}
	
	protected String getUserUUIDCookieName(PortletRequest request) {
		String userUUIDCookieName = request.getPreferences().getValue(R.PREFS.USER_UUID_COOKIENAME, Default.USER_UUID_COOKIENAME);
		if( userUUIDCookieName == null || userUUIDCookieName.trim().length() == 0 ) {
			userUUIDCookieName = Default.USER_UUID_COOKIENAME;
		}
		return userUUIDCookieName;
	}
	
	protected String getUserUUIDCookieValue(PortletRequest request) {
		return request.getRemoteUser();
	}
	
	protected boolean useProxy(PortletRequest request) {
		return Boolean.parseBoolean(request.getPreferences().getValue(R.PREFS.USE_PROXY, Default.USE_PROXY));
	}
	
	protected void setupProxy(PortletRequest request, NewHttpManager httpManager) {
		httpManager.setUseProxy(true);
		httpManager.setProxyHost(request.getPreferences().getValue(R.PREFS.PROXY_HOST, null));
		httpManager.setProxyPort(Integer.parseInt(request.getPreferences().getValue(R.PREFS.PROXY_PORT, Default.PROXY_PORT)));
	}
	
	protected boolean isMultipartRequest(ClientDataRequest request) {
		return AdvPortletFileUpload.isMultipartContent(request);
	}
	
	@SuppressWarnings("unchecked")
	protected void setupMultipartRequest(ClientDataRequest request, NewHttpManager httpManager) throws FileUploadException {
		
		// Create a factory for disk-based file items
		final FileItemFactory factory = new DiskFileItemFactory();
		
		// Create a new file upload handler
		final AdvPortletFileUpload upload = new AdvPortletFileUpload(factory);
		
		// Parse the request and
		final List<FileItem> multipartFileItems = upload.parseRequest(request);
		
		// Setup HttpManger
		httpManager.setMultipartRequest(true);
		httpManager.setMultipartFileItems(multipartFileItems);
	}

	protected AllowedUrlsAndXPaths getAllowedUrls(PortletRequest request) {
		final String[] allowedUrlsAndXPaths = request.getPreferences().getValues(R.PREFS.ALLOWED_URLS, new String[]{});
		return new AllowedUrlsAndXPaths(allowedUrlsAndXPaths);
	}
	
	protected URI getPortletRequestBaseUrl(PortletRequest request) throws URISyntaxException {
		return new URI(request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort());
	}
	
	protected String extractCharenc(PortletRequest request) {
		String charenc = request.getPreferences().getValue(R.PREFS.DEFAULT_ENCODING, R.AUTO); // get charset from preferences
		if (charenc == null || R.AUTO.equals(charenc)) {// if charset is R.AUTO
				charenc = R.UTF_8;
		}
		return charenc;
	}
	
	/**
	 * Extracts the proxied request URL from the request.
	 */
	protected String getUrlFromRequest(PortletRequest request) {
		final String url = request.getParameter(R.PORTLET_URL_PARAM_NAME);
		return url != null ? url : "";
	}

	/**
	 * Returns true, if and only if interportlet communication is configured to be used.
	 */
	protected boolean isUseInterportletCommunication(PortletPreferences preferences) {
		return Boolean.parseBoolean(preferences.getValue(R.PREFS.USE_INTERPORTLET_COMMUNICTION, "false"));
	}
	
	/**
	 * Returns the User-Agent configured in the preferences
	 */
	protected String getUserAgent(PortletRequest request) {
		
		String userAgent = request.getPreferences().getValue(R.PREFS.USER_AGENT, "");
		
		if (userAgent == null || userAgent.isEmpty()) {
			userAgent = request.getProperty("User-Agent");
		}
		
		return userAgent;
	}
}
