package org.evolvis.advproxy.portlet.controller;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;

import org.evolvis.advproxy.core.R;
import org.evolvis.advproxy.portlet.Config;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;

@Controller("SaveSettingsController")
@RequestMapping(value="EDIT")
public class SaveSettingsController extends AbstractController {

	@ActionMapping(params={R.JAVAX_PORTLET_ACTION + "=" + R.ACTION_SAVESETTINGS})
	public void saveSettings(ActionRequest request, ActionResponse response) throws PortletException, IOException {
		PortletPreferences preferences = request.getPreferences();
		Config conf = new Config(RB_PREFIX, request.getLocale());

		// Basis options
		String homepage = request.getParameter(R.PREFS.HOMEPAGE);
		String[] allowedURLs = null;//AdvproxyUtils.splitLines(request.getParameter(R.PREFS.ALLOWED_URLS));
		String[] checkResourceUrls = null;//AdvproxyUtils.splitLines(request.getParameter(R.PREFS.CHECK_RESOURCE_URLS));

		// Proxy options
		boolean useProxy = Boolean.valueOf(request.getParameter(R.PREFS.USE_PROXY));
		String proxyHost = request.getParameter(R.PREFS.PROXY_HOST);
		int proxyPort;
		try {
			proxyPort = Integer.parseInt(request.getParameter(R.PREFS.PROXY_PORT));
		} catch (NumberFormatException e) {
			proxyPort = 8080;
		}
		
		// Cookie options
		boolean generateAdditionalCookies = Boolean.valueOf(request.getParameter(R.PREFS.GENERATE_ADDITIONAL_COOKIES));
		boolean sendUserUUID = Boolean.valueOf(request.getParameter(R.PREFS.SEND_USER_UUID));
		String userUUIDCookieName = request.getParameter(R.PREFS.USER_UUID_COOKIENAME);

		// auto if the encoding should be automatically recognized
		// else the name of the encoding
		String defaultEncoding = request.getParameter(R.PREFS.DEFAULT_ENCODING);

		boolean forceNewWindowOnNotAllowedURLs = Boolean.valueOf(request.getParameter(R.PREFS.FORCE_NEW_WINDOW));
		String minHeight = request.getParameter(R.PREFS.MIN_HEIGHT);
		String maxHeight = request.getParameter(R.PREFS.MAX_HEIGHT);

		// Addressbar options
		boolean showAddressbar = Boolean.valueOf(request.getParameter(R.PREFS.SHOW_ADDRESSBAR));
		boolean isAddressEditable = Boolean.valueOf(request.getParameter(R.PREFS.IS_ADDRESS_EDITABLE));
		boolean showGoHomeButtonInAddressbar = Boolean.valueOf(request.getParameter(R.PREFS.SHOW_GO_HOME_BUTTON));

		// Interportlet communication
		boolean useInterportletCommuniction = Boolean.valueOf(request.getParameter(R.PREFS.USE_INTERPORTLET_COMMUNICTION));
		String[] linkedURLs = null;//AdvproxyUtils.splitLines(request.getParameter(R.PREFS.LINKED_WINDOW_IDS));

		// Additional parameters
		boolean updateFormularFields = Boolean.valueOf(request.getParameter(R.PREFS.UPDATE_FORMULAR_FIELDS));

		boolean includeJavaScript = Boolean.valueOf(request.getParameter(R.PREFS.INCLUDE_JAVASCRIPT));
		String[] javaScriptExcludes = null;//AdvproxyUtils.splitLines(request.getParameter(R.PREFS.JAVA_SCRIPT_EXCLUDES));


		// Class and ID attribute rewriting
		boolean isClassRewriteEnabled = Boolean.valueOf(request.getParameter(R.PREFS.CLASS_REWRITE_ENABLED));
		boolean isIdRewriteEnabled = Boolean.valueOf(request.getParameter(R.PREFS.ID_REWRITE_ENABLED));
		boolean isClassRewriteForced = Boolean.valueOf(request.getParameter(R.PREFS.CLASS_REWRITE_FORCED));
		String classSuffix = request.getParameter(R.PREFS.CLASS_SUFFIX);

		Map<String, String> validationErrors = new LinkedHashMap<String, String>();
		if (homepage == null || homepage.length() == 0)
			validationErrors.put(R.PREFS.HOMEPAGE, conf.HOMEPAGE_EMPTY_ERROR);
		if (minHeight != null && minHeight.length() != 0 && !minHeight.matches("[0-9]+"))
			validationErrors.put(R.PREFS.MIN_HEIGHT, conf.MIN_HEIGHT_FORMAT_ERROR);
		if (maxHeight != null && maxHeight.length() != 0 && !maxHeight.matches("[0-9]+"))
			validationErrors.put(R.PREFS.MAX_HEIGHT, conf.MAX_HEIGHT_FORMAT_ERROR);

		if (validationErrors.isEmpty()) {
			preferences.setValue(R.PREFS.HOMEPAGE, homepage);
			preferences.setValues(R.PREFS.ALLOWED_URLS, allowedURLs);
			preferences.setValues(R.PREFS.CHECK_RESOURCE_URLS, checkResourceUrls);
			preferences.setValue(R.PREFS.DEFAULT_ENCODING, defaultEncoding);
			preferences.setValue(R.PREFS.USE_PROXY, Boolean.toString(useProxy));
			preferences.setValue(R.PREFS.PROXY_HOST, proxyHost);
			preferences.setValue(R.PREFS.PROXY_PORT, Integer.toString(proxyPort));
			preferences.setValue(R.PREFS.GENERATE_ADDITIONAL_COOKIES, Boolean.toString(generateAdditionalCookies));
			preferences.setValue(R.PREFS.USER_UUID_COOKIENAME, userUUIDCookieName);
			preferences.setValue(R.PREFS.SEND_USER_UUID, Boolean.toString(sendUserUUID));
			preferences.setValue(R.PREFS.FORCE_NEW_WINDOW, Boolean.toString(forceNewWindowOnNotAllowedURLs));
			preferences.setValue(R.PREFS.MIN_HEIGHT, minHeight);
			preferences.setValue(R.PREFS.MAX_HEIGHT, maxHeight);
			preferences.setValues(R.PREFS.JAVA_SCRIPT_EXCLUDES, javaScriptExcludes);
			preferences.setValue(R.PREFS.SHOW_ADDRESSBAR, Boolean.toString(showAddressbar));
			preferences.setValue(R.PREFS.IS_ADDRESS_EDITABLE, Boolean.toString(isAddressEditable));
			preferences.setValue(R.PREFS.SHOW_GO_HOME_BUTTON, Boolean.toString(showGoHomeButtonInAddressbar));
			preferences.setValue(R.PREFS.USE_INTERPORTLET_COMMUNICTION, Boolean.toString(useInterportletCommuniction));
			preferences.setValues(R.PREFS.LINKED_WINDOW_IDS, linkedURLs);
			preferences.setValue(R.PREFS.UPDATE_FORMULAR_FIELDS, Boolean.toString(updateFormularFields));
			preferences.setValue(R.PREFS.CLASS_REWRITE_ENABLED, Boolean.toString(isClassRewriteEnabled));
			preferences.setValue(R.PREFS.ID_REWRITE_ENABLED, Boolean.toString(isIdRewriteEnabled));
			preferences.setValue(R.PREFS.CLASS_REWRITE_FORCED, Boolean.toString(isClassRewriteForced));
			preferences.setValue(R.PREFS.CLASS_SUFFIX, classSuffix);
			preferences.setValue(R.PREFS.INCLUDE_JAVASCRIPT, Boolean.toString(includeJavaScript));
			preferences.store();
		} else {
			// TODO mpelze session stuff
			request.getPortletSession().setAttribute(R.VALIDATION_ERRORS_PARAM_NAME, validationErrors);
		}
	}
}
