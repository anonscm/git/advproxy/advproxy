package org.evolvis.advproxy.portlet;

import java.io.Serializable;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;


public class ProxyPreferences implements Serializable {

	private static final long serialVersionUID = 8418189383584358065L;
	
	@NotNull
	@Length(min=1)
	private String homepage;
	
	private String allowedURLs;
	
	private boolean forceNewWindowOnNotAllowedURLs;
	
	private String checkResourceUrls;
	
	//User Interfaces
	@Pattern(regexp = "[\\d]*")
	private String minHeight;
	
	@Digits(integer=5000, fraction = 0)
	private String maxHeight;	
	
	private boolean showAddressbar;	
	
	private boolean addressEditable;	
	
	private boolean showGoHomeButtonInAddressbar;
	
	//HTML
	private boolean includeJavascript;
	
	private String excludedJavascriptURLs;
	
	private boolean generateCookies;
	
	private boolean enableUserUUID;
	
	private String UUIDCookieName;
	
	private boolean appendUserUUIDToResourceURL;
	
	private boolean updateFormFields;
	
	private boolean isClassRewriteEnabled;
	
	private boolean isIdRewriteEnabled;
	
	private boolean isNameRewriteEnabled;
	
	private boolean isClassRewriteForced;
	
	private String classSuffix;
	
	//Misc
	private boolean enableProxy;
	
	private String proxyHost;
	
	private String proxyPort;
	
	private boolean interportletCommunication;

	private String portletWindowId;
	
	private String linkedWindowIds;
	
	private String userAgent;
	
	//Templates
	private String openUrlTemplate;
	
	private String formPostUrlTemplate;
	
	private String resourceGetUrlTemplate;
	
	private String resourcePostUrlTemplate;
	
	
	public ProxyPreferences( ) {
	}

//	//TODO include all
//	public ProxyPreferences(String homepage, String allowedURLs,
//			boolean forceNewWindowOnNotAllowedURLs) {
//		super();
//		this.homepage = homepage;
//		this.allowedURLs = allowedURLs;
//		this.forceNewWindowOnNotAllowedURLs = forceNewWindowOnNotAllowedURLs;
//	}

	public String getHomepage() {
		return homepage;
	}

	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	public String getAllowedURLs() {
		return allowedURLs;
	}

	public void setAllowedURLs(String allowedURLs) {
		this.allowedURLs = allowedURLs;
	}

	public boolean getForceNewWindowOnNotAllowedURLs() {
		return forceNewWindowOnNotAllowedURLs;
	}

	public void setForceNewWindowOnNotAllowedURLs(
			boolean forceNewWindowOnNotAllowedURLs) {
		this.forceNewWindowOnNotAllowedURLs = forceNewWindowOnNotAllowedURLs;
	}

	public String getCheckResourceUrls() {
		return checkResourceUrls;
	}

	public void setCheckResourceUrls(String checkResourceUrls) {
		this.checkResourceUrls = checkResourceUrls;
	}

	//User Interface
	
	public String getMinHeight() {
		return minHeight;
	}

	public void setMinHeight(String minHeight) {
		this.minHeight = minHeight;
	}

	public String getMaxHeight() {
		return maxHeight;
	}

	public void setMaxHeight(String maxHeight) {
		this.maxHeight = maxHeight;
	}

	public boolean getShowAddressbar() {
		return showAddressbar;
	}

	public void setShowAddressbar(boolean showAddressbar) {
		this.showAddressbar = showAddressbar;
	}

	public boolean getAddressEditable() {
		return addressEditable;
	}

	public void setAddressEditable(boolean addressEditable) {
		this.addressEditable = addressEditable;
	}

	public boolean getShowGoHomeButtonInAddressbar() {
		return showGoHomeButtonInAddressbar;
	}

	public void setShowGoHomeButtonInAddressbar(boolean showGoHomeButtonInAddressbar) {
		this.showGoHomeButtonInAddressbar = showGoHomeButtonInAddressbar;
	}

	
	//HTML
	public boolean getIncludeJavascript() {
		return includeJavascript;
	}
	
	public void setIncludeJavascript(boolean includeJavascript) {
		this.includeJavascript = includeJavascript;
	}

	public String getExcludedJavascriptURLs() {
		return excludedJavascriptURLs;
	}

	public void setExcludedJavascriptURLs(String excludedJavascriptURLs) {
		this.excludedJavascriptURLs = excludedJavascriptURLs;
	}

	public boolean getGenerateCookies() {
		return generateCookies;
	}

	public void setGenerateCookies(boolean generateCookies) {
		this.generateCookies = generateCookies;
	}

	public boolean getEnableUserUUID() {
		return enableUserUUID;
	}

	public void setEnableUserUUID(boolean enableUserUUID) {
		this.enableUserUUID = enableUserUUID;
	}

	public String getUUIDCookieName() {
		return UUIDCookieName;
	}

	public void setUUIDCookieName(String uUIDCookieName) {
		UUIDCookieName = uUIDCookieName;
	}

	public boolean getAppendUserUUIDToResourceURL() {
		return appendUserUUIDToResourceURL;
	}

	public void setAppendUserUUIDToResourceURL(boolean appendUserUUIDToResourceURL) {
		this.appendUserUUIDToResourceURL = appendUserUUIDToResourceURL;
	}

	public boolean getUpdateFormFields() {
		return updateFormFields;
	}

	public void setUpdateFormFields(boolean updateFormFields) {
		this.updateFormFields = updateFormFields;
	}

	public boolean getClassRewriteEnabled() {
		return isClassRewriteEnabled;
	}

	public void setClassRewriteEnabled(boolean isClassRewriteEnabled) {
		this.isClassRewriteEnabled = isClassRewriteEnabled;
	}

	public boolean getIdRewriteEnabled() {
		return isIdRewriteEnabled;
	}

	public void setIdRewriteEnabled(boolean isIdRewriteEnabled) {
		this.isIdRewriteEnabled = isIdRewriteEnabled;
	}
	
	public boolean getNameRewriteEnabled() {
		return isNameRewriteEnabled;
	}

	public void setNameRewriteEnabled(boolean isNameRewriteEnabled) {
		this.isNameRewriteEnabled = isNameRewriteEnabled;
	}

	public boolean getClassRewriteForced() {
		return isClassRewriteForced;
	}

	public void setClassRewriteForced(boolean isClassRewriteForced) {
		this.isClassRewriteForced = isClassRewriteForced;
	}

	public String getClassSuffix() {
		return classSuffix;
	}

	public void setClassSuffix(String classSuffix) {
		this.classSuffix = classSuffix;
	}
	
	//Misc

	public boolean getEnableProxy() {
		return enableProxy;
	}

	public void setEnableProxy(boolean enableProxy) {
		this.enableProxy = enableProxy;
	}

	public String getProxyHost() {
		return proxyHost;
	}

	public void setProxyHost(String proxyHost) {
		this.proxyHost = proxyHost;
	}

	public String getProxyPort() {
		return proxyPort;
	}

	public void setProxyPort(String proxyPort) {
		this.proxyPort = proxyPort;
	}

	public boolean getInterportletCommunication() {
		return interportletCommunication;
	}

	public void setInterportletCommunication(boolean interportletCommunication) {
		this.interportletCommunication = interportletCommunication;
	}
	
	public String getPortletWindowId() {
		return portletWindowId;
	}

	public void setPortletWindowId(String portletWindowId) {
		this.portletWindowId = portletWindowId;
	}

	public String getLinkedWindowIds() {
		return linkedWindowIds;
	}

	public void setLinkedWindowIds(String linkedWindowIds) {
		this.linkedWindowIds = linkedWindowIds;
	}
	
	public String getUserAgent() {
		return userAgent;
	}
	
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	//Template
	public String getOpenUrlTemplate() {
		return openUrlTemplate;
	}

	public void setOpenUrlTemplate(String openUrlTemplate) {
		this.openUrlTemplate = openUrlTemplate;
	}

	public String getFormPostUrlTemplate() {
		return formPostUrlTemplate;
	}

	public void setFormPostUrlTemplate(String formPostUrlTemplate) {
		this.formPostUrlTemplate = formPostUrlTemplate;
	}

	public String getResourceGetUrlTemplate() {
		return resourceGetUrlTemplate;
	}

	public void setResourceGetUrlTemplate(String resourceGetUrlTemplate) {
		this.resourceGetUrlTemplate = resourceGetUrlTemplate;
	}
	
	public String getResourcePostUrlTemplate() {
		return resourcePostUrlTemplate;
	}

	public void setResourcePostUrlTemplate(String resourcePostUrlTemplate) {
		this.resourcePostUrlTemplate = resourcePostUrlTemplate;
	}

}
