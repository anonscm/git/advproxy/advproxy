package org.evolvis.advproxy.portlet.controller;

import java.net.URI;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import javax.portlet.PortletRequest;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.http.Header;
import org.evolvis.advproxy.core.Default;
import org.evolvis.advproxy.core.HttpMethodeType;
import org.evolvis.advproxy.core.NewHttpManager;
import org.evolvis.advproxy.core.R;
import org.evolvis.advproxy.core.AdvHttpResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

@Controller("ResourceMappingController")
@RequestMapping(value="VIEW")
public class ResourceMappingController extends AbstractController {

	/**
	 * Whitelist all headers, that could bypassed without modifications from the proxyed system.
	 * The Header "Content-Type" and  will be set separately.
	 * @see http://en.wikipedia.org/wiki/List_of_HTTP_header_fields#Responses
	 */
	private final Set<String> allowedHeaders;
	
	public ResourceMappingController() {
		allowedHeaders = new TreeSet<String>(
			new Comparator<String>() {
				public int compare(String o1, String o2) { return o1.compareToIgnoreCase(o2); };
			}
		);
		allowedHeaders.addAll(Arrays.asList(
				"Age",
				"Cache-Control",
				"Content-Encoding",
				"Content-Length",
				"Content-MD5",
				"Content-Disposition",
				"Date",
				"ETag",
				"Expires",
				"Last-Modified",
				"RequestMethod"
			)
		);
	}
	
	@ResourceMapping
	public void serveResource(ResourceRequest request, ResourceResponse response) throws Exception {
		
		final URI targetUrl = URI.create(request.getParameter(R.PORTLET_URL_PARAM_NAME));
		request.setAttribute(R.PORTLET_URL_PARAM_NAME, targetUrl);

		final NewHttpManager httpManager = getDefaultHttpManager(request, targetUrl);

		if (isMultipartRequest(request)) {
			setupMultipartRequest(request, httpManager);
		}
		
		final String charenc = extractCharenc(request);
		
		AdvHttpResponse advResponse = null;
		try {
			if (R.ACTION_FORMPOSTURL.equals(request.getResourceID())) {
				advResponse = httpManager.processUrl(HttpMethodeType.POST, charenc);
			}
			else if (R.ACTION_FORMGETURL.equals(request.getResourceID())) {
				advResponse = httpManager.processUrl(HttpMethodeType.GET, charenc);
			}
			else {
				if ("POST".equalsIgnoreCase(request.getMethod())) {
					advResponse = httpManager.processUrl(HttpMethodeType.POST, charenc);
				}
				else {
					advResponse = httpManager.processUrl(HttpMethodeType.GET, charenc);
				}
			}
			
			response.setContentType(advResponse.getContentType());
			
			for (final Header header : advResponse.getResponseHeaders()) {
				if (allowedHeaders.contains(header.getName())) {
					response.setProperty(header.getName(), header.getValue());
				}
			}
			
			advResponse.writeResponseBodyToStream(response.getPortletOutputStream());
		}
		finally {
			if (advResponse != null) advResponse.freeResources();
		}
	}
	
	@Override
	protected String getUserUUIDCookieValue(PortletRequest request) {
		String targetUrl = request.getParameter(R.PORTLET_URL_PARAM_NAME);
		if (targetUrl != null && !getResourceUrlsChecker(request).isResourceUrl(targetUrl)) {
			return super.getUserUUIDCookieValue(request);
		}
		
		String userUUIDCookieValue = super.getUserUUIDCookieValue(request);
		if (userUUIDCookieValue == null &&
				Boolean.parseBoolean(request.getPreferences().getValue(R.PREFS.APPEND_USER_UUID_TO_RESOURCEURL, Default.APPEND_USER_UUID_TO_RESOURCEURL))) {
			userUUIDCookieValue = request.getParameter(request.getPreferences().getValue(R.PREFS.USER_UUID_COOKIENAME, Default.USER_UUID_COOKIENAME));
		}
		return userUUIDCookieValue;
	}
}
