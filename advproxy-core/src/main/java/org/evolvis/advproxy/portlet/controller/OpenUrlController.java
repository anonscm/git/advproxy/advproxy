package org.evolvis.advproxy.portlet.controller;

import java.net.URI;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.apache.http.cookie.Cookie;
import org.evolvis.advproxy.core.AdvHttpResponse;
import org.evolvis.advproxy.core.HttpMethodeType;
import org.evolvis.advproxy.core.NewHttpManager;
import org.evolvis.advproxy.core.R;
import org.evolvis.advproxy.core.RequestResultType;
import org.evolvis.advproxy.portlet.OpenURL;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;

@Controller("OpenUrlController")
@RequestMapping(value="VIEW")
public class OpenUrlController extends AbstractController {

	@ActionMapping(params={R.JAVAX_PORTLET_ACTION + "=" + R.ACTION_OPENURL})
	public void openURL(ActionRequest request, ActionResponse response) throws Exception {
		
		URI url = null;
		
		String charenc = extractCharenc(request);
		
		// if the openURL event came from another window: 
		// TODO: Rename PORTLET_URL_PARAM_NAME
		String urlParam = (String) request.getAttribute(R.PORTLET_URL_PARAM_NAME);
		String referer = (String) request.getParameter(R.PORTLET_REFERER_PARAM_NAME);
		
		if (urlParam == null) {
			urlParam = getUrlFromRequest(request);
		}
		
		url = URI.create(urlParam);

		// the call's origin is this portlet, and the openUrl parameter is set in the action request url
		// convert to server encoding
		
		String[] windowIDs = request.getPreferences().getValues(R.PREFS.LINKED_WINDOW_IDS, null);
		List<String> windowIDList = windowIDs != null ? Arrays.asList(windowIDs) : null;
		boolean useIPC = isUseInterportletCommunication(request.getPreferences());
		boolean callForMe = !useIPC || (useIPC && (windowIDList == null || (windowIDList != null && windowIDList.contains(request.getWindowID()))));
		if (!callForMe) {
			for (String windowID : windowIDs) {
				response.setEvent(R.OPEN_URL_EVENT_QNAME, new OpenURL(url.toString(), windowID));
			}
		} else {
			if (url != null) {
				response.setRenderParameter(R.PORTLET_URL_PARAM_NAME, url.toString());		
			}
			if (referer != null) {
				response.setRenderParameter(R.PORTLET_REFERER_PARAM_NAME, referer);				
			}
		}

		final NewHttpManager httpManager = getDefaultHttpManager(request, url);

		if (isMultipartRequest(request)) {
			setupMultipartRequest(request, httpManager);
		}
		
		AdvHttpResponse method = null;
		try {
			method = httpManager.processUrl(HttpMethodeType.GET, charenc);
		
			response.setRenderParameter(R.PORTLET_ACTION, R.ACTION_OPENURL);
			response.setRenderParameter(R.STATUS_CODE, String.valueOf(method.getStatusCode()));
			if (method.getFinalRequestUri() != null) {
				response.setRenderParameter(R.FINAL_REQUEST_URI, method.getFinalRequestUri().toString());				
			}
			if (method.getResultType() != null) {
				response.setRenderParameter(R.REQUEST_RESULT_TYPE, method.getResultType().toString());				
			}

			for (Cookie cookie : method.getCookies()) {
				response.addProperty(NewHttpManager.transformFromApacheToServletCookie(getPortletRequestBaseUrl(request), cookie));
			}
	
			if (RequestResultType.TEXT.equals(method.getResultType())) {
				String responseBody = method.getResponseBodyAsString();
				if (responseBody != null) {
					response.setRenderParameter(R.REQUEST_RESULT_HTML, responseBody);					
				}
			}
		}
		finally {
			if (method != null) method.freeResources();
		}
	}
}
