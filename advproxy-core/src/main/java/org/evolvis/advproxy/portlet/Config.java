package org.evolvis.advproxy.portlet;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Configuration object that creates an abstraction for an resource bundle name
 * and local and give "constant like" access to the resource bundle parameters.
 */
public class Config {
	/**
	 * Value of the resource bundle (key <code>request.error</code>)
	 */
	public final String REQUEST_ERROR;
	/**
	 * Value of the resource bundle (key <code>method.failed</code>)
	 */
	public final String METHOD_FAIL;
	/**
	 * Value of the resource bundle (key <code>url.notAllowed</code>)
	 */
	public final String URL_NOT_ALLOWED;
	/**
	 * Value of the resource bundle (key <code>"url.allowedAre"</code>)
	 */
	public final String URL_ALLOWED_ARE;
	/**
	 * Value of the resource bundle (key <code>request.url</code>)
	 */
	public final String REQUEST_URL;
	/**
	 * Value of the resource bundle (key <code>open.external</code>)
	 */
	public final String OPEN_EXTERNAL;
	/**
	 * Value of the resource bundle (key <code>redirect.attention</code>)
	 */
	public final String REDIRECT_ATTENTION;
	/**
	 * Value of the resource bundle (key <code>redirect.extern.instruction</code>)
	 */
	public final String REDIRECT_EXTERN_INSTRUCTION;
	/**
	 * Value of the resource bundle (key <code>setting.error.homepageEmpty</code>)
	 */
	public final String HOMEPAGE_EMPTY_ERROR;
	/**
	 * Value of the resource bundle (key <code>setting.error.minHeight</code>)
	 */
	public final String MIN_HEIGHT_FORMAT_ERROR;
	/**
	 * Value of the resource bundle (key <code>setting.error.maxHeight</code>)
	 */
	public final String MAX_HEIGHT_FORMAT_ERROR;
	/**
	 * Value of the resource bundle (key <code>statusCode</code>)
	 */
	public final String STATUS_CODE;

	/**
	 * Create instance of this configuration object and load all needed
	 * parameters from the resource bundle.
	 * 
	 * @param baseName
	 * @param locale
	 */
	public Config(String baseName, Locale locale) {
		ResourceBundle resourceBundle = ResourceBundle.getBundle(baseName, locale);
		
		REQUEST_ERROR = resourceBundle.getString("request.error");
		METHOD_FAIL = resourceBundle.getString("method.failed");
		URL_NOT_ALLOWED = resourceBundle.getString("url.notAllowed");
		URL_ALLOWED_ARE = resourceBundle.getString("url.allowedAre");
		REQUEST_URL = resourceBundle.getString("request.url");
		OPEN_EXTERNAL = resourceBundle.getString("open.external");
		REDIRECT_ATTENTION = resourceBundle.getString("redirect.attention");
		REDIRECT_EXTERN_INSTRUCTION = resourceBundle.getString("redirect.extern.instruction");
		HOMEPAGE_EMPTY_ERROR = resourceBundle.getString("setting.error.homepageEmpty");
		MIN_HEIGHT_FORMAT_ERROR = resourceBundle.getString("setting.error.minHeight");
		MAX_HEIGHT_FORMAT_ERROR = resourceBundle.getString("setting.error.maxHeight");
		STATUS_CODE = resourceBundle.getString("statusCode");
	}
	
}
