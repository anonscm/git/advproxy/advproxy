<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	xmlns:spring="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form">
	
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" isThreadSafe="true"/>

<jsp:include page="/jsp/view_beginn.jsp" />

<c:if test="${httpStatusNotOK != null}">
	<div class="__advproxyHttpError">
		<h3 class="portlet-msg-error">${requestError}</h3>
		${methodFailed}<br/>
	</div>
</c:if>

<c:if test="${isURLNotAllowed != null}">
	${URLNotAllowedMessage }<br/>
	${allowedAreMessage}:<br/>
	${allowedURLs}
</c:if>

<c:if test="${isTextContent != null}">
	${textContent}
</c:if>

<c:if test="${textContentExceptionStackTrace != null}">
    <div class="portlet-msg-error">
        <pre>${textContentExceptionStackTrace}</pre> 
    </div>
</c:if>

<br/>

<c:if test="${fileLink != null}">
    <div class="__advproxyExternalResource">
        ${openExternalMessage}
    </div>
	<script type="text/javascript">
	function orgEvolvisAdvProxyOpenUrl() {
		var isAttachment = (${isAttachment} === "true");
		
	    if(isAttachment) {
	        var newWindow = window.open('${fileLink}', '_blank');
	        newWindow.focus();
	    } else {
	    	window.location.href='${fileLink}';
	    }
	};
	
	window.setTimeout("orgEvolvisAdvProxyOpenUrl()", 1000);
	window.setTimeout("window.history.back()", 10000);
	</script>
</c:if>

<jsp:include page="/jsp/view_finish.jsp" />

</jsp:root>