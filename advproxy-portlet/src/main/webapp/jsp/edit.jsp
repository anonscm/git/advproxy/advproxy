<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	xmlns:spring="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt">
	
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" isThreadSafe="true"/>

<style type="text/css"> 
.tab_content {
	border: 1px solid #669CCC;
	padding-bottom: 5px;
	padding: 5px;
	background-color: #EFEFEF;
}
 
#tabContainer {
	padding-bottom: 0px;
	margin-bottom: 0px;
	display: block;
}
 
#tabContainer div {
	padding-bottom: 5px;
	padding-right: 8px;
	padding-left: 8px;
	font-weight: bold;
	margin-left: 5px;
	padding-top: 5px;
	color: #000000;
	float: left;
}
 
#tabContainer .tabs_on, #tabContainer .tabs_off {
	border-bottom-color: #669CCC;
	border-right-color: #669CCC;
	background-position: bottom;
	border-left-color: #669CCC;
	border-bottom-style: solid;
	background-color: #EFEFEF;
	border-right-style: solid;
	border-top-color: #669CCC;
	border-left-style: solid;
	border-bottom-width: 0px;
	border-right-width: 1px;
	border-top-style: solid;
	border-left-width: 1px;
	border-top-width: 1px;
	cursor: pointer;
}
#tabContainer .tabs_on {
	background-color: #BBBBBB;
}

.table-tabs {
	width: 100%;
}

.splitline {
	border: 0;
	color: #FFFFFF;
	background-color: #FFFFFF;
	height: 1px;
	width: 100%;
}

.firstcolumn {
	width: 20%;
}
</style>


<c:set var="namespace" ><portlet:namespace /></c:set>
<c:url var="help" value="/images/help.png" />

<script type="text/javascript">
 
	var tabsClass = {
		tabSetArray: new Array(),
		classOn: "tabs_on",
		classOff: "tabs_off",
 
		addTabs: function (tabsContainer) {
		tabs = document.getElementById(tabsContainer).getElementsByTagName("div");
		for (x in tabs) {
			if (typeof(tabs[x].id) != "undefined") {
				this.tabSetArray.push(tabs[x].id);
			} else {}
		}
	},
 
	switchTab: function (element) {
		for (x in this.tabSetArray) {
			tabItem = this.tabSetArray[x];
			dataElement = document.getElementById(tabItem + "_data");
				if (dataElement) {
					if (dataElement.style.display != "none") {
						dataElement.style.display = "none";
					} else {}
				} else {}
 
			tabElement = document.getElementById(tabItem);
			if (tabElement) {
				if (tabElement.className != this.classOff) {
					tabElement.className = this.classOff;
				} else {}
			} else {}
		}
 
	document.getElementById(element.id + "_data").style.display = "";
	element.className = this.classOn;
	}
};

</script>
<script type="text/javascript">
	
	function updateProxyDependencies() {
		if (document.getElementById('useProxy').checked) {
			document.getElementById('useProxyOptionTable').style.display = 'table';
		} else {
			document.getElementById('useProxyOptionTable').style.display = 'none';
		}
	}
	
	function updateJSDependencies() {
		if (document.getElementById('includeJavaScript').checked) {
			document.getElementById('showExcludedURLs').style.display = 'table-row';
		} else {
			document.getElementById('showExcludedURLs').style.display = 'none';
		}
	}
	
    function updateCookieDependencies() {
        if (document.getElementById('generateAdditionalCookies').checked) {
            document.getElementById('useCookieOptionTable').style.display = 'table';
        } else {
            document.getElementById('useCookieOptionTable').style.display = 'none';
        }
    }
    
    function updateCookieNameDependencies() {
        if (document.getElementById('sendUserUUID').checked) {
            document.getElementById('useCookieNameTable').style.display = 'table-row';
            document.getElementById('appendUserUUIDToResourceURLTable').style.display = 'table-row';
        } else {
            document.getElementById('useCookieNameTable').style.display = 'none';
            document.getElementById('appendUserUUIDToResourceURLTable').style.display = 'none';
        }
    }
    
	function updateAddressbarDependencies() {
		if (document.getElementById('showAddressbar').checked) {
			document.getElementById('showAddressbarOptionTable').style.display = 'table';
		} else {
			document.getElementById('showAddressbarOptionTable').style.display = 'none';
		}
	}
	
	function updateInterportletDependencies() {
		if (document.getElementById('useInterportletCommuniction').checked) {
			document.getElementById('showInterportletCommunicationOptionTable').style.display = 'table';
		} else {
			document.getElementById('showInterportletCommunicationOptionTable').style.display = 'none';
		}
	}

</script>


<portlet:renderURL var="settingsUrl">
	<portlet:param name="action" value="savePreferences" />
</portlet:renderURL>

<form:form action="${renderUrl}" method="post" style="margin-top:40px;" commandName="proxypreferences">

<!-- errors -->
<form:errors path="*" element="div" cssClass="portlet-msg-error" />

<!-- tabs -->
<table class="table-tabs">
	<tr>
		<td>
			<div id="tabContainer">
				<div id="tab_1" class="tabs_on" onclick="tabsClass.switchTab(this);"><spring:message code="basic.legend" /></div>
				<div id="tab_2" class="tabs_off" onclick="tabsClass.switchTab(this);"><spring:message code="userinterface.legend" /></div>			
				<div id="tab_3" class="tabs_off" onclick="tabsClass.switchTab(this);"><spring:message code="html.legend" /></div>
				<div id="tab_4" class="tabs_off" onclick="tabsClass.switchTab(this);"><spring:message code="misc.legend" /></div>
				<div id="tab_5" class="tabs_off" onclick="tabsClass.switchTab(this);"><spring:message code="urltemplates.legend" /></div>
			</div>
		</td>
	</tr>
	<tr>
		<td>
			<!-- basic options -->
			<div id="tab_1_data" class="tab_content">
				<table style="width: 95%;">
					<tr>
						<td class="firstcolumn">
							<label for="homepage"><spring:message code="basic.homepage" /></label>
							<spring:message code="basic.homepage.help" var="basicHomepageHelp"/>
							<img src="${help}" title="${basicHomepageHelp}" />
						</td>
						<td>
							<form:input path="homepage" id="homepage" size="200">${homepage}</form:input>
							<form:errors path="homepage" id="" element="div" cssClass="portlet-msg-error" />
						</td>
					</tr>
					<tr>
						<td class="firstcolumn">
							<label for="allowedURLs"><spring:message code="basic.allowedUrls" /></label>
							<spring:message code="basic.internalurl.help" var="basicAllowedURLsHelp"/>
							<img src="${help}" title="${basicAllowedURLsHelp} " />
						</td>
						<td>
							<form:textarea path="allowedURLs" id="allowedURLs" name="allowedURLs" rows="5" style="width: 100%;">
								${allowedURLs}
							</form:textarea>
						</td>
					</tr>	
<!-- 					<tr>
						<td class="firstcolumn">
							<label for="forceNewWindowOnNotAllowedURLs"><spring:message code="basic.forcenewwin" /></label>
							<spring:message code="basic.forcenew.help" var="basicForceNewHelp"/>
							<img src="${help}" title="${basicForceNewHelp} "/>
						</td>
						<td>
							<form:checkbox path="forceNewWindowOnNotAllowedURLs" value="${forceNewWindowOnNotAllowedURLs}" />
						</td>
					</tr> -->				
					<tr>
						<td class="firstcolumn">
							<label for="checkResourceUrls"><spring:message code="basic.checkResourceUrls" /></label>
							<spring:message code="basic.resourceurl.help" var="basicResourceUrlHelp"/>
							<img src="${help}" title="${basicResourceUrlHelp}" />
						</td>
						<td>
							<form:textarea path="checkResourceUrls" id="checkResourceUrls" name="checkResourceUrls" rows="5" style="width: 100%;">
								${checkResourceUrls}
							</form:textarea>
						</td>
					</tr>
				</table>
			</div>
			
			<!-- User Interface -->
			<div id="tab_2_data" class="tab_content" style="display: none;">			
				<table style="width: 95%;">
					<tr>
						<td class="firstcolumn">
							<label for="minHeight"><spring:message code="userinterface.minHeight" /></label>
							<spring:message code="userinterface.minHeight.help" var="userinterfaceMinHeightHelp"/>
							<img src="${help}" title="${userinterfaceMinHeightHelp}"/>
						</td>
						<td>
						<form:input path="minHeight" id="minHeight" name="minHeight" size="5" maxlength="5">
							${minHeight}
						</form:input> <spring:message code="userinterface.px" />
						<form:errors path="minHeight" id="" element="div" cssClass="portlet-msg-error" />
						</td>
					</tr>
					<tr>
						<td class="firstcolumn">
							<label for="maxHeight"><spring:message code="userinterface.maxHeight" /></label>
							<spring:message code="userinterface.maxHeight.help" var="userinterfaceMaxHeightHelp"/>
							<img src="${help}" title="${userinterfaceMaxHeightHelp}"/>
						</td>
						<td>
							<form:input path="maxHeight" id="maxHeight" name="maxHeight" size="5" maxlength="5">
								${maxHeight}
							</form:input> <spring:message code="userinterface.px" /> 
							<form:errors path="maxHeight" id="" element="div" cssClass="portlet-msg-error" />
						</td>
					</tr>
				</table>
				<table style="width: 95%;">				
					<tr>
						<td class="firstcolumn">
							<label for="showAddressbar"><spring:message code="userinterface.addrbar.show" /></label>
							<spring:message code="userinterface.addrbar.show.help" var="userinterfaceAddrbarShowHelp"/>
							<img src="${help}" title="${userinterfaceAddrbarShowHelp}" />
						</td>
						<td>
							<form:checkbox id="showAddressbar" path="showAddressbar" value="${showAddressbar}" 
								onchange="updateAddressbarDependencies();"
							 />
						</td>
					</tr>
				</table>
				<table style="width: 95%;" id="showAddressbarOptionTable" >
					<tr>
						<td class="firstcolumn">
							<label for="isAddressEditable"><spring:message code="userinterface.addrbar.edit" /></label>
							<spring:message code="userinterface.addrbar.edit.help" var="userinterfaceAddrbarEditHelp"/>
							<img src="${help}" title="${userinterfaceAddrbarEditHelp}" />
						</td>
						<td>
							<form:checkbox id="isAddressEditable" path="addressEditable" value="${isAddressEditable}" />
						</td>
					</tr>
					<tr>
						<td class="firstcolumn">
							<label for="showGoHomeButtonInAddressbar"><spring:message code="userinterface.addrbar.home" /></label>
							<spring:message code="userinterface.addrbar.home.help" var="userinterfaceAddrbarHomeHelp"/>
							<img src="${help}" title="${userinterfaceAddrbarHomeHelp}" />
						</td>
						<td>
							<form:checkbox id="showGoHomeButtonInAddressbar" path="showGoHomeButtonInAddressbar" value="${showGoHomeButtonInAddressbar}" />
						</td>
					</tr>
				</table>			
			</div>			
			
			<!-- HTML -->
			<div id="tab_3_data" class="tab_content" style="display: none;">
				<!-- Script Handling -->
				<table style="width: 95%;">
					<tr>
						<td class="firstcolumn">
							<label for="includeJavaScript"><spring:message code="script.inclJS" /></label>
							<spring:message code="script.inclJS.help" var="scriptinclJSHelp"/>
							<img src="${help}" title="${scriptinclJSHelp}" />
						</td>				
						<td>
							<form:checkbox id="includeJavaScript" path="includeJavascript" value="${includeJavaScript}"
								onchange="updateJSDependencies();" />
						</td>		
					</tr>
					<tr style="display: none" id="showExcludedURLs">
						<td class="firstcolumn">
							<label for="javaScriptExcludesFilter"><spring:message code="script.exclJSUrls" /></label>
							<spring:message code="script.exclJSUrls.help" var="scriptExclJSUrlsHelp"/>
							<img src="${help}" title="${scriptExclJSUrlsHelp}" />
						</td>
						<td>
							<form:textarea path="excludedJavascriptURLs" id="javaScriptExcludesFilter" name="javaScriptExcludesFilter" rows="5" style="width: 100%;">
								${javaScriptExcludesFilter}
							</form:textarea>
						</td>
					</tr>
				</table>
				<hr class="splitline"></hr>
				<!-- Cookie options -->				
				<table style="width: 95%;">
					<tr>
						<td class="firstcolumn">
							<label for="generateAdditionalCookies"><spring:message code="cookie.enableAdditionalCookieGen" /></label>
							<spring:message code="cookie.enableAdditionalCookieGen.help" var="cookieEnableAdditionalCookieGenHelp"/>
							<img src="${help}" title="${cookieEnableAdditionalCookieGenHelp}" />
						</td>
						<td>	
							<form:checkbox path="generateCookies" id="generateAdditionalCookies" value="${generateAdditionalCookies}"
								onchange="updateCookieDependencies();" 
							/>
	   					</td>
	   				</tr>
    			</table >
    			<table  style="width: 95%; display: none;" id="useCookieOptionTable" >
					<tr>
						<td class="firstcolumn">
       				   		<label for="sendUserUUID"><spring:message code="cookie.enableUserUUID" /></label>
       				   		<spring:message code="cookie.enableUserUUID.help" var="cookieEnableUserUUIDHelp"/>
       				    	<img src="${help}" title="${cookieEnableUserUUIDHelp}" />
       				    </td>
						<td>
							<form:checkbox path="enableUserUUID" id="sendUserUUID" value="${sendUserUUID}"
								onchange="updateCookieNameDependencies();" 
							/>
            			</td>
            		</tr>
            		<tr id="useCookieNameTable">
						<td class="firstcolumn">
           						<label for="userUUIDCookieName"><spring:message code="cookie.userUUIDCookieName" /></label>
           						<spring:message code="cookie.userUUIDCookieName.help" var="cookieUserUUIDCookieNameHelp"/>
           						<img src="${help}" title="${cookieUserUUIDCookieNameHelp}" />
            			</td>
            			<td>
            				<form:input path="uUIDCookieName" id="userUUIDCookieName" name="userUUIDCookieName" size="20" maxlength="255">
            					${userUUIDCookieName}
            				</form:input>
            			</td>
            		</tr>
					<tr id="appendUserUUIDToResourceURLTable">
						<td class="firstcolumn">
       				   		<label for="appendUserUUIDToResourceURL"><spring:message code="cookie.appendUserUUIDToResourceURL" /></label>
       				   		<spring:message code="cookie.appendUserUUIDToResourceURL.help" var="cookieAppendUserUUIDToResourceURLHelp"/>
       				    	<img src="${help}" title="${cookieAppendUserUUIDToResourceURLHelp}" />
       				    </td>
						<td>
							<form:checkbox path="appendUserUUIDToResourceURL" id="appendUserUUIDToResourceURL" value="${appendUserUUIDToResourceURL}"
							/>
            			</td>
            		</tr>
    			</table >		
				<hr class="splitline"></hr>
 				<!-- Additional parameters -->   			
				<table style="width: 95%;">
					<tr>
						<td class="firstcolumn">
							<label for="updateFormularFields"><spring:message code="add.update" /></label>
							<spring:message code="add.update.help" var="addUpdateHelp"/>
							<img src="${help}" title="${addUpdateHelp}" />
						</td>
						<td>
							<form:checkbox path="updateFormFields" id="updateFormularFields" value="${updateFormularFields}" />
						</td>
					</tr>
				</table>    
				<hr class="splitline"></hr>
				<!-- Class and ID rewriting parameters -->
				<table style="width: 95%;">
					<tr>
						<td class="firstcolumn">
							<label for="isClassRewriteEnabled"><spring:message code="rewrite.class" /></label>
							<spring:message code="rewrite.class.help" var="rewriteClassHelp"/>
							<img src="${help}" title="${rewriteClassHelp}" />
						</td>
						<td>
							<form:checkbox path="classRewriteEnabled" id="isClassRewriteEnabled" value="${isClassRewriteEnabled}" />
						</td>
					</tr>
					<tr>
						<td class="firstcolumn">
							<label for="isIdRewriteEnabled"><spring:message code="rewrite.id" /></label>
							<spring:message code="rewrite.id.help" var="rewriteIdHelp"/>
							<img src="${help}" title="${rewriteIdHelp}" />
						</td>
						<td>
							<form:checkbox path="idRewriteEnabled" id="isIdRewriteEnabled" value="${isIdRewriteEnabled}" />
						</td>
					</tr>
					<tr>
						<td class="firstcolumn">
							<label for="isNameRewriteEnabled"><spring:message code="rewrite.name" /></label>
							<spring:message code="rewrite.name.help" var="rewriteNameHelp"/>
							<img src="${help}" title="${rewriteNameHelp}" />
						</td>
						<td>
							<form:checkbox path="nameRewriteEnabled" id="isNameRewriteEnabled" value="${isNameRewriteEnabled}" />
						</td>
					</tr>
					<tr>
						<td class="firstcolumn">
							<label for="isClassRewriteForced"><spring:message code="rewrite.forced" /></label>
							<spring:message code="rewrite.forced.help" var="rewriteForcedHelp"/>
							<img src="${help}" title="${rewriteForcedHelp}" />
						</td>
						<td>
							<form:checkbox path="classRewriteForced" id="isClassRewriteForced" value="${isClassRewriteForced}" />
						</td>
					</tr>
				</table>
				<table style="width: 95%;">
					<tr>
						<td class="firstcolumn">
							<label for="classSuffix"><spring:message code="rewrite.suffix" /></label>
							<spring:message code="rewrite.suffix.help" var="rewriteSuffixHelp"/>
							<img src="${help}" title="${rewriteSuffixHelp}" />
						</td>
						<td>
							<form:input path="classSuffix" id="classSuffix" name="classSuffix" style="width: 100%;" maxlength="255">
            					${classSuffix}
            				</form:input>
						</td>
					</tr>
				</table>							
			</div>
			
			<!-- misc options -->
			<div id="tab_4_data" class="tab_content" style="display: none;">
				<!-- proxy options -->
				<table style="width: 95%;">
					<tr>
						<td class="firstcolumn">
							<label for="useProxy"><spring:message code="proxy.enable" /></label>
							<spring:message code="proxy.enable.help" var="proxyEnableHelp"/>
							<img src="${help}" title="${proxyEnableHelp}" />
						</td>
						<td>
							<form:checkbox path="enableProxy" id="useProxy" value="${useProxy}"
								onchange="updateProxyDependencies();" 
							/>						
						</td>
					</tr>
				</table>
				<table style="width: 95%; display: none;" id="useProxyOptionTable">
					<tr>
						<td class="firstcolumn">
							<label for="proxyHost"><spring:message code="proxy.host" /></label>
							<spring:message code="proxy.host.help" var="proxyHostHelp"/>
							<img src="${help}" title="${proxyHostHelp}" />
						</td>
						<td>
							<form:input path="proxyHost" id="proxyHost" name="proxyHost" size="20" maxlength="255">
            					${proxyHost}
            				</form:input>						
						</td>
					</tr>
					<tr>
						<td class="firstcolumn">
							<label for="proxyPort"><spring:message code="proxy.port" /></label>
							<spring:message code="proxy.port.help" var="proxyPortHelp"/>
							<img src="${help}" title="${proxyPortHelp}" />
						</td>
						<td>
							<form:input path="proxyPort" id="proxyPort" name="proxyPort" size="20" maxlength="255">
            					${proxyPort}
            				</form:input>
						</td>
					</tr>
				</table>
				<hr class="splitline"></hr>
				<!-- interportlet communication -->			
				<table style="width: 95%;">
					<tr>
						<td class="firstcolumn">
							<label for="useInterportletCommuniction"><spring:message code="ipc.use" /></label>
							<spring:message code="ipc.use.help" var="ipcUseHelp"/>
							<img src="${help}" title="${ipcUseHelp}" />
						</td>
						<td>
							<form:checkbox path="interportletCommunication" id="useInterportletCommuniction" value="${useInterportletCommuniction}"
								onchange="updateInterportletDependencies();" 
							/>						
						</td>
					</tr>
				</table>
				<table style="width: 95%;" id="showInterportletCommunicationOptionTable">
					<tr>
						<td class="firstcolumn">
							<label for="portletWindowID"><spring:message code="ipc.winid" /></label>
							<spring:message code="ipc.winid.help" var="ipcWinidHelp"/>
							<img src="${help}" title="${ipcWinidHelp}" />
						</td>
						<td>
							<form:input path="portletWindowId" id="portletWindowID" name="portletWindowID" style="width: 100%;" readonly="true">
            					${portletWindowId}
            				</form:input>											
						</td>
					</tr>
					<tr>
						<td class="firstcolumn">
							<label for="linkedWindowIDs"><spring:message code="ipc.linkedwins" /></label>
							<spring:message code="ipc.linkedwins.help" var="ipcLinkedwinsHelp"/>
							<img src="${help}" title="${ipcLinkedwinsHelp}" />
						</td>
						<td>
							<form:textarea path="linkedWindowIds" id="linkedWindowIDs" name="linkedWindowIDs" rows="5" style="width: 100%;">
								${linkedWindowIDs}
							</form:textarea>						
						</td>
					</tr>
				</table>	
			<hr class="splitline"></hr>
				<!-- User Agent -->
				<table style="width: 95%;">
					<tr>
						<td class="firstcolumn">
							<label for="userAgent"><spring:message code="misc.useragent" /></label>
							<spring:message code="misc.useragent.help" var="basicUserAgentHelp"/>
							<img src="${help}" title="${basicUserAgentHelp}" />
						</td>
						<td>
							<form:input path="userAgent" id="userAgent" size="100">${userAgent}</form:input>
						</td>
					</tr>
				</table>
			</div>			
			
			<!-- URL Templates -->			
			<div id="tab_5_data" class="tab_content" style="display: none;">
				<table style="width: 95%;">
       				 <tr>
       				 	<spring:message code="urltemplates.howTo" />
       				 </tr>
       			</table>
       			<hr class="splitline"></hr>
       			<table style="width: 95%;">
       				 <tr>
            			<td class="firstcolumn">
            				<label for="postUrl"><spring:message code="urltemplates.openUrl" /></label>
            				<spring:message code="urltemplates.openUrl.help" var="urltemplatesOpenUrlHelp"/>
            				<img src="${help}" title="${urltemplatesOpenUrlHelp}" />
            			</td>
           			 	<td>
            				${openUrlTemplate}
           			 	</td>
       			 	</tr>
       			</table>
       			<hr class="splitline"></hr>
       			<table style="width: 95%;">
       				<tr>
           				<td class="firstcolumn">
           					<label for="formPostUrl"><spring:message code="urltemplates.formPostUrl" /></label>
           					<spring:message code="urltemplates.formPostUrl.help" var="urltemplatesFormPostUrlHelp"/>
           					<img src="${help}" title="${urltemplatesFormPostUrlHelp}" />
           				</td>
            			<td>
            				${formPostUrlTemplate}																
            			</td>
        			</tr>
        		</table>
        		<hr class="splitline"></hr>
       			<table style="width: 95%;">
       				<tr>
           				<td class="firstcolumn">
           					<label for="resourceGetUrl"><spring:message code="urltemplates.resourceGetUrl" /></label>
           					<spring:message code="urltemplates.resourceGetUrl.help" var="urltemplatesResourceGetUrllHelp"/>
           					<img src="${help}" title="${urltemplatesResourceGetUrllHelp}" />
           				</td>
            			<td>
            				${resourceGetUrlTemplate}															
            			</td>
        			</tr>
        		</table>
        		<hr class="splitline"></hr>
       			<table style="width: 95%;">
       				<tr>
           				<td class="firstcolumn">
           					<label for="resourcePostUrl"><spring:message code="urltemplates.resourcePostUrl" /></label>
           					<spring:message code="urltemplates.resourcePostUrl.help" var="urltemplatesResourcePostUrllHelp"/>
           					<img src="${help}" title="${urltemplatesResourcePostUrllHelp}" />
           				</td>
            			<td>
            			     ${resourcePostUrlTemplate}							
            			</td>
        			</tr>
    			</table>
			</div>				
		
			<script type="text/javascript">
				tabsClass.addTabs("tabContainer");
			</script>
		</td>
	</tr>
</table>

<!-- end tabs -->


<script type="text/javascript">
	updateProxyDependencies();
	updateJSDependencies();
	updateCookieDependencies();
	updateCookieNameDependencies();
	updateAddressbarDependencies();
	updateInterportletDependencies();
</script>

<br/>

<div align="center"  >
	<button type="reset"><spring:message code="form.reset" /></button>
	<button type="submit"><spring:message code="form.save" /></button>
</div>

</form:form>

</jsp:root>