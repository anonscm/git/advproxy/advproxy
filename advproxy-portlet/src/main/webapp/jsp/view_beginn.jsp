<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page import="java.util.*" %>
<%@ page import="javax.portlet.*" %>
<%@ page import="org.evolvis.advproxy.core.R" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet" %>

<%
PortletRequest portletRequest = (PortletRequest) request.getAttribute("javax.portlet.request");
PortletPreferences portletPreferences = portletRequest.getPreferences();

// Basis options
String minHeight = portletPreferences.getValue(R.PREFS.MIN_HEIGHT, null);
String maxHeight = portletPreferences.getValue(R.PREFS.MAX_HEIGHT, null);
String homepage = portletPreferences.getValue(R.PREFS.HOMEPAGE, null);

String css = "";
if (minHeight != null && minHeight.length() != 0)
	css += "min-height: " + minHeight + "px; ";
if (maxHeight != null && maxHeight.length() != 0)
	css += "max-height: " + maxHeight + "px; ";

// Addressbar options
boolean showAddressbar = Boolean.valueOf(portletPreferences.getValue(R.PREFS.SHOW_ADDRESSBAR, null)).booleanValue();
boolean isAddressEditable = Boolean.valueOf(portletPreferences.getValue(R.PREFS.IS_ADDRESS_EDITABLE, null)).booleanValue();
boolean showGoHomeButtonInAddressbar = Boolean.valueOf(portletPreferences.getValue(R.PREFS.SHOW_GO_HOME_BUTTON, null)).booleanValue();
%>

<!-- begin advproxy -->

<% if (showAddressbar) { %>
	<div style="width: 100%;">

	<form action="<portlet:actionURL />" method="post" style="width: 100%;">
		<input type="hidden" name="<portlet:namespace /><%= R.JAVAX_PORTLET_ACTION %>" value="<%= R.ACTION_OPENURL %>">

		<table style="width: 100%; margin-bottom: 0.5em;">
			<tr>
				<% if (showGoHomeButtonInAddressbar) { %>
					<portlet:actionURL var="homepageActionUrl">
						<portlet:param name="<%= R.JAVAX_PORTLET_ACTION %>" value="<%= R.ACTION_OPENURL %>" />
						<portlet:param name="<%= R.PORTLET_URL_PARAM_NAME %>" value="<%= homepage %>" />
					</portlet:actionURL>
		
				<td style="width: 30px; text-align: left; vertical-align: middle;">
					<a name="goHomeButton" id="goHomeButton" href="<%= homepageActionUrl %>" >
						<img src="<c:url value="/images/go-home_22x22.png" />" style="background-color: transparent; border: none;" />
					</a>
				</td>
				<% } %>
				<% if (isAddressEditable) { %>
					<td>
						<input type="text" name="<%= R.PORTLET_URL_PARAM_NAME %>" value="<c:out value="<%= portletRequest.getAttribute(R.PORTLET_URL_PARAM_NAME) %>" escapeXml="true"/>" style="width: 100%;">
					</td>
					<td style="width: 30px; text-align: right; vertical-align: middle;">
		  				<input type="image" src="<c:url value="/images/go-next_22x22.png" />" name="<%= R.PORTLET_BUTTON_GO_NAME %>" value="Go" style="background-color: transparent; border: none;" />
					</td>
				<% } else { %>
					<td>
						<input type="text" name="<%= R.PORTLET_URL_PARAM_NAME %>" value="<c:out value="<%= portletRequest.getAttribute(R.PORTLET_URL_PARAM_NAME) %>" escapeXml="true"/>" style="width: 100%;" readonly="readonly">
					</td>
				<% } %>
			</tr>
		</table>

		<portlet:renderURL portletMode="edit" var="prefUrl" />
		<input type="hidden" id="preferencesUrl" name="preferencesUrl" value="${prefUrl}" />

	</form>
	
	</div>
<% } %>

<div style="width: 100%; <%= css %>overflow: auto;">
