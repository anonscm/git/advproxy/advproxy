<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	xmlns:spring="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form">
	
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" isThreadSafe="true"/>

<h2><spring:message code="exception.headline"/></h2>

<div class="portlet-msg-alert">
	<p><spring:message code="exception.details"/></p>
</div>

<portlet:renderURL var="homeUrl"><portlet:param name="portletMode" value="view"/></portlet:renderURL>
<p><a href="${homeUrl}"><spring:message code="exception.back"/></a></p>

</jsp:root>
