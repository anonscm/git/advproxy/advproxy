<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	xmlns:spring="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form">
	
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" isThreadSafe="true"/>

<div class="portlet-msg-alert"> 
	<b><spring:message code="redirect.attention" /></b>
	<br />
	<spring:message code="redirect.extern.instruction" arguments="${exception.notAllowedUrl}" />
</div>

<!--  javascript-part -->
<script type="text/javascript">
	function orgEvolvisAdvProxyOpenUrl() {
		// opened in the same window
	    var newWindow = window.open('${exception.notAllowedUrl}', '_self');
		newWindow.focus();
	}
	window.setTimeout("orgEvolvisAdvProxyOpenUrl()", 1000);
</script>

</jsp:root>
