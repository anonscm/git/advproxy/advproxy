package org.evolvis.advproxy.portalTools.itest;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.Date;

import org.evolvis.advproxy.portalTools.GetSetAdvProxyPortletSettings;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GetSetAdvProxyPortletSettingsIT extends AbstractIT {
	
	@Before
	public void setup() {
		portalTool.login(null, login, password);
	}
	
	@After
	public final void logout() throws MalformedURLException {
		portalTool.logout();
	}

	/**
	 * Test, that reading the AdvProxyPortletSettings twice result in equally state of the SettingObjects.
	 */
	@Test
	public void testDoubleSettingsRead() {

		final GetSetAdvProxyPortletSettings settings1 = new GetSetAdvProxyPortletSettings();
		settings1.setPortletSite(integrationSystem);
		portalTool.execute(settings1);

		final GetSetAdvProxyPortletSettings settings2 = new GetSetAdvProxyPortletSettings();
		settings2.setPortletSite(integrationSystem);
		portalTool.execute(settings2);
		
		assertNotSame("settings1 and settings2 must not be the same", settings1, settings2);
		assertEquals("settings1 and settings2 must be equals", settings1, settings2);
	}
	
	@Test
	public void testSetAdvProxyPortletSettings() throws MalformedURLException {

		final GetSetAdvProxyPortletSettings originalAdvSettings = new GetSetAdvProxyPortletSettings();
		originalAdvSettings.setPortletSite(integrationSystem);
		portalTool.execute(originalAdvSettings);
		
		final GetSetAdvProxyPortletSettings newAdvSettings = new GetSetAdvProxyPortletSettings();
		newAdvSettings.setPortletSite(integrationSystem);
		newAdvSettings.setHomepage(integrationSystem.getProtocol() + "://" + integrationSystem.getHost() + ":" + integrationSystem.getPort());
		newAdvSettings.setAllowedUrls(Arrays.asList(".*", "was.tested.on." + new Date()));
		newAdvSettings.setResourceUrls(Arrays.asList(".*.jpg", "was.tested.on." + new Date()));
		newAdvSettings.setEnableUserUUID(true);
		newAdvSettings.setGenerateAdditionalCookie(true);
		newAdvSettings.setCookieName("userUUID");
		newAdvSettings.setIdRewriteEnabled(false);
		newAdvSettings.setClassRewriteEnabled(false);
		newAdvSettings.setNameRewriteEnabled(true);
		newAdvSettings.setClassRewriteForced(false);
		newAdvSettings.setClassSuffix("test_suffix");
		newAdvSettings.setUserAgent("test_userAgent");

		assertFalse("originalAdvSettings must not equals  to newToSetAdvSettings", originalAdvSettings.equals(newAdvSettings));
		
		portalTool.execute(newAdvSettings);
		
		final GetSetAdvProxyPortletSettings newReadedAdvSettings = new GetSetAdvProxyPortletSettings();
		newReadedAdvSettings.setPortletSite(integrationSystem);
		portalTool.execute(newReadedAdvSettings);
		
		assertTrue("newAdvSettings: " + newAdvSettings + "\n must equals newReadedAdvSettings: " + newReadedAdvSettings, 
				newAdvSettings.equals(newReadedAdvSettings));
	}
}
