package org.evolvis.advproxy.portalTools.utest;

import static org.junit.Assert.*;

import java.beans.PropertyDescriptor;

import org.evolvis.advproxy.portalTools.GetSetAdvProxyPortletSettings;
import org.junit.Test;
import org.springframework.beans.BeanUtils;

public class GetSetAdvProxyPortletSettingsTest {
	
	@Test
	public void testEquals() {

		final GetSetAdvProxyPortletSettings settings1 = new GetSetAdvProxyPortletSettings();
	
		final GetSetAdvProxyPortletSettings settings2 = new GetSetAdvProxyPortletSettings();
		
		assertNotSame("settings1 and settings2 must not be the same", settings1, settings2);
		assertEquals("settings1 and settings2 must be equals", settings1, settings2);
	}
	
	@Test
	public void testToString() {
		
		final GetSetAdvProxyPortletSettings testObj = new GetSetAdvProxyPortletSettings();
		final String testObjString = testObj.toString().toLowerCase();
		final PropertyDescriptor[] properties = BeanUtils.getPropertyDescriptors(testObj.getClass());
		
		for (PropertyDescriptor property : properties) {
			final String propertyName = property.getName().toLowerCase();
			if ( !( propertyName.contains("xpath") || propertyName.equals("class") ) ) {
				assertTrue("toString() must contains property: " + propertyName, testObjString.contains(propertyName + "="));
			}
		}
	}
}
