package org.evolvis.advproxy.portalTools.itest;

import java.net.MalformedURLException;
import java.net.URL;

import org.evolvis.liferay.lrPortalTools.PortalTool;
import org.evolvis.liferay.lrPortalTools.PortalToolFactory;
import org.junit.Before;

public class AbstractIT {

	// integration test settings, readed fron system property
	URL integrationSystem;
	URL baseUrl;
	String login;
	String password;
	
	PortalTool portalTool;
	
	@Before
	public void _setUp() throws MalformedURLException {
		integrationSystem = new URL(System.getProperty("integrationSystem"));
		baseUrl = new URL(integrationSystem.getProtocol() + "://" + integrationSystem.getHost() + ":" + integrationSystem.getPort());
		login = System.getProperty("login");
		password = System.getProperty("password");
		portalTool = PortalToolFactory.getInstance(baseUrl);
	}
}
