package org.evolvis.advproxy.portalTools;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.evolvis.advproxy.core.R;
import org.evolvis.liferay.lrPortalTools.PortalTool;
import org.evolvis.liferay.lrPortalTools.ServiceProvider;
import org.evolvis.liferay.lrPortalTools.ToolExecutionException;

import com.gargoylesoftware.htmlunit.html.HtmlCheckBoxInput;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTextArea;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

public class SetAdvProxyPortletSettings implements ServiceProvider {

	private URL portletSite;
	private String portletId = "advproxy";
	private Integer index;
	
	// setting homepage
	private final String homePageXPath = "//input[contains(@name, '" + R.PREFS.HOMEPAGE + "')]";
	private String homepage;
	
	// allowedUrls
	private final String allowedUrlsXPath = "//textarea[contains(@name,'" + R.PREFS.ALLOWED_URLS + "')]";
	private Collection<String> allowedUrls;
	
	// resourceUrls
	private final String resourceUrlsXPath = "//textarea[contains(@name,'" + R.PREFS.CHECK_RESOURCE_URLS + "')]";
	private Collection<String> resourceUrls;
	
	// Cookie options
	private final String generateAdditionalCookieXPath = "//input[contains(@name, '" + R.PREFS.GENERATE_ADDITIONAL_COOKIES+ "')]";
	private Boolean generateAdditionalCookie;
	private final String enableUserUUIDXPath = "//input[contains(@name, '" + R.PREFS.SEND_USER_UUID + "')]";
	private Boolean enableUserUUID;
	private final String setCookieNameXPath = "//input[contains(@name, '" + R.PREFS.USER_UUID_COOKIENAME + "')]";
	private String cookieName;
	
	// submitButton
	private final String submitXPath = "//input[@type='submit']";
	
	// setter & getter

	/**
	 * Set the PortletSite operating on. The default value is the current site of PortalTool,#
	 * (see {@link PortalTool#getCurrentUrl()}). 
	 */
	public void setPortletSite(URL portletSite) {
		this.portletSite = portletSite;
	}

	/**
	 * Set the portletId or a qualifying part of it. The default value is 'advproxy'. 
	 */
	public void setPortletId(String portletId) {
		this.portletId = portletId;
	}

	/**
	 * Define the Index if more than one portlet was found. The default value is 0. 
	 */
	public void setIndex(Integer index) {
		this.index = index;
	}
	
	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}
	
	public void setAllowedUrls(Collection<String> allowedUrls) {
		this.allowedUrls = allowedUrls;
	}
	
	public void setResourceUrls(Collection<String> resourceUrls) {
		this.resourceUrls = resourceUrls;
	}
	
	public void setEnableUserUUID (Boolean enableUserUUID) {
		this.enableUserUUID = enableUserUUID;
	}
	
	public void setGenerateAdditionalCookie (Boolean generateAdditionalCookie) {
		this.generateAdditionalCookie = generateAdditionalCookie;
	}
	
	public void setCookieName (String cookieName) {
		this.cookieName = cookieName;
	}

	@Override
	@SuppressWarnings("unchecked")
	public HtmlPage execute(PortalTool portalTool) {
		final HtmlPage advPortletSettingPage = portalTool.openEditMode(portletSite, portletId, index);
		
		if (homepage != null) {
			advPortletSettingPage.<HtmlTextInput>getFirstByXPath(homePageXPath).setText(homepage);		
		}
		
		if (allowedUrls != null) {
			advPortletSettingPage.<HtmlTextArea>getFirstByXPath(allowedUrlsXPath).setText(StringUtils.join(allowedUrls, '\n'));
		}
		
		if (resourceUrls != null) {
			advPortletSettingPage.<HtmlTextArea>getFirstByXPath(resourceUrlsXPath).setText(StringUtils.join(resourceUrls, '\n'));
		}
		
		if (generateAdditionalCookie != null) {
			advPortletSettingPage.<HtmlCheckBoxInput>getFirstByXPath(generateAdditionalCookieXPath).setChecked(generateAdditionalCookie);
		}
		
		if (enableUserUUID != null) {
			advPortletSettingPage.<HtmlCheckBoxInput>getFirstByXPath(enableUserUUIDXPath).setChecked(enableUserUUID);
		}
		
		if (cookieName != null) {
			advPortletSettingPage.<HtmlTextInput>getFirstByXPath(setCookieNameXPath).setText(cookieName);
		}
		
		try {
			final HtmlPage submitResultPage = advPortletSettingPage.<HtmlInput>getFirstByXPath(submitXPath).click();
			return portalTool.closeEditMode(submitResultPage);
		}
		catch (IOException e) {
			throw new ToolExecutionException("execute() failed when saving AdvProxyPortletSettings", e);
		}
	}
}
