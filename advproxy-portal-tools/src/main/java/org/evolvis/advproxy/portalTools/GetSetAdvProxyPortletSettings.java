package org.evolvis.advproxy.portalTools;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.evolvis.advproxy.core.R;
import org.evolvis.liferay.lrPortalTools.PortalTool;
import org.evolvis.liferay.lrPortalTools.ServiceProvider;
import org.evolvis.liferay.lrPortalTools.ToolExecutionException;

import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.HtmlCheckBoxInput;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTextArea;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

/**
 * <p>The GetSetAdvProxyPortletSettings provides reading and updating of the AdvProxyPortlet settings. For reading a setting
 * don't call the property setter before execution. For update a setting, call the porperty setter before execution.</p>
 * <p>Read the <a href="https://evolvis.org/plugins/mediawiki/wiki/advproxy/index.php/Portlet_preferences">Project-Wiki</a>
 * 	to learn more about the offered settings.</p>
 * @author Tino Rink
 * @author Sabine Hoecher
 */
public class GetSetAdvProxyPortletSettings implements ServiceProvider {

	// data that qualifying a portlet
	private URL portletSite;
	private String portletId = "advproxy";
	private Integer index;
	
	// setting homepage
	private final String homePageXPath = "//input[contains(@name, '" + R.PREFS.HOMEPAGE + "')]";
	private String homepage;
	
	// allowedUrls
	private final String allowedUrlsXPath = "//textarea[contains(@name,'" + R.PREFS.ALLOWED_URLS + "')]";
	private Collection<String> allowedUrls;
	
	// resourceUrls
	private final String resourceUrlsXPath = "//textarea[contains(@name,'" + R.PREFS.CHECK_RESOURCE_URLS + "')]";
	private Collection<String> resourceUrls;
	
	//script handling
	private final String enableJavaScriptXPath = "//input[contains(@id, '" + R.PREFS.INCLUDE_JAVASCRIPT + "')]";
	private Boolean enableJavaScript;
	private final String excludedJavaScriptsXPath = "//textarea[contains(@id,'" + R.PREFS.JAVA_SCRIPT_EXCLUDES + "')]";
	private Collection<String> excludedJavaScripts;
	
	// Cookie options
	private final String generateAdditionalCookieXPath = "//input[contains(@id, '" + R.PREFS.GENERATE_ADDITIONAL_COOKIES+ "')]";
	private Boolean generateAdditionalCookie;
	
	private final String enableUserUUIDXPath = "//input[contains(@id, '" + R.PREFS.SEND_USER_UUID + "')]";
	private Boolean enableUserUUID;
	
	private final String setCookieNameXPath = "//input[contains(@id, '" + R.PREFS.USER_UUID_COOKIENAME + "')]";
	private String cookieName;

	
	
	// suffix handling
	private final String classRewriteEnabledXPath = "//input[contains(@id, '" + R.PREFS.ID_REWRITE_ENABLED + "')]";
	private Boolean classRewriteEnabled;
	
	private final String idRewriteEnabledXPath = "//input[contains(@id, '" + R.PREFS.CLASS_REWRITE_ENABLED + "')]";
	private Boolean idRewriteEnabled;
	
	private final String nameRewriteEnabledXPath = "//input[contains(@id, '" + R.PREFS.NAME_REWRITE_ENABLED + "')]";
	private Boolean nameRewriteEnabled;
	
	private final String classRewriteForcedXPath = "//input[contains(@id, '" + R.PREFS.CLASS_REWRITE_FORCED + "')]";
	private Boolean classRewriteForced;
	
	private final String classSuffixXPath = "//input[contains(@id, '" + R.PREFS.CLASS_SUFFIX + "')]";
	private String classSuffix;
	
	// user agent
	private final String userAgentXPath = "//input[contains(@name, '" + R.PREFS.USER_AGENT + "')]";
	private String userAgent;
	
	
	// submitButton
//	private final String submitXPath = "//input[@type='submit']";
	private final String submitXPath = "//button[@type='submit']";
	
	// setter & getter for qualifying a portlet

	/**
	 * Set the PortletSite operating on. The default value is the current site of PortalTool,#
	 * (see {@link PortalTool#getCurrentUrl()}). 
	 */
	public void setPortletSite(URL portletSite) {
		this.portletSite = portletSite;
	}

	/**
	 * Set the portletId or a qualifying part of it. The default value is 'advproxy'. 
	 */
	public void setPortletId(String portletId) {
		this.portletId = portletId;
	}

	/**
	 * Define the Index if more than one portlet was found. The default value is 0. 
	 */
	public void setIndex(Integer index) {
		this.index = index;
	}
	
	// getter & setter for portlet settings
	
	public String getHomepage() {
		return homepage;
	}
	
	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}
	
	public Collection<String> getAllowedUrls() {
		return allowedUrls;
	}
	
	public void setAllowedUrls(Collection<String> allowedUrls) {
		this.allowedUrls = allowedUrls;
	}
	
	public Collection<String> getResourceUrls() {
		return resourceUrls;
	}
	
	public void setResourceUrls(Collection<String> resourceUrls) {
		this.resourceUrls = resourceUrls;
	}
	
	public Boolean getEnableJavaScript() {
		return enableJavaScript;
	}
	
	public void setEnableJavaScript(Boolean enableJavaScript) {
		this.enableJavaScript = enableJavaScript;
	}
	
	public Collection<String> getExcludedJavaScripts() {
		return excludedJavaScripts;
	}
	
	public void setExcludedJavaScripts(Collection<String> excludedJavaScripts) {
		this.excludedJavaScripts = excludedJavaScripts;
	}
	
	public Boolean getEnableUserUUID() {
		return enableUserUUID;
	}
	
	public void setEnableUserUUID (Boolean enableUserUUID) {
		this.enableUserUUID = enableUserUUID;
	}
	
	public String getEnableUserUUIDXPath() {
		return enableUserUUIDXPath;
	}
	
	public void setGenerateAdditionalCookie (Boolean generateAdditionalCookie) {
		this.generateAdditionalCookie = generateAdditionalCookie;
	}
	
	public String getCookieName() {
		return cookieName;
	}
	
	public void setCookieName (String cookieName) {
		this.cookieName = cookieName;
	}

	public Boolean getClassRewriteEnabled() {
		return classRewriteEnabled;
	}

	public void setClassRewriteEnabled(Boolean classRewriteEnabled) {
		this.classRewriteEnabled = classRewriteEnabled;
	}

	public Boolean getIdRewriteEnabled() {
		return idRewriteEnabled;
	}

	public void setIdRewriteEnabled(Boolean idRewriteEnabled) {
		this.idRewriteEnabled = idRewriteEnabled;
	}
	
	public Boolean getNameRewriteEnabled() {
		return nameRewriteEnabled;
	}

	public void setNameRewriteEnabled(Boolean nameRewriteEnabled) {
		this.nameRewriteEnabled = nameRewriteEnabled;
	}

	public Boolean getClassRewriteForced() {
		return classRewriteForced;
	}

	public void setClassRewriteForced(Boolean classRewriteForced) {
		this.classRewriteForced = classRewriteForced;
	}

	public String getClassSuffix() {
		return classSuffix;
	}

	public void setClassSuffix(String classSuffix) {
		this.classSuffix = classSuffix;
	}
	

	public String getUserAgent() {
		return userAgent;
	}
	
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}


	@Override
	@SuppressWarnings("unchecked")
	public HtmlPage execute(PortalTool portalTool) {
		
		final HtmlPage advPortletSettingPage = portalTool.openEditMode(portletSite, portletId, index);
		
		final HtmlTextInput homepageInput = advPortletSettingPage.<HtmlTextInput>getFirstByXPath(homePageXPath);
		if (homepage != null) {
			homepageInput.setText(homepage);		
		} else {
			homepage = homepageInput.getText();
		}
		
		final HtmlTextArea allowedUrlsTextArea = advPortletSettingPage.<HtmlTextArea>getFirstByXPath(allowedUrlsXPath);
		if (allowedUrls != null) {
			allowedUrlsTextArea.setText(StringUtils.join(allowedUrls, '\n'));
		} else {
			allowedUrls = Arrays.asList(StringUtils.split(allowedUrlsTextArea.getText(), '\n'));
		}
		
		final HtmlTextArea resourceUrlsTextArea = advPortletSettingPage.<HtmlTextArea>getFirstByXPath(resourceUrlsXPath);
		if (resourceUrls != null) {
			resourceUrlsTextArea.setText(StringUtils.join(resourceUrls, '\n'));
		} else {
			resourceUrls = Arrays.asList(StringUtils.split(resourceUrlsTextArea.getText(), '\n'));
		}
		
		final HtmlCheckBoxInput enableJavaScriptInput = advPortletSettingPage.<HtmlCheckBoxInput>getFirstByXPath(enableJavaScriptXPath);
		if (enableJavaScript != null) {
			enableJavaScriptInput.setChecked(enableJavaScript);
		} else {
			enableJavaScript = enableJavaScriptInput.isChecked();
		}		
		
		final HtmlTextArea excludedJavaScriptsTextArea = advPortletSettingPage.<HtmlTextArea>getFirstByXPath(excludedJavaScriptsXPath);
		if (excludedJavaScripts != null) {
			excludedJavaScriptsTextArea.setText(StringUtils.join(excludedJavaScripts, '\n'));
		} else {
			excludedJavaScripts = Arrays.asList(StringUtils.split(excludedJavaScriptsTextArea.getText(), '\n'));
		}		

		final HtmlCheckBoxInput generateAdditionalCookieInput = advPortletSettingPage.<HtmlCheckBoxInput>getFirstByXPath(generateAdditionalCookieXPath);
		if (generateAdditionalCookie != null) {
			generateAdditionalCookieInput.setChecked(generateAdditionalCookie);
		} else {
			generateAdditionalCookie = generateAdditionalCookieInput.isChecked();
		}
		
		final HtmlCheckBoxInput enableUserUUIDInput = advPortletSettingPage.<HtmlCheckBoxInput>getFirstByXPath(enableUserUUIDXPath);
		if (enableUserUUID != null) {
			enableUserUUIDInput.setChecked(enableUserUUID);
		} else {
			enableUserUUID = enableUserUUIDInput.isChecked();
		}
		
		final HtmlTextInput cookieNameInput = advPortletSettingPage.<HtmlTextInput>getFirstByXPath(setCookieNameXPath);
		if (cookieName != null) {
			cookieNameInput.setText(cookieName);
		} else {
			cookieName = cookieNameInput.getText();
		}

		final HtmlCheckBoxInput classRewriteEnabledInput = advPortletSettingPage.<HtmlCheckBoxInput>getFirstByXPath(classRewriteEnabledXPath);
		if (classRewriteEnabled != null) {
			classRewriteEnabledInput.setChecked(classRewriteEnabled);
		} else {
			classRewriteEnabled = classRewriteEnabledInput.isChecked();
		}
		
		final HtmlCheckBoxInput idRewriteEnabledInput = advPortletSettingPage.<HtmlCheckBoxInput>getFirstByXPath(idRewriteEnabledXPath);
		if (idRewriteEnabled != null) {
			idRewriteEnabledInput.setChecked(idRewriteEnabled);
		} else {
			idRewriteEnabled = idRewriteEnabledInput.isChecked();
		}
		
		final HtmlCheckBoxInput nameRewriteEnabledInput = advPortletSettingPage.<HtmlCheckBoxInput>getFirstByXPath(nameRewriteEnabledXPath);
		if (nameRewriteEnabled != null) {
			nameRewriteEnabledInput.setChecked(nameRewriteEnabled);
		} else {
			nameRewriteEnabled = nameRewriteEnabledInput.isChecked();
		}
		
		final HtmlCheckBoxInput classRewriteForcedInput = advPortletSettingPage.<HtmlCheckBoxInput>getFirstByXPath(classRewriteForcedXPath);
		if (classRewriteForced != null) {
			classRewriteForcedInput.setChecked(classRewriteForced);
		} else {
			classRewriteForced = classRewriteForcedInput.isChecked();
		}
	
		final HtmlTextInput classSuffixInput = advPortletSettingPage.<HtmlTextInput>getFirstByXPath(classSuffixXPath);
		if (classSuffix != null) {
			classSuffixInput.setText(classSuffix);
		} else {
			classSuffix = classSuffixInput.getText();
		}
		
		final HtmlTextInput userAgentInput = advPortletSettingPage.<HtmlTextInput>getFirstByXPath(userAgentXPath);
		if (userAgent != null) {
			userAgentInput.setText(userAgent);		
		} else {
			userAgent = userAgentInput.getText();
		}
		
		
		try {
//			final HtmlPage submitResultPage = advPortletSettingPage.<HtmlInput>getFirstByXPath(submitXPath).click();
			final HtmlPage submitResultPage = advPortletSettingPage.<HtmlButton>getFirstByXPath(submitXPath).click();
	
			return portalTool.closeEditMode(submitResultPage);
		}
		catch (IOException e) {
			throw new ToolExecutionException("execute() failed when saving AdvProxyPortletSettings", e);
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GetSetAdvProxyPortletSettings [portletSite=");
		builder.append(portletSite);
		builder.append(", portletId=");
		builder.append(portletId);
		builder.append(", index=");
		builder.append(index);
		builder.append(", homepage=");
		builder.append(homepage);
		builder.append(", allowedUrls=");
		builder.append(allowedUrls);
		builder.append(", resourceUrls=");
		builder.append(resourceUrls);
		builder.append(", enableJavaScript=");
		builder.append(enableJavaScript);
		builder.append(", excludedJavaScripts=");
		builder.append(excludedJavaScripts);		
		builder.append(", generateAdditionalCookie=");
		builder.append(generateAdditionalCookie);
		builder.append(", enableUserUUID=");
		builder.append(enableUserUUID);
		builder.append(", cookieName=");
		builder.append(cookieName);
		builder.append(", classRewriteEnabled=");
		builder.append(classRewriteEnabled);
		builder.append(", idRewriteEnabled=");
		builder.append(idRewriteEnabled);
		builder.append(", nameRewriteEnabled=");
		builder.append(nameRewriteEnabled);
		builder.append(", classRewriteForced=");
		builder.append(classRewriteForced);
		builder.append(", classSuffix=");
		builder.append(classSuffix);
		builder.append(", userAgent=");
		builder.append(userAgent);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((allowedUrls == null) ? 0 : allowedUrls.hashCode());
		result = prime * result
				+ ((cookieName == null) ? 0 : cookieName.hashCode());
		result = prime * result
				+ ((enableUserUUID == null) ? 0 : enableUserUUID.hashCode());
		result = prime
				* result
				+ ((generateAdditionalCookie == null) ? 0
						: generateAdditionalCookie.hashCode());
		result = prime * result
				+ ((homepage == null) ? 0 : homepage.hashCode());
		result = prime * result + ((index == null) ? 0 : index.hashCode());
		result = prime * result
				+ ((portletId == null) ? 0 : portletId.hashCode());
		result = prime * result
				+ ((portletSite == null) ? 0 : portletSite.hashCode());
		result = prime * result
				+ ((resourceUrls == null) ? 0 : resourceUrls.hashCode());
		result = prime * result
				+ ((excludedJavaScripts == null) ? 0 : excludedJavaScripts.hashCode());
		result = prime * result
				+ ((enableJavaScript == null) ? 0 : enableJavaScript.hashCode());
		result = prime * result
				+ ((userAgent == null) ? 0 : userAgent.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		GetSetAdvProxyPortletSettings other = (GetSetAdvProxyPortletSettings) obj;
		if (allowedUrls == null) {
			if (other.allowedUrls != null) {
				return false;
			}
		} else if (!allowedUrls.equals(other.allowedUrls)) {
			return false;
		}
		if (cookieName == null) {
			if (other.cookieName != null) {
				return false;
			}
		} else if (!cookieName.equals(other.cookieName)) {
			return false;
		}
		if (enableUserUUID == null) {
			if (other.enableUserUUID != null) {
				return false;
			}
		} else if (!enableUserUUID.equals(other.enableUserUUID)) {
			return false;
		}
		if (generateAdditionalCookie == null) {
			if (other.generateAdditionalCookie != null) {
				return false;
			}
		} else if (!generateAdditionalCookie
				.equals(other.generateAdditionalCookie)) {
			return false;
		}
		if (homepage == null) {
			if (other.homepage != null) {
				return false;
			}
		} else if (!homepage.equals(other.homepage)) {
			return false;
		}
		if (index == null) {
			if (other.index != null) {
				return false;
			}
		} else if (!index.equals(other.index)) {
			return false;
		}
		if (portletId == null) {
			if (other.portletId != null) {
				return false;
			}
		} else if (!portletId.equals(other.portletId)) {
			return false;
		}
		if (portletSite == null) {
			if (other.portletSite != null) {
				return false;
			}
		} else if (!portletSite.equals(other.portletSite)) {
			return false;
		}
		if (resourceUrls == null) {
			if (other.resourceUrls != null) {
				return false;
			}
		} else if (!resourceUrls.equals(other.resourceUrls)) {
			return false;
		}
		if (excludedJavaScripts == null) {
			if (other.excludedJavaScripts != null) {
				return false;
			}
		} else if (!excludedJavaScripts.equals(other.excludedJavaScripts)) {
			return false;
		}
		if (enableJavaScript == null) {
			if (other.enableJavaScript != null) {
				return false;
			}
		} else if (!enableJavaScript.equals(other.enableJavaScript)) {
			return false;
		}
		if (userAgent == null) {
			if (other.userAgent != null) {
				return false;
			}
		} else if (!userAgent.equals(other.userAgent)) {
			return false;
		}
		return true;
	}
}
