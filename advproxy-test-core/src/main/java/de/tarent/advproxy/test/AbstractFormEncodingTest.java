package de.tarent.advproxy.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;

/**
 * Different html form encoding tests.
 */
public class AbstractFormEncodingTest extends AbstractBaseTest {
	
	protected final String nameField = "nameInputField";
	protected final String firstnameField = "vornameInputField";
	
	/**
	 * Setup this test with testcase name.
	 */
	public AbstractFormEncodingTest() {
		super("testFormEncoded");
	}
	
	/**
	 * Positive tests for x-www-form-urlencoded forms.
	 * 
	 * @throws FailingHttpStatusCodeException
	 * @throws IOException
	 */
	@Test
	public void testCorrectEncoding() throws FailingHttpStatusCodeException, IOException {
		
		final String firstname = "Mustafa";
		final String lastname = "Mustermann";
		final String encrypte = "application/x-www-form-urlencoded";
		
		HtmlPage formEncodingTestPage = getTestCasePage();
		
		// check test-div
		HtmlElement testform = formEncodingTestPage.getElementById("testFormEncoded");
		assertNotNull(testform);
		
		testform.<HtmlInput>getElementById(nameField).setValueAttribute(lastname);
		testform.<HtmlInput>getElementById(firstnameField).setValueAttribute(firstname);
		
		// submit input data 
		HtmlSubmitInput buttonSubmit = testform.<HtmlSubmitInput>getElementById("formSubmitButton");
		
		// must return a TEXTpage not HTML
		final HtmlPage answerText = buttonSubmit.click();
		final String encrypteValue= answerText.getElementById("encrypte").asText();

		assertNotNull("answerText should not be null", answerText);
		
		assertEquals(lastname, answerText.getElementById("lastname").asText());
		assertEquals(firstname, answerText.getElementById("firstname").asText());
		assertNotNull("encrypteValue should not be null", encrypteValue);
		assertTrue("must start with: " + encrypte + " but was: " + encrypteValue, encrypteValue.startsWith(encrypte));
	}
	
	/**
	 * Test special characters.
	 * 
	 * @throws FailingHttpStatusCodeException
	 * @throws IOException
	 */
	@Test
	public void testReservedChars() throws FailingHttpStatusCodeException, IOException {

		final String firstname = "!*'();:@&";
		final String lastname = "=+$,/?#[]";
		final String encrypte = "application/x-www-form-urlencoded";
		
		HtmlPage formEncodingTestPage = getTestCasePage();
		
		// check test-div
		HtmlElement testform = formEncodingTestPage.getElementById("testFormEncoded");
		assertNotNull(testform);
		
		testform.<HtmlInput>getElementById(nameField).setValueAttribute(lastname);
		testform.<HtmlInput>getElementById(firstnameField).setValueAttribute(firstname);
		
		// submit input data 
		HtmlSubmitInput buttonSubmit = testform.<HtmlSubmitInput>getElementById("formSubmitButton");
		
		// must return a TEXTpage not HTML
		final HtmlPage answerText = buttonSubmit.click();
		final String encrypteValue= answerText.getElementById("encrypte").asText();

		assertNotNull("answerText should not be null", answerText);
		
		assertEquals(lastname, answerText.getElementById("lastname").asText());
		assertEquals(firstname, answerText.getElementById("firstname").asText());
		assertNotNull("encrypteValue should not be null", encrypteValue);
		assertTrue("must start with: " + encrypte + " but was: " + encrypteValue, encrypteValue.startsWith(encrypte));
	}
	
	/**
	 * Test special characters.
	 * 
	 * @throws FailingHttpStatusCodeException
	 * @throws IOException
	 */
	@Test
	public void testSpecChars() throws FailingHttpStatusCodeException, IOException {

		final String firstname = "äöüß";
		final String lastname = "^°!{}~";
		final String encrypte = "application/x-www-form-urlencoded";
		
		HtmlPage formEncodingTestPage = getTestCasePage();
		
		// check test-div
		HtmlElement testform = formEncodingTestPage.getElementById("testFormEncoded");
		assertNotNull(testform);
		
		testform.<HtmlInput>getElementById(nameField).setValueAttribute(lastname);
		testform.<HtmlInput>getElementById(firstnameField).setValueAttribute(firstname);
		
		// submit input data 
		HtmlSubmitInput buttonSubmit = testform.<HtmlSubmitInput>getElementById("formSubmitButton");
		
		// must return a TEXTpage not HTML
		final HtmlPage answerText = buttonSubmit.click();
		final String encrypteValue= answerText.getElementById("encrypte").asText();

		assertNotNull("answerText should not be null", answerText);
		
		assertEquals(lastname, answerText.getElementById("lastname").asText());
		assertEquals(firstname, answerText.getElementById("firstname").asText());
		assertNotNull("encrypteValue should not be null", encrypteValue);
		assertTrue("must start with: " + encrypte + " but was: " + encrypteValue, encrypteValue.startsWith(encrypte));
	}
}
