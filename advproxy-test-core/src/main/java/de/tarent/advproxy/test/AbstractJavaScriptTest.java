package de.tarent.advproxy.test;

import org.junit.Test;

/**
 * JavaScript test cases.
 */
public class AbstractJavaScriptTest extends AbstractBaseTest {

	/**
	 * Setup this test with testcase name.
	 */
	public AbstractJavaScriptTest() {
		super("testJavaScript");
	}
	
	/**
	 * Dummy tests because we currently have no javascript tests...
	 */
	@Test
	public void dummyTest() { }
}
