package de.tarent.advproxy.test;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.evolvis.liferay.lrPortalTools.PortalTool;
import org.evolvis.liferay.lrPortalTools.PortalToolFactory;
import org.junit.Before;
import org.junit.runner.RunWith;

import com.gargoylesoftware.htmlunit.CookieManager;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import de.tarent.advproxy.test.concurrency.Concurrency;
import de.tarent.advproxy.test.concurrency.ConcurrencyJunitRunner;

/**
 * AbstractBaseTest handles login and create a portal tool instance.
 */
public abstract class AbstractBaseTest {

	// integration test settings, readed fron system property
	protected URL integrationSystem;
	protected URL baseUrl;
	protected String login;
	protected String password;
	
	private String testCase;
	private HtmlPage testIndexPage;
	private HtmlPage testCasePage;
	protected PortalTool portalTool;

	/**
	 * Creates a new instance.
	 */
	public AbstractBaseTest(String testCase) {
		this.testCase = testCase;
	}
	
	/**
	 * Setup abstract base tests with system properties!
	 * 
	 * @throws MalformedURLException
	 */
	@Before
	public final void _setUp() throws MalformedURLException {
		integrationSystem = new URL(getIntegrationSystem());
		baseUrl = new URL(integrationSystem.getProtocol() + "://" + integrationSystem.getHost() + ":" + integrationSystem.getPort());
		login = System.getProperty("login");
		password = System.getProperty("password");
		portalTool = PortalToolFactory.getInstance(baseUrl);
		portalTool.getWebClient().setJavaScriptEnabled(false);
		portalTool.getWebClient().setCssEnabled(false);
	}	
	
	/**
	 * @return portal tool to test portlet communication.
	 */
	public PortalTool getPortalTool() {
		return portalTool;
	}
	
	/**
	 * The integration system url if system property was set.
	 * @return
	 */
	public String getIntegrationSystem() {
		final String integrationSystem = System.getProperty("integrationSystem");
		if (integrationSystem == null) {
			throw new RuntimeException("SystemProperty 'integrationSystem' wasn't set!");
		}
		return integrationSystem;
	}

	/**
	 * Login name
	 * @return
	 */
	public String getLogin() {
		return login;
	}
	
	/**
	 * Password
	 * @return
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * Get first {@link HtmlPage} for testing.
	 * @return
	 * @throws FailingHttpStatusCodeException
	 * @throws IOException
	 */
	public HtmlPage getTestIndexPage() throws FailingHttpStatusCodeException, IOException {
		if(null == testIndexPage) {
			final String testStartUrl = getIntegrationSystem();
			testIndexPage = getBrowser().getPage(testStartUrl);
		}
		return testIndexPage;
	}
	
	/**
	 * Get test case {@link HtmlPage}.
	 * @return
	 * @throws FailingHttpStatusCodeException
	 * @throws IOException
	 */
	public HtmlPage getTestCasePage() throws FailingHttpStatusCodeException, IOException {
		if(null == testCasePage) {
			testCasePage = getTestIndexPage().getElementById(testCase).click();
		}
		return testCasePage;
	}
	
	/**
	 * Get test case url.
	 * @return
	 * @throws IOException
	 */
	public URL getTestPageUrl() throws IOException {
		return getTestCasePage().getUrl();
	}

	/**
	 * Get {@link WebClient} instance.
	 * @return
	 */
	public WebClient getBrowser() {
		return getPortalTool().getWebClient();
	}

	/**
	 * Get {@link CookieManager} instance.
	 * @return
	 */
	public CookieManager getCookieManager() {
		return getPortalTool().getCookieManager();
	}
}
