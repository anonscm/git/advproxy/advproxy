package de.tarent.advproxy.test.common;

/**
 * Liferay specific property names.
 */
public class LiferaySetting {
	public static final String PORTLET_ID_DESIGNATOR = "p_p_id";
	public static final String PORTLET_ID_DELIMMITER = "&";
	
	public static final String LOGINPORTLET_LOGIN_NAME = "login";
	public static final String LOGINPORTLET_PASSWORD_NAME = "password";
	public static final String LOGINPORTLET_REDIRECT_NAME = "redirect";
	public static final String LOGINPORTLET_SINGIN_BUTTON_NAME = "Sign In";
	public static final String LOGINPORTLET_FORM_NAME = "fm";
}
