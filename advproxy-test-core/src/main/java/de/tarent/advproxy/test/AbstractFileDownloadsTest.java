package de.tarent.advproxy.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;

import org.evolvis.advproxy.core.R;
import org.junit.Ignore;
import org.junit.Test;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.UnexpectedPage;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;

import de.tarent.advproxy.test.common.Util;

/**
 * Tests the different types of downloads
 * @author Sabine Höcher, tarent GmbH
 *
 */
public class AbstractFileDownloadsTest extends AbstractBaseTest {
	
	public static final String ELEMENT_NAME_DOWNLOAD_LINK = "downloadFile";
	public static final String ELEMENT_NAME_TEST_FILE_DOWNLOAD_FOR_RESOURCE_URL = "downloadFileForResourceUrl";
	public static final String ELEMENT_NAME_DOWNLOAD_LINK_POST = "downloadPostSubmitButton";

	WebResponse downloadedFile;
	File testFile;

	/**
	 * Setup this test with testcase name.
	 */
	public AbstractFileDownloadsTest() {
		super("testFileDownloads");
	}
	
	
	/**
	 * Tests for FileDownload
	 */
	@Test
	public void testDownloadFileByteArray() throws FailingHttpStatusCodeException, IOException, URISyntaxException {
		
		testSetUp(ELEMENT_NAME_DOWNLOAD_LINK);
		
		assertArrayEquals("byte array isn't the same", Util.getFileBytes(testFile), downloadedFile.getContentAsBytes());

	}
	
	/**
	 * Test download headers.
	 * 
	 * @throws IOException
	 * @throws FailingHttpStatusCodeException
	 * @throws URISyntaxException
	 */
	@Test
	public void testDownloadFileHeaderWeak() throws IOException, FailingHttpStatusCodeException, URISyntaxException {
		
		testSetUp(ELEMENT_NAME_DOWNLOAD_LINK);
		
		assertEquals("'Content-Length' header doesn't match", 
				String.valueOf(Util.getFileBytes(testFile).length), 
				downloadedFile.getResponseHeaderValue("Content-Length"));
		
		assertEquals("'Content-Disposition' header doesn't match",
				"attachment;filename=test.pdf",
				downloadedFile.getResponseHeaderValue("Content-Disposition"));	
		
		assertNotNull("'Last-Modified' header must exist", downloadedFile.getResponseHeaderValue("Last-Modified"));	
		
		assertTrue("'Content-Type' header doesn't match", downloadedFile.getResponseHeaderValue("Content-Type").startsWith("application/pdf"));
		
	}
	
	/**
	 * Test download headers.
	 * 
	 * @throws IOException
	 * @throws FailingHttpStatusCodeException
	 * @throws URISyntaxException
	 */
	@Test
	public void testDownloadFileHeaderStrict() throws IOException, FailingHttpStatusCodeException, URISyntaxException {
		
		testSetUp(ELEMENT_NAME_DOWNLOAD_LINK);
		
		assertEquals("'Content-Type' header doesn't match", "application/pdf", downloadedFile.getResponseHeaderValue("Content-Type"));
		
	}
		
	
	/**
	 * Tests for FileDownloadForResourceUrl
	 */
	@Test
	public void testDownloadFileForResourceUrlByteArray() throws FailingHttpStatusCodeException, IOException, URISyntaxException {
		
		testSetUp(ELEMENT_NAME_TEST_FILE_DOWNLOAD_FOR_RESOURCE_URL);
		
		assertArrayEquals("byte array isn't the same", Util.getFileBytes(testFile), downloadedFile.getContentAsBytes());
		
	}
	
	/*
	 * Test download file resource
	 */
	@Test
	public void testDownloadFileForResourceUrlWeak() throws IOException, FailingHttpStatusCodeException, URISyntaxException {
		
		testSetUp(ELEMENT_NAME_TEST_FILE_DOWNLOAD_FOR_RESOURCE_URL);
		
		assertEquals("'Content-Length' header doesn't match",
				String.valueOf(Util.getFileBytes(testFile).length),
				downloadedFile.getResponseHeaderValue("Content-Length"));
		
		assertEquals("'Content-Disposition' header doesn't match",
				"attachment;filename=test.pdf",
				downloadedFile.getResponseHeaderValue("Content-Disposition"));	
		
		assertNotNull("'Last-Modified' header must exist", downloadedFile.getResponseHeaderValue("Last-Modified"));
		
		assertTrue("'Content-Type' header doesn't match",
				downloadedFile.getResponseHeaderValue("Content-Type").startsWith("application/pdf"));
		
	}
	
	@Test
	public void testDownloadFileForResourceUrlStrict() throws FailingHttpStatusCodeException, URISyntaxException, IOException {
		
		testSetUp(ELEMENT_NAME_TEST_FILE_DOWNLOAD_FOR_RESOURCE_URL);
		
		assertEquals("'Content-Type' header doesn't match", "application/pdf",
				downloadedFile.getResponseHeaderValue("Content-Type"));
		
	}

	/**
	 * Tests for FileDownloadForPOST
	 */
	@Ignore("Is ignored cause not needed yet.")
	@Test
	public void testDownloadPostFile() throws FailingHttpStatusCodeException, IOException, URISyntaxException {
		
		final File testFile = new File(getClass().getResource("/test.pdf").toURI());

		HtmlPage downloadFileTestPage = getTestCasePage();
		HtmlSubmitInput downloadButton = (HtmlSubmitInput) downloadFileTestPage.getElementById(ELEMENT_NAME_DOWNLOAD_LINK_POST);
		Page result = downloadButton.click();
		if( !(result instanceof UnexpectedPage) ) {
			downloadButton = (HtmlSubmitInput) ((HtmlPage)result).getElementById(R.PORTLET_BYNARY_FILE_RESOURCE_URL_NAME);
		} 
	
		assertTrue(Arrays.equals(Util.getBytes(downloadButton.click().getWebResponse().getContentAsStream(), (int)testFile.length()), Util.getFileBytes(testFile)));
		
	}
	

	private void testSetUp(String ElementId) throws URISyntaxException, FailingHttpStatusCodeException, IOException {
		testFile = new File(getClass().getResource("/test.pdf").toURI());

		HtmlPage downloadFileTestPage = getTestCasePage();
//		downloadFileTestPage.getWebClient().setJavaScriptEnabled(true);
		HtmlAnchor downloadLink = (HtmlAnchor) downloadFileTestPage.getElementById(ElementId);
		Page result = downloadLink.click();
		if( !(result instanceof UnexpectedPage) ) {
			downloadLink = (HtmlAnchor) ((HtmlPage)result).getElementById(R.PORTLET_BYNARY_FILE_RESOURCE_URL_NAME);
		}
	
		downloadedFile = downloadLink.click().getWebResponse();
	}
}
