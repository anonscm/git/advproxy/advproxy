package de.tarent.advproxy.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Before;
import org.junit.Test;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.Cookie;

/**
 * UUID and cookie test cases.
 */
public abstract class AbstractUUIDCookieTest extends AbstractBaseTest {

	protected final String cookieName = "userUUID";
	protected final String cookieVaule = "uuid-test-value";

	/**
	 * Setup this test with testcase name.
	 */
	public AbstractUUIDCookieTest() {
		super("testUUIDCookie");
	}
	
	/**
	 * Setup
	 * 
	 * @throws MalformedURLException
	 */
	@Before
	public void setUp() throws MalformedURLException {
		// add valid userUUID cookie
		final URL cookieUrl = new URL(getIntegrationSystem());
		final String cookieDomain = cookieUrl.getHost();
		getCookieManager().addCookie(new Cookie(cookieDomain, cookieName, cookieVaule));
	}
	
	/**
	 * Test uuid and cookie.
	 * 
	 * @throws FailingHttpStatusCodeException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	@Test
	public void test_validUUIDCookie() throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		
		// get testUUIDCookie page
		HtmlPage cookieTestPage = getTestCasePage();
		
		// check test-div
		HtmlElement testUuidCookieDiv = cookieTestPage.getElementById("testUuidCookie");
		assertNotNull(testUuidCookieDiv);
		
		// check returned cookie values
		
		HtmlElement hasUuidCookie = testUuidCookieDiv.getElementById("hasUuidCookie");
		assertNotNull(hasUuidCookie);
		assertEquals(Boolean.TRUE.toString(), hasUuidCookie.getTextContent());
		
		HtmlElement uuidCookieValue = testUuidCookieDiv.getElementById("uuidCookieValue");
		assertNotNull(uuidCookieValue);
		assertEquals(cookieVaule, uuidCookieValue.getTextContent());
	}
}
