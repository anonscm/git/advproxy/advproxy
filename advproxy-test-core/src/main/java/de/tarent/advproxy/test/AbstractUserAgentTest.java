package de.tarent.advproxy.test;

import java.io.IOException;

import org.junit.Test;

import static org.junit.Assert.*;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

/**
 * Tests if the Header-Information User-Agent is correctly set
 * @author Sabine Höcher, tarent GmbH
 *
 */
public class AbstractUserAgentTest extends AbstractBaseTest{
	
	/**
	 * Setup this test with testcase name.
	 */
	public AbstractUserAgentTest() {
		super("testUsrAgent");
	}

	/**
	 * Test user agent.
	 * 
	 * @throws FailingHttpStatusCodeException
	 * @throws IOException
	 */
	@Test
	public void testUserAgent() throws FailingHttpStatusCodeException, IOException {
		
		// get testUserAgent page
		HtmlPage userAgentTestPage = getTestCasePage();
		HtmlElement testUserAgentDiv = userAgentTestPage.getElementById("testUserAgent");
		assertNotNull(testUserAgentDiv);	
		
		HtmlElement getUserAgent = testUserAgentDiv.getElementById("getUserAgent");
		assertNotNull(getUserAgent);
		assertFalse("User-Agent must not be null", getUserAgent.getTextContent().isEmpty());		
	}

}
