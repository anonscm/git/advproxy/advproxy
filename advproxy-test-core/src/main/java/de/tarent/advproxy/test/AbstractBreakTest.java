package de.tarent.advproxy.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

public abstract class AbstractBreakTest extends AbstractBaseTest{

	/**
	 * Setup this test with testcase name.
	 */
	public AbstractBreakTest() {
		super("testBreak");
	}

	/**
	 * Test BR tags.
	 * 
	 * @throws FailingHttpStatusCodeException
	 * @throws IOException
	 */
	@Test
	public void countBreakTagsTest() throws FailingHttpStatusCodeException, IOException { 
		
		// get testPage
		HtmlPage testPage = getTestCasePage();
		
		// check test-div
		HtmlElement testBreakDiv = testPage.getElementById("testBreakDiv");
		assertNotNull(testBreakDiv);

		// all <br> Tag-"Combinations" should be converted into <br/>Tags
		Pattern p = Pattern.compile("(.*<br/>.*)");
		Matcher m = p.matcher(testBreakDiv.asXml());
		int counter = 0;
		while(m.find()){
			counter++;
		}
		// <br/>Tag should be 4 times in div
		assertEquals(4, counter);
		
		Pattern p2 = Pattern.compile("(.*<br>.*)");
		Matcher m2 = p2.matcher(testBreakDiv.asXml());
		int counter2 = 0;
		while(m2.find()){
			counter2++;
		}
		// <br>Tag should not be in div
		assertEquals(0, counter2);
		
	}

}