package de.tarent.advproxy.test;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.junit.Test;

import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlImage;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlScript;
import com.gargoylesoftware.htmlunit.util.NameValuePair;

/**
 * Test different form request methods.
 */
public class AbstractRequestMethodTest extends AbstractBaseTest {

	public AbstractRequestMethodTest() {
		super("testMethod");
	}
	
	@Test
	public void test_linkRequestMethodType() throws Exception {
		
		HtmlPage testResourceMethod = getTestCasePage();
		
		final HtmlAnchor testLink = (HtmlAnchor) testResourceMethod.getElementById("testLinkUrl");
		assertNotNull("testLink must not be null", testLink);
		
		final Page testLinkPage = testLink.click();
		assertNotNull("testLinkPage must not be null", testLinkPage);
		
		final List<NameValuePair> responseHeaders = testLinkPage.getWebResponse().getResponseHeaders();
		assertNotNull("responseHeaders must not be null", responseHeaders);
		assertTrue("ResponseHeaders must contains RequestMethod=GET", responseHeaders.contains(new NameValuePair("RequestMethod", "GET")));
	}
	
	@Test
	public void test_scriptRequestMethodType() throws Exception {
		
		HtmlPage testResourceMethod = getTestCasePage();
		
		final HtmlScript testScript = (HtmlScript) testResourceMethod.getElementById("testJsUrl");
		assertNotNull("testScript must not be null", testScript);
		
		final String testScriptUrlString = testScript.getSrcAttribute();
		assertNotNull("testScriptUrlString must not be null", testScriptUrlString);
		
		final URL testScriptUrl = checkAndCreateFullUrl(getTestPageUrl(), testScriptUrlString);
		final Page testScriptPage = getPortalTool().getWebClient().getPage(testScriptUrl);
		assertNotNull("testScriptPage must not be null", testScriptPage);

		final List<NameValuePair> responseHeaders = testScriptPage.getWebResponse().getResponseHeaders();
		assertNotNull("responseHeaders must not be null", responseHeaders);
		assertTrue("ResponseHeaders must contains RequestMethod=GET", responseHeaders.contains(new NameValuePair("RequestMethod", "GET")));
	}

	@Test
	public void test_imageRequestMethodType() throws Exception {
		
		HtmlPage testResourceMethod = getTestCasePage();
		
		final HtmlImage testImage = (HtmlImage) testResourceMethod.getElementById("testImgUrl");
		assertNotNull("testImage must not be null", testImage);
		
		final String testImageUrlString = testImage.getSrcAttribute();
		assertNotNull("testImageUrlString must not be null", testImageUrlString);
		
		final URL testImageUrl = checkAndCreateFullUrl(getTestPageUrl(), testImageUrlString);
		final Page testImagePage = getPortalTool().getWebClient().getPage(testImageUrl);
		assertNotNull("testScriptPage must not be null", testImagePage);

		final List<NameValuePair> responseHeaders = testImagePage.getWebResponse().getResponseHeaders();
		assertNotNull("responseHeaders must not be null", responseHeaders);
		assertTrue("ResponseHeaders must contains RequestMethod=GET", responseHeaders.contains(new NameValuePair("RequestMethod", "GET")));
	}
	
	private URL checkAndCreateFullUrl(URL baseUrl, String abstractUrl) throws MalformedURLException {
		try {
			return new URL(abstractUrl);
		}
		catch (MalformedURLException e) {
			return new URL(baseUrl.getProtocol() + "://" + baseUrl.getHost() + ":" + baseUrl.getPort() + abstractUrl);
		}
	}
}
