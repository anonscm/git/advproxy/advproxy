package de.tarent.advproxy.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.gargoylesoftware.htmlunit.html.HtmlAnchor;

/**
 * Different html element tests.
 */
public abstract class AbstractHTMLElementsTest extends AbstractBaseTest{

	/**
	 * Setup this test with testcase name.
	 */
	public AbstractHTMLElementsTest() {
		super("testHTMLElements");
	}


	/**
	 * Html a href=mailto: test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_MailTo() throws Exception {
		
		assertNotNull(getTestCasePage().getElementById("testMailDiv"));

		HtmlAnchor mailToLinkAnchor = (HtmlAnchor) getTestCasePage().getElementById("testMailLink");
		String href = mailToLinkAnchor.getHrefAttribute();
		
		assertTrue("element should start with 'mailto' but is: " + href, href.startsWith("mailto"));

	}

}