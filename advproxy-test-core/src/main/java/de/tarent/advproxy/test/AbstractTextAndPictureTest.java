package de.tarent.advproxy.test;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;

import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlImage;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

/**
 * Different picture tests.
 *
 */
public abstract class AbstractTextAndPictureTest extends AbstractBaseTest{

	/**
	 * Setup this test with testcase name.
	 */
	public AbstractTextAndPictureTest() {
		super("testTextAndPicture");
	}

	/**
	 * Test valid image.
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_validPicture() throws Exception {
		
		// get testImagePage page
		HtmlPage textAndPicturePage = getTestCasePage();
		
		// check test-div
		HtmlElement testTextAndPictureDiv = textAndPicturePage.getElementById("testTextAndPicture");
		assertNotNull(testTextAndPictureDiv);
		
		// image-test
		HtmlImage testImage = (HtmlImage) textAndPicturePage.getElementById("testImage");
		assertNotNull(testImage);
	    assertNotNull(testImage.getImageReader().getInput());
	}

	/**
	 * Test invalid image.
	 * 
	 * @throws Exception
	 */
	@Test(expected = IOException.class)
	public void test_invalidPicture() throws Exception {
		
		// get testImagePage page
		HtmlPage textAndPicturePage = getTestCasePage();
		
		// check test-div
		HtmlElement testTextAndPictureDiv = textAndPicturePage.getElementById("testTextAndPicture");
		assertNotNull(testTextAndPictureDiv);
		 
	    // no image must be detected, getImageReader() must throw an IOException
		HtmlImage testImage2 = (HtmlImage) textAndPicturePage.getElementById("testImage2");
		testImage2.getImageReader();
	}
}