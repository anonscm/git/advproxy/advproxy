package de.tarent.advproxy.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;

import org.junit.Test;

import com.gargoylesoftware.htmlunit.html.HtmlDivision;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlFileInput;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTableCell;
import com.gargoylesoftware.htmlunit.html.HtmlTextArea;

/**
 * Different file upload tests.
 */
public abstract class AbstractFileUploadTest extends AbstractBaseTest {

	/**
	 * Setup this test with testcase name.
	 */
	public AbstractFileUploadTest() {
		super("testFileUpload");
	}

	/**
	 * File upload testcase.
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_fileUpload() throws Exception {
		
		final String name = "Mustermann";
		final String vorname = "Mustafa";
		final String desc = "Test-Beschreibung";
		final File testFile = new File(getClass().getResource("/test.txt").toURI());
		final String testFileName = testFile.getName();
		final long testFileSize = testFile.length();
		
		// get testFileUpload page
		HtmlPage fileUploadPage = getTestCasePage();
		
		// check test-div
		HtmlElement testFileUploadDiv = fileUploadPage.getElementById("testFileUpload");
		assertNotNull(testFileUploadDiv);
		
		// set input data
		testFileUploadDiv.<HtmlInput>getElementById("nameInputField").setValueAttribute(name);
		testFileUploadDiv.<HtmlInput>getElementById("vornameInputField").setValueAttribute(vorname);
		testFileUploadDiv.<HtmlTextArea>getElementById("beschreibungTextArea").setText(desc);
		testFileUploadDiv.<HtmlFileInput>getElementById("dateiFileInput").setValueAttribute(getClass().getResource("/" + testFileName).getFile());
		
		// submit input data 
		HtmlSubmitInput buttonSubmit = testFileUploadDiv.<HtmlSubmitInput>getElementById("fileUploadSubmitButton");
		HtmlPage fileUploadedPage = buttonSubmit.click();
		
		// check fileUploadedPage
		assertNotNull(fileUploadedPage);
		
		HtmlDivision uploadSuccessful = fileUploadedPage.getElementByName("uploadSuccessful");
		assertNotNull(uploadSuccessful);
		
		HtmlTableCell uploaded_datei = fileUploadedPage.getElementByName("uploaded_datei");
		assertEquals(uploaded_datei.getTextContent(), "File, name: '" + testFileName + "', file size: '" + testFileSize + "'");
		
		HtmlTableCell uploaded_nameInputField = fileUploadedPage.getElementByName("uploaded_nameInputField");
		assertEquals(uploaded_nameInputField.getTextContent(), "Simple form field, value: [" + name + "]");
		
		HtmlTableCell uploaded_vornameInputField = fileUploadedPage.getElementByName("uploaded_vornameInputField");
		assertEquals(uploaded_vornameInputField.getTextContent(), "Simple form field, value: [" + vorname + "]");
		
		HtmlTableCell uploaded_beschreibungTextArea = fileUploadedPage.getElementByName("uploaded_beschreibungTextArea");
		assertEquals(uploaded_beschreibungTextArea.getTextContent(), "Simple form field, value: [" + desc + "]");
	}
	

	/**
	 * tests a fileupload with umlaut in formfields and filename 
	 */
	@Test
	public void test_fileUploadWithUmlaut() throws Exception {
		
		final String name = "Müstermann";
		final String vorname = "Müstafa";
		final String desc = "Täst-Beschreibung";
		final File testFile = new File(getClass().getResource("/täst.txt").toURI());
		final String testFileName = testFile.getName();
		final long testFileSize = testFile.length();
		
		// get testFileUpload page
		HtmlPage fileUploadPage = getTestCasePage();
		
		// check test-div
		HtmlElement testFileUploadDiv = fileUploadPage.getElementById("testFileUpload");
		assertNotNull(testFileUploadDiv);
		
		// set input data
		testFileUploadDiv.<HtmlInput>getElementById("nameInputField").setValueAttribute(name);
		testFileUploadDiv.<HtmlInput>getElementById("vornameInputField").setValueAttribute(vorname);
		testFileUploadDiv.<HtmlTextArea>getElementById("beschreibungTextArea").setText(desc);
		testFileUploadDiv.<HtmlFileInput>getElementById("dateiFileInput").setValueAttribute(getClass().getResource("/" + testFileName).getFile());
		
		// submit input data 
		HtmlSubmitInput buttonSubmit = testFileUploadDiv.<HtmlSubmitInput>getElementById("fileUploadSubmitButton");
		HtmlPage fileUploadedPage = buttonSubmit.click();
		
		// check fileUploadedPage
		assertNotNull(fileUploadedPage);
		
//		HtmlDivision uploadSuccessful = fileUploadedPage.getElementByName("uploadSuccessful");
//		assertNotNull(uploadSuccessful);
//		
//		HtmlTableCell uploaded_datei = fileUploadedPage.getElementByName("uploaded_datei");
//		assertEquals(uploaded_datei.getTextContent(), "File, name: '" + testFileName + "', file size: '" + testFileSize + "'");
		
		HtmlTableCell uploaded_nameInputField = fileUploadedPage.getElementByName("uploaded_nameInputField");
		assertEquals(uploaded_nameInputField.getTextContent(), "Simple form field, value: [" + name + "]");
		
		HtmlTableCell uploaded_vornameInputField = fileUploadedPage.getElementByName("uploaded_vornameInputField");
		assertEquals(uploaded_vornameInputField.getTextContent(), "Simple form field, value: [" + vorname + "]");
		
		HtmlTableCell uploaded_beschreibungTextArea = fileUploadedPage.getElementByName("uploaded_beschreibungTextArea");
		assertEquals(uploaded_beschreibungTextArea.getTextContent(), "Simple form field, value: [" + desc + "]");
	}
}
