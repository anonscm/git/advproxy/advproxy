package de.tarent.advproxy.test;

import static org.junit.Assert.*;

import java.io.StringReader;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.junit.Test;

import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.TextPage;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;

/**
 * Different form post tests.
 */
public class AbstractResourcePostForm extends AbstractBaseTest {
	
	/**
	 * Setup this test with testcase name.
	 */
	public AbstractResourcePostForm() {
		super("testResourcePostForm");
	}
	
	/**
	 * Test simple request.
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_testSimpleRequest() throws Exception {
		
		final String vorname = "Mustafa";
		final String name = "Mustermann";
		
		// get testImagePage page
		HtmlPage textAndPostToText = getTestCasePage();
		
		// check test-div
		HtmlElement testTextAnswerDiv = textAndPostToText.getElementById("testResourcePostForm");
		assertNotNull(testTextAnswerDiv);
		
		testTextAnswerDiv.<HtmlInput>getElementById("nameInputField").setValueAttribute(name);
		testTextAnswerDiv.<HtmlInput>getElementById("vornameInputField").setValueAttribute(vorname);
		
		// submit input data 
		HtmlSubmitInput buttonSubmit = testTextAnswerDiv.<HtmlSubmitInput>getElementById("formSubmitButton");
		
		// must return a TEXTpage not HTML
		final Page responsePage = buttonSubmit.click();
		
		assertNotNull(responsePage);
		assertEquals("responsePage must be type of TextPage", TextPage.class, responsePage.getClass());
		
		LineIterator lineIterator = IOUtils.lineIterator(new StringReader(((TextPage)responsePage).getContent().trim()));

		assertEquals("Vorname=" + vorname, lineIterator.nextLine());
		assertEquals("Nachname=" + name, lineIterator.nextLine());
	}
	
	@Test
	public void test_testReservedChars() throws Exception {
		
		final String vorname = "!*'();:@&";
		final String name = "=+$,/?#[]";
		
		// get testImagePage page
		HtmlPage textAndPostToText = getTestCasePage();
		
		// check test-div
		HtmlElement testTextAnswerDiv = textAndPostToText.getElementById("testResourcePostForm");
		assertNotNull(testTextAnswerDiv);
		
		testTextAnswerDiv.<HtmlInput>getElementById("nameInputField").setValueAttribute(name);
		testTextAnswerDiv.<HtmlInput>getElementById("vornameInputField").setValueAttribute(vorname);
		
		// submit input data 
		HtmlSubmitInput buttonSubmit = testTextAnswerDiv.<HtmlSubmitInput>getElementById("formSubmitButton");
		
		// must return a TEXTpage not HTML
		final Page responsePage = buttonSubmit.click();
		
		assertNotNull(responsePage);
		assertEquals("responsePage must be type of TextPage", TextPage.class, responsePage.getClass());
		
		LineIterator lineIterator = IOUtils.lineIterator(new StringReader(((TextPage)responsePage).getContent().trim()));

		assertEquals("Vorname=" + vorname, lineIterator.nextLine());
		assertEquals("Nachname=" + name, lineIterator.nextLine());
	}
	
	@Test
	public void test_testSpecChars() throws Exception {
		
		final String vorname = "äöüß";
		final String name = "^°!{}~";
		
		// get testImagePage page
		HtmlPage textAndPostToText = getTestCasePage();
		
		// check test-div
		HtmlElement testTextAnswerDiv = textAndPostToText.getElementById("testResourcePostForm");
		assertNotNull(testTextAnswerDiv);
		
		testTextAnswerDiv.<HtmlInput>getElementById("nameInputField").setValueAttribute(name);
		testTextAnswerDiv.<HtmlInput>getElementById("vornameInputField").setValueAttribute(vorname);
		
		// submit input data 
		HtmlSubmitInput buttonSubmit = testTextAnswerDiv.<HtmlSubmitInput>getElementById("formSubmitButton");
		
		// must return a TEXTpage not HTML
		final Page responsePage = buttonSubmit.click();
		
		assertNotNull(responsePage);
		assertEquals("responsePage must be type of TextPage", TextPage.class, responsePage.getClass());
		
		LineIterator lineIterator = IOUtils.lineIterator(new StringReader(((TextPage)responsePage).getContent().trim()));

		assertEquals("Vorname=" + vorname, lineIterator.nextLine());
		assertEquals("Nachname=" + name, lineIterator.nextLine());
	}
}
