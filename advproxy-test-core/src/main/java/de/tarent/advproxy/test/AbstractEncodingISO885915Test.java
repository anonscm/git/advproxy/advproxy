package de.tarent.advproxy.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.MalformedURLException;

import org.junit.Test;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

/**
 * This Test-Case calls an ISO-8859-15 encoded page and check the received HTTP-Header 
 * and the correct encoded special characters.
 * 
 * @author Tino Rink <t.rink@tarent.de>
 */
public abstract class AbstractEncodingISO885915Test extends AbstractBaseTest {
	
	protected final String testString = "!\"§$%&/()=?Ü*ÖÄ'@€<>";

	protected final String aspectedEncoding;

	/**
	 * @param aspectedEncoding defines the encoding when the test case is called. When a webapp is called, 
	 * "ISO-8859-15" should be aspected, if the testpage is called via the advproxy-portlet then "UTF-8"
	 * should be aspected.
	 */
	public AbstractEncodingISO885915Test(String aspectedEncoding) {
		super("testEncodingISO885915");
		this.aspectedEncoding = aspectedEncoding;
	}
	
	/**
	 * Simple encoding test.
	 * 
	 * @throws FailingHttpStatusCodeException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	@Test
	public void test_validEncoding() throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		
		// get testUUIDCookie page
		HtmlPage encodingTestPage = getTestCasePage();
		
		final String pageEncoding = encodingTestPage.getPageEncoding();	
		assertTrue("test failded, encoding must be: " + aspectedEncoding + ", but was: " + pageEncoding, aspectedEncoding.equalsIgnoreCase(pageEncoding));
		
		final String returnedTestString = encodingTestPage.getElementById("testString").asText();
		assertEquals(testString, returnedTestString);
	}
}
