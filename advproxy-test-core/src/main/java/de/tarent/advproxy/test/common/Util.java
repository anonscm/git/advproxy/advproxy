package de.tarent.advproxy.test.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Iterator;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;

/**
 * Utitily class.
 */
public class Util {

	/**
	 * Logs into the passed liferay portal login page using the passed credentials.
	 * The returned {@link WebClient} contains necessary session informations for the logged in user.
	 * @param logInPageUrl
	 * @param loginRedirectUrl
	 * @param userName
	 * @param password
	 * @return
	 * @throws FailingHttpStatusCodeException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public static void loginToLiferayPortal(WebClient client, String logInPageUrl, String loginRedirectUrl, String userName, String password)
		throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		HtmlPage loginPortlet = client.getPage(logInPageUrl);
		String loginElementPrefix = "_" +  retrievePortletId(loginPortlet.getUrl().toString(), LiferaySetting.PORTLET_ID_DESIGNATOR) + "_";
		
		((HtmlInput)loginPortlet.getElementByName(loginElementPrefix + LiferaySetting.LOGINPORTLET_LOGIN_NAME)).setValueAttribute(userName);
		((HtmlPasswordInput)loginPortlet.getElementByName(loginElementPrefix + LiferaySetting.LOGINPORTLET_PASSWORD_NAME)). setValueAttribute(password);
		((HtmlInput)loginPortlet.getElementByName(loginElementPrefix + LiferaySetting.LOGINPORTLET_REDIRECT_NAME)).setValueAttribute(loginRedirectUrl);
		HtmlSubmitInput signInButton = (HtmlSubmitInput) retrieveFirstElementWithValue(
			loginPortlet.getFormByName(loginElementPrefix + LiferaySetting.LOGINPORTLET_FORM_NAME), LiferaySetting.LOGINPORTLET_SINGIN_BUTTON_NAME);
		
		signInButton.click();
	}

	/**
	 * Return the first html element which contains an value attribute or
	 * null if no element was found.
	 * 
	 * @param root
	 * @param value
	 * @return
	 */
	public static HtmlElement retrieveFirstElementWithValue(HtmlElement root, String value) {
		if( null == root || null == root.getHtmlElementDescendants() || null == root.getHtmlElementDescendants().iterator() ) {
			return null;
		}
		
		Iterator<HtmlElement> it = root.getHtmlElementDescendants().iterator();
		while(it.hasNext()) {
			HtmlElement e = it.next();
			if( e.getAttribute("value").compareTo(value) == 0 ) {
				return e;
			}
		}
		
		return null;
	}

	/**
	 * Return the protlet if of the given portlet url.
	 * 
	 * @param portletURL
	 * @param portletIdDesignator
	 * @return
	 */
	public static String retrievePortletId(String portletURL, String portletIdDesignator) {
		if( null == portletURL || !portletURL.contains(portletIdDesignator) ) {
			return null;
		}
		portletURL = portletURL.substring(portletURL.indexOf(portletIdDesignator));
		int portletIdDelimmiterIndex = portletURL.indexOf(LiferaySetting.PORTLET_ID_DELIMMITER);
		if( portletIdDelimmiterIndex > 0 ) {
			return portletURL.substring(LiferaySetting.PORTLET_ID_DESIGNATOR.length() + 1, portletIdDelimmiterIndex);
		} else {
			return portletURL;
		}
	}
	
	/**
	 * Return the given file as byte array.
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static byte[] getFileBytes(File file) throws IOException {
		return getBytes(new FileInputStream(file), (int) file.length());
	}
	
	/**
	 * Return the given inputstream as byte array and close the inputstream.
	 * 
	 * @param inputStream
	 * @param length
	 * @return
	 * @throws IOException
	 */
	public static byte[] getBytes(InputStream inputStream, int length) throws IOException {
        byte[] data = new byte[length];
        inputStream.read(data);
        inputStream.close();
        return data;
	}
}
