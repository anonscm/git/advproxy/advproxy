package de.tarent.advproxy.test;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import de.tarent.advproxy.test.concurrency.Concurrency;

/**
 * Differernt cookie testcases.
 */
/*
 * Cookies could not be tested parallel!
 */
@Concurrency(times = 1, parallelThreads = 1)
public abstract class AbstractCookieTest extends AbstractBaseTest {
	
	protected final String cookieName = "testCookieName";
	protected final String cookieValue = "testCookieValue";
	
	/**
	 * Setup this test with testcase name.
	 */
	public AbstractCookieTest() {
		super("testCookie");
	}

	/**
	 * Simple set and and get cookie test.
	 * 
	 * @throws FailingHttpStatusCodeException
	 * @throws IOException
	 */
	@Test
	public void test_Cookie() throws FailingHttpStatusCodeException, IOException {
		
		HtmlPage cookieTestPage = getTestCasePage();
		
		assertNotNull("cookie testCookieName must not be null", getCookieManager().getCookie("testCookieName"));
		assertEquals("testCookieValue", getCookieManager().getCookie("testCookieName").getValue());
		assertEquals("testCookieName", getCookieManager().getCookie("testCookieName").getName());
		
		HtmlPage pageWithCookies = cookieTestPage.getElementById("showTestCookie").click();

		HtmlElement testCookieName = pageWithCookies.getElementById("testCookieName");
		
		assertEquals(cookieName, testCookieName.getTextContent());
		
		HtmlElement testCookieValue = pageWithCookies.getElementById("testCookieValue");
		
		assertEquals(cookieValue, testCookieValue.getTextContent());
		
		
	}
	
}
