package de.tarent.advproxy.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.gargoylesoftware.htmlunit.html.HtmlPage;

/**
 * Testcases for different Redirects
 * 
 * @author Sabine Höcher
 * @author Tino Rink
 *
 */
public class AbstractRedirectsTest extends AbstractBaseTest {
	
	protected final String targetLocation = "http://www.heise.de/";
	
	/**
	 * Setup this test with testcase name.
	 */
	public AbstractRedirectsTest() {
		super("testRedirects");
	}
	
	/**
	 * Tests the simple intern get redirect which should lead to the intern targetPage of the redirect
	 */
	@Test
	public void testSimpleInternGetRedirect() throws Exception {

		getTestCasePage().getWebClient().setRedirectEnabled(true);
		
		HtmlPage testGetRedirect = getTestCasePage().getElementById("testSimpleInternGETRedirectPage").click();

		assertNotNull(testGetRedirect.getElementById("testInternRedirectText"));
	}
	
	/**
	 * Tests the simple intern post redirect which should lead to the intern targetPage of the redirect
	 */
	@Test
	public void testPostRedirect() throws Exception {
		getTestCasePage().getWebClient().setRedirectEnabled(true);

		HtmlPage testGetRedirect = getTestCasePage().getElementById("internPostFormSubmitButton").click();
		
		assertNotNull(testGetRedirect.getElementById("testInternRedirectText"));
	}
	
	/**
	 * Tests the simple extern get redirect which should lead to an external page
	 */
	@Test
	public void testExternRedirect() throws Exception {
		final HtmlPage externalPage = getTestCasePage().getElementById("testSimpleGETRedirectLink").click();
		assertEquals(targetLocation, externalPage.getUrl().toExternalForm());
	}
	
	/**
	 * Tests the linked intern get-post redirect which should lead to the intern targetPage of the redirect
	 */
	@Test
	public void testLinkedGetPostRedirect() throws Exception {

		getTestCasePage().getWebClient().setRedirectEnabled(true);
		
		HtmlPage testLinkedGetRedirect = getTestCasePage().getElementById("postGetFormSubmitButton").click();

		assertNotNull(testLinkedGetRedirect.getElementById("testInternRedirectText"));
	}
	
	/**
	 * Tests the linked intern get redirect which should lead to the intern targetPage of the redirect
	 */
	@Test
	public void testLinkedGetRedirect() throws Exception {

		getTestCasePage().getWebClient().setRedirectEnabled(true);
		
		HtmlPage testLinkedGetRedirect = getTestCasePage().getElementById("testLinkedGETRedirectPage").click();

		assertNotNull(testLinkedGetRedirect.getElementById("testInternRedirectText"));
	}

}
