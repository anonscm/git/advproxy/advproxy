package de.tarent.advproxy.test;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

/**
 * Different session urls.
 */
public abstract class AbstractSessionUrlTest extends AbstractBaseTest {
	
	private final String vaildUrlPart = "/html/testSessionUrlResult;kfmsession=qayxsw1234edcvfr";
	
	/**
	 * Setup this test with testcase name.
	 */
	public AbstractSessionUrlTest() {
		super("testSessionUrl");
	}
	
	/**
	 * Test session links.
	 * 
	 * @throws FailingHttpStatusCodeException
	 * @throws IOException
	 */
	@Test
	public void testSessionLink() throws FailingHttpStatusCodeException, IOException {
		
		final HtmlElement testSessionUrlLink = getTestCasePage().getElementById("testSessionUrlLink");
		assertNotNull("testSessionUrlLink must exist", testSessionUrlLink);
		
		final HtmlPage sessionUrlLinkPage = testSessionUrlLink.click();
		assertNotNull("sessionUrlLinkPage must exist", sessionUrlLinkPage);
		
		final HtmlElement testSessionUrlResult = sessionUrlLinkPage.getElementById("testSessionUrlResult");
		assertNotNull("testSessionUrlResult must exist", testSessionUrlResult);
		assertTrue("testSessionUrlResult must contains correct session informations",
				testSessionUrlResult.asText().endsWith(vaildUrlPart));
	}

	/**
	 * Test session form.
	 * 
	 * @throws FailingHttpStatusCodeException
	 * @throws IOException
	 */
	@Test
	public void testSessionForm() throws FailingHttpStatusCodeException, IOException {
		
		final HtmlElement formButton = getTestCasePage().getElementById("testSessionUrlFormButton");
		assertNotNull("testSessionUrlForm must exist", formButton);
		
		final HtmlPage sessionUrlFormPage = formButton.click();
		assertNotNull("sessionUrlFormPage must exist", sessionUrlFormPage);
		
		final HtmlElement testSessionUrlResult = sessionUrlFormPage.getElementById("testSessionUrlResult");
		assertNotNull("testSessionUrlResult must exist", testSessionUrlResult);
		assertTrue("testSessionUrlResult must contains correct session informations",
				testSessionUrlResult.asText().endsWith(vaildUrlPart));
	}
}
