Settings for successful run of integration tests:
- a running liferay portal with
  * deployed advproxy portlet,
  * admin user to reach edit mode of advproxy portlet.
- a running application server with
  * deployed test web applications.
  
All necessary setting have to be set in src/main/resources/tarent.properties.

Amongst others testing through WSRP can be switched on/off by setting the appropriate flag.