package de.tarent.advproxy.portlet;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import de.tarent.advproxy.test.AbstractFileUploadTest;
import de.tarent.advproxy.test.concurrency.Concurrency;
import de.tarent.advproxy.test.concurrency.ConcurrencyJunitRunner;

@RunWith(ConcurrencyJunitRunner.class)
@Concurrency(times = 200)
public class IT_FileUpload extends AbstractFileUploadTest {
	
	@Test
	/**
	 * tests if the accepted-charset is set to UTF-8 in the formular
	 */
	public void test_acceptedCharset() throws FailingHttpStatusCodeException, IOException {
		
		HtmlPage fileUploadPage = getTestCasePage();
		
		HtmlForm form = fileUploadPage.getFormByName("formular");
		String acceptCharset = form.getAttribute("accept-charset");
		// the accepted charset should be set to utf-8 in the formulars in advproxyportlet integration-tests
		assertEquals("UTF-8", acceptCharset);
		
		// checks if the accepted-charset is set in a form without any accept-charset
		HtmlForm formNoCharset = fileUploadPage.getFormByName("testFormNoCharset");
		String acceptCharset2 = formNoCharset.getAttribute("accept-charset");
		assertEquals("UTF-8", acceptCharset2);
	}
}
