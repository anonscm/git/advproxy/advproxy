package de.tarent.advproxy.portlet;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.MalformedURLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import de.tarent.advproxy.test.AbstractBaseTest;
import de.tarent.advproxy.test.concurrency.Concurrency;
import de.tarent.advproxy.test.concurrency.ConcurrencyJunitRunner;

@RunWith(ConcurrencyJunitRunner.class)
@Concurrency(times = 200)
public class IT_UUIDCookie extends AbstractBaseTest {
	
	private static final String cookieName = "userUUID";
	
	public IT_UUIDCookie() {
		super("testUUIDCookie");
	}
	
	@Before
	public void init() throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		getPortalTool().login(null, getLogin(), getPassword());
	}
	
	@After
	public final void logout() throws MalformedURLException {
		portalTool.logout();
	}
	
	/**
	 * Positive test: enabling specific preferences which have to enable for creating and sending user UUID as cookie.
	 * @throws FailingHttpStatusCodeException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	@Test
	public void testUserUUIDCookieSupport() throws FailingHttpStatusCodeException, MalformedURLException, IOException {

		HtmlPage uuidCookieTestPortletThroughAdvproxy = getTestCasePage();

		// Verifying that cookies contain user UUID
		HtmlElement hasUuidCookie = uuidCookieTestPortletThroughAdvproxy.getElementById("hasUuidCookie");
		assertTrue(Boolean.parseBoolean(hasUuidCookie.getTextContent()));
		
		HtmlElement uuidCookieValue = uuidCookieTestPortletThroughAdvproxy.getElementById("uuidCookieValue");
		assertTrue(uuidCookieValue.getTextContent().trim().length() > 0);
	}
	
//	/**
//	 * Negative text
//	 * 
//	 * @throws FailingHttpStatusCodeException
//	 * @throws MalformedURLException
//	 * @throws IOException
//	 */
//	@Test
//	public void testUserUUIDCookieSupportNegative() throws FailingHttpStatusCodeException, MalformedURLException, IOException {
//
//		setAdvproxyPortletPreferencesGenerateUUIDCookie(Boolean.FALSE, cookieName);
//		
//		HtmlPage uuidCookieTestPortletThroughAdvproxy = getTestCasePage();
//
//		HtmlElement hasUuidCookie = uuidCookieTestPortletThroughAdvproxy.getElementById("hasUuidCookie");
//		assertTrue(!Boolean.parseBoolean(hasUuidCookie.getTextContent()));
//		
//		HtmlElement uuidCookieValue = uuidCookieTestPortletThroughAdvproxy.getElementById("uuidCookieValue");
//		assertTrue(uuidCookieValue.getTextContent().trim().length() == 0);
//	}
}
