package de.tarent.advproxy.portlet;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.gargoylesoftware.htmlunit.html.HtmlPage;

import de.tarent.advproxy.test.AbstractRedirectsTest;
import de.tarent.advproxy.test.concurrency.Concurrency;
import de.tarent.advproxy.test.concurrency.ConcurrencyJunitRunner;

/**
 * @author Tino Rink
 * @author Sabine Höcher
 */
@RunWith(ConcurrencyJunitRunner.class)
@Concurrency(times = 200)
public class IT_Redirects extends AbstractRedirectsTest{
	
	@Test
	@Override
	public void testExternRedirect() throws Exception {
		// we can only test without JS
		getBrowser().setJavaScriptEnabled(false);
		final HtmlPage testPage = getTestCasePage();
		final HtmlPage redirectingPage = testPage.getElementById("testSimpleGETRedirectLink").click();
		final HtmlPage externPage = redirectingPage.getElementById("linkToExtern").click();
		assertEquals(targetLocation, externPage.getUrl().toString());
	}

}
