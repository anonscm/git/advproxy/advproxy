package de.tarent.advproxy.portlet;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.evolvis.advproxy.portalTools.GetSetAdvProxyPortletSettings;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import de.tarent.advproxy.test.AbstractJavaScriptTest;
import de.tarent.advproxy.test.concurrency.Concurrency;
import de.tarent.advproxy.test.concurrency.ConcurrencyJunitRunner;

@RunWith(ConcurrencyJunitRunner.class)
@Concurrency(times = 200)
public class IT_JavaScript extends AbstractJavaScriptTest {
	
	private GetSetAdvProxyPortletSettings setAdvSettings;

	final Collection<String> defaultExcludedJavaScripts = Arrays.asList("");
	
	@Before
	public void init() throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		//settings especially for each JStest are set in the tests
		getPortalTool().login(null, getLogin(), getPassword());
		setAdvSettings = new GetSetAdvProxyPortletSettings();
		setAdvSettings.setPortletSite(getTestIndexPage().getUrl());
		setAdvSettings.setExcludedJavaScripts(defaultExcludedJavaScripts);
	}
	
	@After
	public final void logout() throws MalformedURLException {
		portalTool.logout();
	}

	/**
	 * Fails if a script-element is written more or less than once, checked by id of script-tag.
	 */
	@Test
	public void testFrequencyOfScriptTags() throws FailingHttpStatusCodeException, IOException {
		
		// setup test
		setAdvSettings.setEnableJavaScript(true);
		setAdvSettings.setExcludedJavaScripts(defaultExcludedJavaScripts);
		getPortalTool().execute(setAdvSettings);
		
		final HtmlPage testPage = getTestCasePage();
		final HtmlElement endDiv = testPage.getElementById("endDiv");
		assertNotNull("endDiv must not be null", endDiv);
		
		List<?> scriptElementBodySnippetInclude = testPage.getByXPath("//script[@id='bodySnippetInclude']");		
		assertTrue("scriptElementBodySnippetInclude be 1, but is " + scriptElementBodySnippetInclude.size(),
				scriptElementBodySnippetInclude.size() == 1);

		List<?> scriptElementHeadSnippetInclude = testPage.getByXPath("//script[@id='headSnippetInclude']");
		assertTrue("scriptElementHeadSnippetInclude must be 1, but is " + scriptElementHeadSnippetInclude.size(),
				scriptElementHeadSnippetInclude.size() == 1);
		
		List<?> scriptElementBodyFileInclude = testPage.getByXPath("//script[@id='bodyFileInclude']");
		assertTrue("scriptElementBodyFileInclude must be 1, but is " + scriptElementBodyFileInclude.size(),
				scriptElementBodyFileInclude.size() == 1);
		
		List<?> scriptElementHeadFileInclude = testPage.getByXPath("//script[@id='headFileInclude']");
		assertTrue("scriptElementHeadFileInclude must be 1, but is " + scriptElementHeadFileInclude.size(),
				scriptElementHeadFileInclude.size() == 1);
		
	}
	
	/**
	 * If javascript is not enabled, javascript-snippets should not be found.
	 */
	@Test
	public void testFrequencyOfScriptTagsIfNotEnabled() throws FailingHttpStatusCodeException, IOException {
		
		// setup test
		setAdvSettings.setEnableJavaScript(false);
		setAdvSettings.setExcludedJavaScripts(defaultExcludedJavaScripts);
		getPortalTool().execute(setAdvSettings);
		
		final HtmlPage testPage = getTestCasePage();
		final HtmlElement endDiv = testPage.getElementById("endDiv");
		assertNotNull("endDiv must not be null", endDiv);
		
		List<?> scriptElementBodySnippetInclude = testPage.getByXPath("//script[@id='bodySnippetInclude']");		
		assertTrue("scriptElementBodySnippetInclude be 0, but is " + scriptElementBodySnippetInclude.size(),
				scriptElementBodySnippetInclude.size() == 0);

		List<?> scriptElementHeadSnippetInclude = testPage.getByXPath("//script[@id='headSnippetInclude']");
		assertTrue("scriptElementHeadSnippetInclude must be 0, but is " + scriptElementHeadSnippetInclude.size(),
				scriptElementHeadSnippetInclude.size() == 0);
		
		List<?> scriptElementBodyFileInclude = testPage.getByXPath("//script[@id='bodyFileInclude']");
		assertTrue("scriptElementBodyFileInclude must be 0, but is " + scriptElementBodyFileInclude.size(),
				scriptElementBodyFileInclude.size() == 0);
		
		List<?> scriptElementHeadFileInclude = testPage.getByXPath("//script[@id='headFileInclude']");
		assertTrue("scriptElementHeadFileInclude must be 0, but is " + scriptElementHeadFileInclude.size(),
				scriptElementHeadFileInclude.size() == 0);
		
	}
	
	/**
	 * Fails if there is any more script element in portlet after body-content of webapplication.
	 */
	@Test
	public void testLocationOfJSEnd() throws FailingHttpStatusCodeException, IOException {
		
		// setup test
		setAdvSettings.setEnableJavaScript(true);
		setAdvSettings.setExcludedJavaScripts(defaultExcludedJavaScripts);
		getPortalTool().execute(setAdvSettings);
		
		final HtmlPage testPage = getTestCasePage();
		final HtmlElement bodyDiv = testPage.getElementById("bodyDiv");
		assertNotNull("bodyDiv must not be null", bodyDiv);
		DomNode script = bodyDiv.getNextSibling();

		assertNull("it shouldn´t exist any next JS sibling to the body", script);

	}
	
	/**
	 * Tests if there are all javascript elements of head of the webapplication before content of portlet.
	 */
	@Test
	public void testLocationOfJSHeadAtBeginning() throws FailingHttpStatusCodeException, IOException {
		
		// setup test
		setAdvSettings.setEnableJavaScript(true);
		setAdvSettings.setExcludedJavaScripts(defaultExcludedJavaScripts);
		getPortalTool().execute(setAdvSettings);
		
		final HtmlPage testPage = getTestCasePage();
		final HtmlElement bodyDiv = testPage.getElementById("bodyDiv");
		assertNotNull("bodyDiv must not be null", bodyDiv);
		
		DomNode script1 = bodyDiv.getPreviousSibling();
		assertNotNull("there should exist all script elements before the content of the portlet", script1);
		assertTrue("DomNode1 should be a script node", script1.getNodeName().equalsIgnoreCase("script"));
		
		DomNode script2 = script1.getPreviousSibling();
		assertNotNull("there should exist all script elements before the content of the portlet", script2);
		assertTrue("DomNode2 should be a script node", script2.getNodeName().equalsIgnoreCase("script"));

	}
	
	/**
	 * Tests if all javascript of head is included.
	 */
	@Test
	public void testJSActivatedAndNotExcludedHead() throws FailingHttpStatusCodeException, IOException {
		
		// setup test
		setAdvSettings.setEnableJavaScript(true);
		setAdvSettings.setExcludedJavaScripts(defaultExcludedJavaScripts);
		getPortalTool().execute(setAdvSettings);

		final HtmlPage testPage = getTestCasePage();
		
		final HtmlElement headFileInclude = testPage.getElementById("headFileInclude");
		assertNotNull("headFileInclude must not be null", headFileInclude);
		
		final HtmlElement headSnippetInclude = testPage.getElementById("headSnippetInclude");
		assertNotNull("headSnippetInclude must not be null", headSnippetInclude);
		
	}
	
	/**
	 * Tests if all javascript of body is included.
	 */
	@Test
	public void testJSActivatedAndNotExcludedBody() throws FailingHttpStatusCodeException, IOException {
		
		// setup test
		setAdvSettings.setEnableJavaScript(true);
		setAdvSettings.setExcludedJavaScripts(defaultExcludedJavaScripts);
		getPortalTool().execute(setAdvSettings);
		
		final HtmlPage testPage = getTestCasePage();

		//tests the snippet of javascript in the html page
		final HtmlElement bodySnippetInclude = testPage.getElementById("bodySnippetInclude");
		assertNotNull("bodySnippetInclude must not be null", bodySnippetInclude);

		//tests the included js-file
		HtmlElement bodyFileInclude = testPage.getElementById("bodyFileInclude");
		assertNotNull("bodyFileInclude must not be null", bodyFileInclude);

	}
	
	/**
	 * Fails if javascript of excluded Url is included in portlet.
	 */
	@Test
	public void testExcludedJSURLsHead () throws FailingHttpStatusCodeException, IOException {
		
		final Collection<String> excludedJSURLsHead = Arrays.asList(".*/jsSnippetOfHead.js");
		
		// setup test
		setAdvSettings.setEnableJavaScript(true);
		setAdvSettings.setExcludedJavaScripts(excludedJSURLsHead);
		getPortalTool().execute(setAdvSettings);
		
		final HtmlPage testPage = getTestCasePage();
		
		final HtmlElement headFileInclude = testPage.getElementById("headFileInclude");
		assertNull("headFileInclude must be null", headFileInclude);
		
	}
	
	/**
	 * Fails if javascript of excluded Url is included in portlet.
	 */
	@Test
	public void testExcludedJSURLsBody () throws FailingHttpStatusCodeException, IOException {
		
		final Collection<String> excludedJSURLsBody = Arrays.asList(".*/jsSnippetOfBody.js");
		
		// setup test
		setAdvSettings.setEnableJavaScript(true);
		setAdvSettings.setExcludedJavaScripts(excludedJSURLsBody);
		getPortalTool().execute(setAdvSettings);
		
		final HtmlPage testPage = getTestCasePage();
		
		final HtmlElement bodyFileInclude = testPage.getElementById("bodyFileInclude");
		assertNull("bodyFileInclude must be null", bodyFileInclude);
		
	}
	
	@Test 
	public void testBothJSExcluded() throws FailingHttpStatusCodeException, IOException {
		
		final Collection<String> excludedJSURLs = Arrays.asList(".*/jsSnippetOfBody.js", ".*/jsSnippetOfHead.js");
		
		// setup test
		setAdvSettings.setEnableJavaScript(true);
		setAdvSettings.setExcludedJavaScripts(excludedJSURLs);
		getPortalTool().execute(setAdvSettings);	
		
		final HtmlPage testPage = getTestCasePage();
		
		final HtmlElement headFileInclude = testPage.getElementById("headFileInclude");
		assertNull("headFileInclude must be null", headFileInclude);
		
		final HtmlElement bodyFileInclude = testPage.getElementById("bodyFileInclude");
		assertNull("bodyFileInclude must be null", bodyFileInclude);
	}
	
	
	/**
	 * This case should not happen but you never know.
	 */
	@Test
	public void testDisabledJSAndExcludedFiles() throws FailingHttpStatusCodeException, IOException {
		
		final Collection<String> excludedJSURLs = Arrays.asList(".*/jsSnippetOfBody.js", ".*/jsSnippetOfHead.js");
		
		// setup test
		setAdvSettings.setEnableJavaScript(false);
		setAdvSettings.setExcludedJavaScripts(excludedJSURLs);
		getPortalTool().execute(setAdvSettings);	
		
		final HtmlPage testPage = getTestCasePage();
		
		List<?> scriptElementBodySnippetInclude = testPage.getByXPath("//script[@id='bodySnippetInclude']");		
		assertTrue("scriptElementBodySnippetInclude be 0, but is " + scriptElementBodySnippetInclude.size(),
				scriptElementBodySnippetInclude.size() == 0);

		List<?> scriptElementHeadSnippetInclude = testPage.getByXPath("//script[@id='headSnippetInclude']");
		assertTrue("scriptElementHeadSnippetInclude must be 0, but is " + scriptElementHeadSnippetInclude.size(),
				scriptElementHeadSnippetInclude.size() == 0);
		
		List<?> scriptElementBodyFileInclude = testPage.getByXPath("//script[@id='bodyFileInclude']");
		assertTrue("scriptElementBodyFileInclude must be 0, but is " + scriptElementBodyFileInclude.size(),
				scriptElementBodyFileInclude.size() == 0);
		
		List<?> scriptElementHeadFileInclude = testPage.getByXPath("//script[@id='headFileInclude']");
		assertTrue("scriptElementHeadFileInclude must be 0, but is " + scriptElementHeadFileInclude.size(),
				scriptElementHeadFileInclude.size() == 0);
	}

}
