package de.tarent.advproxy.portlet;

import org.junit.runner.RunWith;

import de.tarent.advproxy.test.AbstractFileDownloadsTest;
import de.tarent.advproxy.test.concurrency.Concurrency;
import de.tarent.advproxy.test.concurrency.ConcurrencyJunitRunner;

@RunWith(ConcurrencyJunitRunner.class)
@Concurrency(times = 200)
public class IT_FileDownloads extends AbstractFileDownloadsTest {}
