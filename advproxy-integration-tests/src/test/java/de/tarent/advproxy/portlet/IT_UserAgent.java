package de.tarent.advproxy.portlet;

import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.net.MalformedURLException;

import org.evolvis.advproxy.portalTools.GetSetAdvProxyPortletSettings;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import de.tarent.advproxy.test.AbstractUserAgentTest;
import de.tarent.advproxy.test.concurrency.Concurrency;
import de.tarent.advproxy.test.concurrency.ConcurrencyJunitRunner;

@RunWith(ConcurrencyJunitRunner.class)
@Concurrency(times = 200)
public class IT_UserAgent extends AbstractUserAgentTest {
	
	private GetSetAdvProxyPortletSettings setAdvSettings;
	private String userAgent = "test_userAgent";
	
	@Before
	public void init() throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		//settings especially for the userAgentTest
		getPortalTool().login(null, getLogin(), getPassword());
		setAdvSettings = new GetSetAdvProxyPortletSettings();
		setAdvSettings.setPortletSite(getTestIndexPage().getUrl());
	}
	
	@After
	public final void logout() throws MalformedURLException {
		portalTool.logout();
	}
	
	@Test
	public void testSetUserAgent() throws FailingHttpStatusCodeException, IOException {
		
		setAdvSettings.setUserAgent(userAgent);
		getPortalTool().execute(setAdvSettings);
		
		// get testUserAgent page
		HtmlPage userAgentTestPage = getTestCasePage();
		HtmlElement testUserAgentDiv = userAgentTestPage.getElementById("testUserAgent");
		assertNotNull(testUserAgentDiv);	
		
		HtmlElement getUserAgent = testUserAgentDiv.getElementById("getUserAgent");
		assertNotNull(getUserAgent);
		assertFalse("User-Agent must not be null", getUserAgent.getTextContent().isEmpty());
		assertEquals("User-Agent must be " + userAgent + " but was " + getUserAgent.getTextContent(), userAgent, getUserAgent.getTextContent());	
	}
	
	@Test
	public void testDefaultUserAgent() throws FailingHttpStatusCodeException, IOException {
		
		setAdvSettings.setUserAgent("");
		getPortalTool().execute(setAdvSettings);
		
		// get testUserAgent page
		HtmlPage userAgentTestPage = getTestCasePage();
		HtmlElement testUserAgentDiv = userAgentTestPage.getElementById("testUserAgent");
		assertNotNull(testUserAgentDiv);	
		
		HtmlElement getUserAgent = testUserAgentDiv.getElementById("getUserAgent");
		assertNotNull(getUserAgent);
		assertFalse("User-Agent must not be empty", getUserAgent.getTextContent().isEmpty());
		
	}

}
