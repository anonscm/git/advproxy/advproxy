package de.tarent.advproxy.portlet;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.MalformedURLException;

import org.evolvis.advproxy.portalTools.GetSetAdvProxyPortletSettings;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlElement;

import de.tarent.advproxy.test.AbstractHTMLElementsTest;
import de.tarent.advproxy.test.concurrency.Concurrency;
import de.tarent.advproxy.test.concurrency.ConcurrencyJunitRunner;

@RunWith(ConcurrencyJunitRunner.class)
@Concurrency(times = 200)
public class IT_HTMLElements extends AbstractHTMLElementsTest {
	
	private GetSetAdvProxyPortletSettings setAdvSettings;
	final private String CLASS_SUFFIX = "testSuffix";
	final private String ANCHOR_ID = "beginning";
	final private String ANCHOR_NAME = "beginningAnchorName";
	
	
	@Before
	public void init() throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		//settings especially for the anchorTest
		getPortalTool().login(null, getLogin(), getPassword());
		setAdvSettings = new GetSetAdvProxyPortletSettings();
		setAdvSettings.setPortletSite(getTestIndexPage().getUrl());
	}
	
	@After
	public final void logout() throws MalformedURLException {
		portalTool.logout();
	}
	
	/**
	 * Tests if Anchor ("#") is working without a given Suffix in Properties
	 */
	@Test
	public void test_AnchorWithoutSuffixId() throws Exception {
		
		setAdvSettings.setIdRewriteEnabled(false);
		setAdvSettings.setClassRewriteEnabled(false);
		setAdvSettings.setNameRewriteEnabled(false);
		setAdvSettings.setClassRewriteForced(false);
		setAdvSettings.setClassSuffix("");
		getPortalTool().execute(setAdvSettings);
	
		assertNotNull(getTestCasePage().getElementById("testAnchorDiv"));

		HtmlAnchor anchorTest1 = (HtmlAnchor) getTestCasePage().getElementById("anchor1");
		String href1 = anchorTest1.getHrefAttribute();
		
		assertTrue("element includes no #  ", href1.contains("#"));
		
		HtmlAnchor anchorTest2 = (HtmlAnchor) getTestCasePage().getElementById("anchor2");
		String href2 = anchorTest2.getHrefAttribute();
		
		assertTrue("element includes no # ", href2.contains("#"));
	}	
	
	
	/**
	 * Tests if Anchor ("#") is also working with a given Suffix in Properties
	 */
	@Test
	public void test_AnchorWithSuffixId() throws FailingHttpStatusCodeException, IOException {
		
		// setup test
		setAdvSettings.setClassRewriteEnabled(true);		
		setAdvSettings.setIdRewriteEnabled(false);
		setAdvSettings.setNameRewriteEnabled(false);
		setAdvSettings.setClassRewriteForced(false);
		setAdvSettings.setClassSuffix(CLASS_SUFFIX);
		getPortalTool().execute(setAdvSettings);
		getPortalTool().logout();
			
		assertNotNull(getTestCasePage().getElementById("testAnchorDiv" + "_" + CLASS_SUFFIX ));

		HtmlAnchor anchorTest1 = (HtmlAnchor) getTestCasePage().getElementById("anchor1" + "_" + CLASS_SUFFIX);
		String href1 = anchorTest1.getHrefAttribute();
		assertTrue("element includes no suffix ", href1.contains("#" + ANCHOR_ID + "_" + CLASS_SUFFIX));

		HtmlAnchor anchorTest2 = (HtmlAnchor) getTestCasePage().getElementById("anchor2" + "_" + CLASS_SUFFIX);
		String href2 = anchorTest2.getHrefAttribute();
		assertTrue("element includes no suffix ", href2.contains("#" + ANCHOR_ID + "_" + CLASS_SUFFIX));
		
		HtmlElement anchor =  getTestCasePage().getElementById(ANCHOR_ID + "_" + CLASS_SUFFIX);
		assertNotNull("The anchor element must not be null!", anchor);
		
		// setback after test
		getPortalTool().login(null, getLogin(), getPassword());
		setAdvSettings = new GetSetAdvProxyPortletSettings();
		setAdvSettings.setPortletSite(getTestIndexPage().getUrl());
		setAdvSettings.setClassRewriteEnabled(false);		
		setAdvSettings.setIdRewriteEnabled(false);
		setAdvSettings.setNameRewriteEnabled(false);
		setAdvSettings.setClassRewriteForced(false);
		setAdvSettings.setClassSuffix("");
		getPortalTool().execute(setAdvSettings);

		getPortalTool().logout();
	}
	
	/**
	 * Tests if Anchor ("#") is working without a given Suffix in Properties (add Suffix to name-Attribute)
	 */
	@Test
	public void test_AnchorWithoutSuffixName() throws Exception {
		
		setAdvSettings.setIdRewriteEnabled(false);
		setAdvSettings.setClassRewriteEnabled(false);
		setAdvSettings.setClassRewriteForced(false);
		setAdvSettings.setNameRewriteEnabled(false);
		setAdvSettings.setClassSuffix("");
		getPortalTool().execute(setAdvSettings);
	
		assertNotNull(getTestCasePage().getElementById("testAnchorDiv"));

		HtmlAnchor anchorTestName1 = (HtmlAnchor) getTestCasePage().getElementById("anchor1name");
		String href1 = anchorTestName1.getHrefAttribute();
		
		assertTrue("element includes no #  ", href1.contains("#"));
		
		HtmlAnchor anchorTest2 = (HtmlAnchor) getTestCasePage().getElementById("anchor2name");
		String href2 = anchorTest2.getHrefAttribute();
		
		assertTrue("element includes no # ", href2.contains("#"));

	}	
	
	
	/**
	 * Tests if Anchor ("#") is also working with a given Suffix in Properties (add Suffix to name-Attribute)
	 */
	@Test
	public void test_AnchorWithSuffixName() throws FailingHttpStatusCodeException, IOException {
		
		// setup test
		setAdvSettings.setClassRewriteEnabled(false);		
		setAdvSettings.setIdRewriteEnabled(false);
		setAdvSettings.setClassRewriteForced(false);
		setAdvSettings.setNameRewriteEnabled(true);
		setAdvSettings.setClassSuffix(CLASS_SUFFIX);
		getPortalTool().execute(setAdvSettings);
			
		assertNotNull(getTestCasePage().getElementById("testAnchorDiv"));

		HtmlAnchor anchorTest1 = (HtmlAnchor) getTestCasePage().getElementById("anchor1name");
		String href1 = anchorTest1.getHrefAttribute();
		assertTrue("element includes no suffix ", href1.contains("#" + ANCHOR_NAME + "_" + CLASS_SUFFIX));

		HtmlAnchor anchorTest2 = (HtmlAnchor) getTestCasePage().getElementById("anchor2name");
		String href2 = anchorTest2.getHrefAttribute();
		assertTrue("element includes no suffix ", href2.contains("#" + ANCHOR_NAME + "_" + CLASS_SUFFIX));
		
		HtmlElement anchor =  getTestCasePage().getElementByName(ANCHOR_NAME + "_" + CLASS_SUFFIX);
		assertNotNull("The anchor element must not be null!", anchor);
		
	}
	
}