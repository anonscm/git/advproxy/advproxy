package de.tarent.advproxy.portlet;

import org.junit.runner.RunWith;

import de.tarent.advproxy.test.AbstractEncodingISO885915Test;
import de.tarent.advproxy.test.concurrency.Concurrency;
import de.tarent.advproxy.test.concurrency.ConcurrencyJunitRunner;

@RunWith(ConcurrencyJunitRunner.class)
@Concurrency(times = 200)
public class IT_EncodingISO885915 extends AbstractEncodingISO885915Test {
	public IT_EncodingISO885915() {
		super("UTF-8");
	}
}
