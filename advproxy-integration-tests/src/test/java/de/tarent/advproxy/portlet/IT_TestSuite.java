package de.tarent.advproxy.portlet;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;

import org.evolvis.advproxy.portalTools.GetSetAdvProxyPortletSettings;
import org.evolvis.liferay.lrPortalTools.PortalTool;
import org.evolvis.liferay.lrPortalTools.PortalToolFactory;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
		IT_GetTextAndPicture.class,
		IT_ResourcePostForm.class,
		IT_FileUpload.class,
		IT_UUIDCookie.class,
		IT_EncodingISO885915.class,
		IT_Cookie.class,
		IT_FormEncoding.class,
		IT_Method.class,
		IT_SessionUrl.class, 
		IT_JavaScript.class,
		IT_Break.class, 
		IT_HTMLElements.class, 
		IT_FileDownloads.class, 
		IT_Redirects.class, 
		IT_UserAgent.class
		})
public class IT_TestSuite {

	@BeforeClass
	@AfterClass
	public static void setUp() throws MalformedURLException {

		URL integrationSystem = new URL(System.getProperty("integrationSystem"));
		String proxiedBaseUrl = System.getProperty("proxiedBaseUrl");
		URL baseUrl = new URL(integrationSystem.getProtocol() + "://" + integrationSystem.getHost() + ":" + integrationSystem.getPort());
		String login = System.getProperty("login");
		String password = System.getProperty("password");
		PortalTool portalTool = PortalToolFactory.getInstance(baseUrl);
		String testHomepage = proxiedBaseUrl + "/advproxy-test-webapp/html/index";
		final Collection<String> allowedUrls = Arrays.asList(proxiedBaseUrl + "/.*", "http://.*.org/.*");
		final Collection<String> resourceUrls = Arrays.asList(".*\\.jpg", ".*/testResourcePostFormRequest", ".*/testFileDownloadForResourceUrl\\?action=download");	
		final Collection<String> defaultExcludedJavaScripts = Arrays.asList("");
		final String cookieName = "userUUID";
		
		portalTool.login(null, login, password);
		
		final GetSetAdvProxyPortletSettings setAdvSettings = new GetSetAdvProxyPortletSettings();
		setAdvSettings.setPortletSite(integrationSystem);
		setAdvSettings.setHomepage(testHomepage);
		setAdvSettings.setAllowedUrls(allowedUrls);
		setAdvSettings.setResourceUrls(resourceUrls);
		setAdvSettings.setGenerateAdditionalCookie(true);
		setAdvSettings.setEnableUserUUID(true);
		setAdvSettings.setCookieName(cookieName);
		
		setAdvSettings.setEnableJavaScript(true);
		setAdvSettings.setExcludedJavaScripts(defaultExcludedJavaScripts);
		
		setAdvSettings.setIdRewriteEnabled(false);
		setAdvSettings.setClassRewriteEnabled(false);
		setAdvSettings.setNameRewriteEnabled(false);
		setAdvSettings.setClassRewriteForced(false);
		setAdvSettings.setClassSuffix("");
		
		setAdvSettings.setUserAgent("");
		
		portalTool.execute(setAdvSettings);
		
		portalTool.logout();
	}
}
