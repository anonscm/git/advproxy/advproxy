package de.tarent.advproxy.testportlets.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Test form controller.
 */
@Controller
public class TestResourcePostFormController {

	/**
	 * Nothing
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/testResourcePostForm")
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) 
	{
		return "testResourcePostForm";
    }

	/**
	 * Set the request parameter to the request attributes.
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/testResourcePostFormRequest")
	public String handlePostForm(HttpServletRequest request, HttpServletResponse response) {
		
		String firstname = (String)request.getParameter("vornameInputField");
		String lastname = (String)request.getParameter("nameInputField");
		
		request.setAttribute("firstname", firstname);
		request.setAttribute("lastname", lastname);
		
		return "testResourcePostFormAnswer";
	}
}
