package de.tarent.advproxy.testportlets.controller;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Download test controller.
 */
@Controller
@RequestMapping
public class TestFileDownloadsController {
	/**
	 * Nothing.
	 * 
	 * @param request
	 * @param response
	 * @param modell
	 * @return
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	@RequestMapping("/testFileDownloads")
	public String handleFileDownloadsRequest(HttpServletRequest request, HttpServletResponse response, ModelMap modell) throws URISyntaxException, IOException
	{
		return "testFileDownloads";
    }

	/**
	 * Set the download information to the header.
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value={"/testFileDownload"}, params={"action=download"})
	public String provideTestPdfFile(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setHeader("Content-Disposition", "attachment;filename=test.pdf");
		request.getRequestDispatcher("/resources/test.pdf").forward(request, response);
		return null;
	}
	
	/**
	 * She the download information to the headers.
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value={"/testFileDownloadForResourceUrl"}, params = { "action=download" })
	public String provideTestPdfFileForResourceUrl(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setHeader("Content-Disposition", "attachment;filename=test.pdf");
		request.getRequestDispatcher("/resources/test.pdf").forward(request, response);
		return null;
	}
	

	/**
	 * Set the download information to the header.
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/testFileDownloadPost", method = RequestMethod.POST)
	public String provideTestPdfFilePost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition",	"attachment;filename=test.pdf");
		request.getRequestDispatcher("/resources/test.pdf").forward(request, response);
		return null;
	}
}
