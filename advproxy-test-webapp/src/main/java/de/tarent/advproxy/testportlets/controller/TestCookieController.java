package de.tarent.advproxy.testportlets.controller;

import java.io.IOException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Cookie test controller.
 */
@Controller
@RequestMapping(value={"/testCookie", "/showTestCookie"})
public class TestCookieController {
	
	public static final String TEST_COOKIE_NAME = "testCookie";
		
	/**
	 * Set a new cookie to the response.
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws IOException
	 * @throws FileUploadException
	 */
	@RequestMapping
	public String handleCookieRequest(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) throws IOException,
			FileUploadException {

		Cookie testCookie = new Cookie("testCookieName", "testCookieValue");

		response.addCookie(testCookie);

		return "testCookie";
	}
	
	/**
	 * Nothing.
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws IOException
	 * @throws FileUploadException
	 */
	@RequestMapping(value="/showTestCookie")
	public String showCookies(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) throws IOException,
			FileUploadException {

		return "showTestCookie";
	}
}
