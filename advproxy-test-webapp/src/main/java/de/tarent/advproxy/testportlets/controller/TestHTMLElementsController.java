package de.tarent.advproxy.testportlets.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Html test controller.
 */
@Controller
@RequestMapping("/testHTMLElements")
public class TestHTMLElementsController {

	/**
	 * Nothing.
	 * 
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping
	public String handleRequest(HttpServletResponse response)
		throws IOException
	{
		return "testHTMLElements";
    }

}
