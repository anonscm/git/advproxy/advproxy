package de.tarent.advproxy.testportlets.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/** Test controller */
@Controller
@RequestMapping("/index")
public class Default {
	/** Nothing */
	@RequestMapping
	public String defaultView() {
		return "index";
	}
}
