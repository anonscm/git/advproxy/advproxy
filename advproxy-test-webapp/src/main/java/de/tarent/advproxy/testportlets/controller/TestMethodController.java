package de.tarent.advproxy.testportlets.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Method controller test.
 */
@Controller
public class TestMethodController {
	/**
	 * Set the method to set request attribute.
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/testMethod")
	public String handleRequest(HttpServletRequest request,
			HttpServletResponse response) {
		String method = (String) request.getMethod();
		request.setAttribute("method", method);
		return "testMethod";
	}

	/**
	 * Set the request method to the response header.
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/testMethod.*")
	public String handlePostForm(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setHeader("RequestMethod",
				((String) request.getMethod()).toUpperCase());
		request.getRequestDispatcher("/img/bild.jpg")
				.forward(request, response);
		return null;
	}
}
