package de.tarent.advproxy.testportlets.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Test session url controller.
 */
@Controller
public class TestSessionUrlController {

	/**
	 * Nothing.
	 * @return
	 */
	@RequestMapping(value = "/testSessionUrl")
	public String handleTestSessionUrl() {
		return "testSessionUrl";
	}
	
	/**
	 * Set the request uri to the model.
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/testSessionUrlResult")
	public String handleTestSessionUrlResult(HttpServletRequest request, ModelMap model) {
		model.addAttribute("requestURI", request.getRequestURI());
		return "testSessionUrlResult";
	}
}
