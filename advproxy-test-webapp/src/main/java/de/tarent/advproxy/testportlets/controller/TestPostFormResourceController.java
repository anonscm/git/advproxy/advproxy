package de.tarent.advproxy.testportlets.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Post form test controller
 */
@Controller
@RequestMapping(value = { "testPostFormResource" })
public class TestPostFormResourceController {

	/**
	 * Nothing.
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping
	public String handleRequest(HttpServletRequest request,
			HttpServletResponse response) {
		return "testPostFormResource";
	}

	/**
	 * Nothing.
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/testPostFormResource")
	public String handlePostForm(HttpServletRequest request,
			HttpServletResponse response) {
		String param = (String) request.getParameter("testResourceUrl");
		return "success";
	}
}
