package de.tarent.advproxy.testportlets.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import de.tarent.advproxy.testportlets.util.ServletUtils;

/**
 * Controller for Redirect-Tests
 * 
 * @author Tino Rink
 * @author Sabine Höcher
 *
 */
@Controller
public class TestRedirectsController {
	
	
	private final String redirectTarget = "http://www.heise.de";
	
	/**
	 * Handler to view of Redirect TestCases
	 */
	@RequestMapping("/testRedirects" )
	public String handleRequest(HttpServletRequest request)
		throws IOException
	{
		request.setAttribute(redirectTarget, redirectTarget);
		return "testRedirects";
    }

	/**
	 * Handler for a Simple Get Redirect to an external page
	 */
	@RequestMapping("/testSimpleGETRedirectLink" )
	public void handleSimpleGetExternRequest(HttpServletResponse response)
		throws IOException
	{
		response.setStatus(HttpServletResponse.SC_FOUND);
		response.addHeader("Location", redirectTarget);
    }
	
	/**
	 * Handler for a Simple Get Redirect to an intern page
	 */
	@RequestMapping("/testSimpleInternGETRedirectPage" )
	public void handleSimpleInternGETRequest(HttpServletRequest request, HttpServletResponse response)
		throws IOException
	{
		response.setStatus(HttpServletResponse.SC_FOUND);
		response.addHeader("Location", ServletUtils.getBaseUrl(request) + "/html/testRedirectInternGoHere");
    }

	/**
	 * Handler for a Linked Get Redirect to an intern page
	 */
	@RequestMapping("/testLinkedGETRedirectPage" )
	public void handleLinkedInternGetRequest(HttpServletRequest request, HttpServletResponse response)
		throws IOException
	{
		response.setStatus(HttpServletResponse.SC_FOUND);
		//redirects to the page which is redirecting as well
		response.addHeader("Location", ServletUtils.getBaseUrl(request) + "/html/testSimpleInternGETRedirectPage");
    }
	
	/**
	 * Handler for intern post redirect
	 */
	@RequestMapping(method = RequestMethod.POST, value="/postForm")
	public void handlePostRequest(HttpServletRequest request, HttpServletResponse response)
		throws IOException
	{
		response.setStatus(HttpServletResponse.SC_FOUND);
		response.addHeader("Location", ServletUtils.getBaseUrl(request) + "/html/testRedirectInternGoHere");
    }
	
	/**
	 * Handler for intern post-get redirect 
	 */
	@RequestMapping(method = RequestMethod.POST, value="/linkedPostGetForm")
	public void handlePostGetRequest(HttpServletRequest request, HttpServletResponse response)
		throws IOException
	{
		response.setStatus(HttpServletResponse.SC_FOUND);
		response.addHeader("Location", ServletUtils.getBaseUrl(request) + "/html/testSimpleInternGETRedirectPage");
    }
	
	/**
	 * Page to go after a intern redirect
	 */
	@RequestMapping("/testRedirectInternGoHere" )
	public String handleGetRequest(HttpServletResponse response)
		throws IOException
	{
		return "testRedirectInternGoHere";
    }
}
