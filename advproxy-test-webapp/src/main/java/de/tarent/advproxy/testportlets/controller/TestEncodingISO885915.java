package de.tarent.advproxy.testportlets.controller;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Encoding test controller.
 */
@Controller
@RequestMapping("/testEncodingISO885915")
public class TestEncodingISO885915 {

	/**
	 * Nothing.
	 * 
	 * @param request
	 * @param response
	 * @param modell
	 * @return
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	@RequestMapping
	public String handleDownloadRequest(HttpServletRequest request, HttpServletResponse response, ModelMap modell) throws URISyntaxException, IOException
	{
		return "testEncodingISO885915";
    }
}
