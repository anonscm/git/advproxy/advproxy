package de.tarent.advproxy.testportlets.controller;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;

import javax.imageio.ImageIO;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Test request handler
 */
@Controller
@RequestMapping(value={"/testUUIDCookie", "/testUUIDCookieImageResource"})
public class TestUUIDCookieController {
	
	public static final String UUID_COOKIE_NAME = "userUUID";
	
	/**
	 * set cookie information (uuid) to model handler.
	 * 
	 * @param request
	 * @param model
	 * @return
	 * @throws IOException
	 * @throws FileUploadException
	 */
	@RequestMapping
	public String handleCookieRequest(HttpServletRequest request, ModelMap model)
		throws IOException, FileUploadException {
		Cookie uuidCookie = getUuidCookie(request);
		
		StringBuilder allRequestCookies = new StringBuilder();
		for (final Cookie curCookie : getCookies(request)) {
			append(curCookie, allRequestCookies).append("<br/>");
		}
		
		model.addAttribute("uuidCookie", uuidCookie != null);
		model.addAttribute("uuidCookieValue", uuidCookie != null ? uuidCookie.getValue() : "");
		model.addAttribute("sessionId", request.getSession().getId());
		model.addAttribute("allRequestCookies", allRequestCookies.toString());
		
		return "testUUIDCookie";
    }
	
	@RequestMapping(value="/testUUIDCookieImageResource")
	@ResponseBody
	public byte[] handleCookieRequest2(HttpServletRequest request, HttpServletResponse response)
		throws IOException {
		response.setDateHeader("Expires", 0); //prevents caching at the proxy server
		response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
		response.setHeader("Pragma", "no-cache"); //HTTP 1.0
		
		Cookie uuidCookie = getUuidCookie(request);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		BufferedImage image = new BufferedImage(740, 500, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2d = image.createGraphics();
		try {
			int lineHeight = 20;
			int x = 0;
			int y = 0;
			
			g2d.setColor(Color.WHITE);
			g2d.fillRect(0, 0, image.getWidth(), image.getHeight());
			g2d.setColor(Color.BLACK);
			
			g2d.drawString("userUUID cookie: " + (uuidCookie != null), x, y += lineHeight);
			g2d.drawString("userUUID value:" + (uuidCookie != null ? uuidCookie.getValue() : ""), x, y += lineHeight);
			g2d.drawString("sessionId:" + (request.getSession().getId()), x, y += lineHeight);
			g2d.drawString("allRequestCookies:", x, y += lineHeight);
			
			for (final Cookie curCookie : getCookies(request)) {
				g2d.drawString(toString(curCookie), x, y += lineHeight);
			}
			
			ImageIO.write(image, MediaType.IMAGE_PNG.getSubtype(), baos);
			return baos.toByteArray();
		} finally {
			g2d.dispose();
		}
	}
	
	/**
	 * Return the cookie with the name <code>userUUID</code> or null if the
	 * request is null, the request cookie array is null or the cookie is not
	 * found.
	 * 
	 * @param request
	 * @return
	 */
	private Cookie getUuidCookie(HttpServletRequest request) {
		if (request != null && request.getCookies() != null) {
			for (final Cookie curCookie : request.getCookies()) {
				if (UUID_COOKIE_NAME.equals(curCookie.getName())) {
					return curCookie;
				}
			}
		}
		return null;
	}
	
	/**
	 * Return a sorted list of cookies and never null, also if request or
	 * request cookie array is null.
	 * 
	 * @param request
	 * @return
	 */
	private Cookie[] getCookies(HttpServletRequest request) {
		if (request == null || request.getCookies() == null) {
			return new Cookie[0];
		}
		Cookie[] cookies = Arrays.copyOf(request.getCookies(), request.getCookies().length);
		Arrays.sort(cookies, new Comparator<Cookie>() {
			@Override
			public int compare(Cookie cookie1, Cookie cookie2) {
				return cookie1.getName().compareTo(cookie2.getName());
			}
		});
		return cookies;
	}
	
	private String toString(Cookie cookie) {
		StringBuilder buffer = new StringBuilder();
		append(cookie, buffer);
		return buffer.toString();
	}
	
	private StringBuilder append(Cookie cookie, StringBuilder buffer) {
		buffer.append(cookie.getName()).append("=").append(cookie.getValue());
		buffer.append("; path=").append(cookie.getPath());
		buffer.append("; domain=").append(cookie.getDomain());
		return buffer;
	}
}
