package de.tarent.advproxy.testportlets.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * JavaScript test controller.
 */
@Controller
public class TestJavaScriptController {
	/**
	 * Nothing.
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/testJavaScript")
	public String handleJavaScriptBody(HttpServletRequest request,
			HttpServletResponse response) {
		return "testJavaScript";
	}
}
