package de.tarent.advproxy.testportlets.common;

/**
 * Key value pair of field name and description.
 */
public class FileUploadInformation {
	/** Field name */
	private String fieldName;
	/** Description */
	private String description;
	
	/**
	 * @return field name
	 */
	public String getFieldName() {
		return fieldName;
	}
	
	/**
	 * Set field name
	 * @param fieldName
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	
	/**
	 * @return description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Set description
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}
