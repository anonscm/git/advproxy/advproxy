package de.tarent.advproxy.testportlets.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Test request handler to test user agent
 * @author christoph
 *
 */
@Controller
@RequestMapping("/testUsrAgent")
public class TestUserAgentController {
	
	/**
	 * Set the user agent to the model.
	 * 
	 * @param response
	 * @param request
	 * @param model
	 * @return
	 * @throws IOException
	 */
	@RequestMapping
	public String handleRequest(HttpServletResponse response, HttpServletRequest request, ModelMap model)
		throws IOException
	{
		String userAgent= request.getHeader("User-Agent");
		model.addAttribute("userAgent", userAgent);
		return "testUserAgent";
    }
	

}
