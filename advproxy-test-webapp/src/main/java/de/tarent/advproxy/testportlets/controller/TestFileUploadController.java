package de.tarent.advproxy.testportlets.controller;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.fileupload.FileUploadException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import de.tarent.advproxy.testportlets.common.Defs;
import de.tarent.advproxy.testportlets.common.FileUploadInformation;
import de.tarent.advproxy.testportlets.util.JustUploadedUtil;

/**
 * Fileupload test controller.
 */
@Controller
@RequestMapping("/testFileUpload")
public class TestFileUploadController {
	
	/**
	 * Return the file upload informations.
	 */
	@ModelAttribute(Defs.FILE_UPLOAD_INFORMATIONS)
	protected ArrayList<FileUploadInformation> getFileUploadInformations(
		@ModelAttribute(Defs.FILE_UPLOAD_INFORMATIONS) ArrayList<FileUploadInformation> fileUploadInformations)
	{
		if( null == fileUploadInformations ) {
			fileUploadInformations = new ArrayList<FileUploadInformation>();
		}
		return fileUploadInformations;
	}
	
	/**
	 * Nothing.
	 * 
	 * @return
	 * @throws IOException
	 * @throws FileUploadException
	 */
	@RequestMapping
	public String handleEmptyRequest()
		throws IOException, FileUploadException
	{
		return "testFileUpload";
    }
	
	/**
	 * Set the upload information to the model.
	 * 
	 * @param request
	 * @param modell
	 * @return
	 * @throws IOException
	 * @throws FileUploadException
	 */
	@RequestMapping(method = {RequestMethod.POST})
	public String handleUploadRequest(MultipartHttpServletRequest request, ModelMap model)
		throws IOException, FileUploadException
	{
		JustUploadedUtil.addJustUploadedMultipartInformations(request, model);
		return "testFileUpload";
    }

}
