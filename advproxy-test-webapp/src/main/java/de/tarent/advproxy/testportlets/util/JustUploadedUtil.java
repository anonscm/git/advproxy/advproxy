package de.tarent.advproxy.testportlets.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;

import javax.servlet.ServletRequest;

import org.springframework.ui.ModelMap;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import de.tarent.advproxy.testportlets.common.Defs;
import de.tarent.advproxy.testportlets.common.FileUploadInformation;

public class JustUploadedUtil {

	public static void addJustUploadedMultipartInformations(MultipartHttpServletRequest request, ModelMap modell) {
		ArrayList<FileUploadInformation> fileUploadInformations = new ArrayList<FileUploadInformation>();
		FileUploadInformation information;
		
		for( String key : request.getMultiFileMap().keySet() ) {
			if( request.getMultiFileMap().get(key).get(0).getOriginalFilename().trim().length() > 0 && request.getMultiFileMap().get(key).get(0).getSize() > 0 ) {
				information = new FileUploadInformation();
				information.setFieldName(key);
				information.setDescription("File, name: '" + request.getMultiFileMap().get(key).get(0).getOriginalFilename() + "', file size: '" + request.getMultiFileMap().get(key).get(0).getSize() + "'");
				fileUploadInformations.add(information);
			}
		}
		
		if( fileUploadInformations.size() > 0 ) {
			modell.addAttribute(Defs.FILE_UPLOAD_SUCCESSFUL, Boolean.TRUE);
		} else {
			modell.addAttribute(Defs.FILE_UPLOAD_SUCCESSFUL, Boolean.FALSE);
		}
		
		modell.addAttribute(Defs.FILE_UPLOAD_INFORMATIONS, fileUploadInformations);
		addJustUploadedInformations(request, modell);
	}
	
	@SuppressWarnings("unchecked")
	public static void addJustUploadedInformations(ServletRequest request, ModelMap modell) {
		ArrayList<FileUploadInformation> fileUploadInformations;
		
		if( null != modell.get(Defs.FILE_UPLOAD_INFORMATIONS) ) {
			fileUploadInformations = (ArrayList<FileUploadInformation>) modell.get(Defs.FILE_UPLOAD_INFORMATIONS);
		} else {
			fileUploadInformations = new ArrayList<FileUploadInformation>();
		}
		
		for(final Enumeration<String> e = request.getParameterNames(); e.hasMoreElements(); ) {
			final String key = e.nextElement();
			final String values = Arrays.toString(request.getParameterValues(key));
			final FileUploadInformation	information = new FileUploadInformation();
			information.setFieldName(key);
			information.setDescription("Simple form field, value: " + values);
			fileUploadInformations.add(information);
		}

		modell.addAttribute(Defs.FILE_UPLOAD_INFORMATIONS, fileUploadInformations);
	}
}
