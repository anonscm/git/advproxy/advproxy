package de.tarent.advproxy.testportlets.util;

import javax.servlet.http.HttpServletRequest;

public class ServletUtils {

	/**
	 * Returns the server base url (e.g. http://localhost:8888/mywebapp) for the given
	 * ServletRequest and ServletContext when the request was send to 'localhost:8888'
	 * and the 'ContextPath' of the WebApp is 'myweb'.
	 */
	public static String getBaseUrl(HttpServletRequest request) {
		return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath();
	}
}
