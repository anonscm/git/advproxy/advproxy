package de.tarent.advproxy.testportlets.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller for StatusCode >= 400 - Tests
 * 
 * @author Tino Rink
 * @author Sabine Höcher
 *
 */
@Controller
public class TestStatusCode400Controller {
	

	
	/**
	 * Handler to view of Redirect TestCases
	 */
	@RequestMapping("/testStatusCode400" )
	public String handleRequest(HttpServletRequest request)
		throws IOException
	{
		return "testStatusCode400";
    }

	/**
	 * Handler for a StatusCode 400
	 */
	@RequestMapping("/testStatusCode400Show" )
	public String handleSimpleGetExternRequest(HttpServletRequest request, HttpServletResponse response)
		throws IOException
	{
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		return "testStatusCode400Show";
    }


}
