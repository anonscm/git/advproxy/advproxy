package de.tarent.advproxy.testportlets.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Test form encoding.
 */
@Controller
public class TestXwwwFormEncodedController {
	/**
	 * static request handler.
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 * @throws FileUploadException
	 */
	@RequestMapping(value="/testFormEncoded")
	public String handleRequest(HttpServletRequest request)
		throws IOException, FileUploadException
	{
		return "testFormEncoded";
    }
	
	/**
	 * show encoding request handler.
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 * @throws FileUploadException
	 */
	@RequestMapping(value = "/testFormEncodedShow")
	public String showEncoding(HttpServletRequest request, HttpServletResponse response)
		throws IOException, FileUploadException
	{
		
		String firstname = (String)request.getParameter("vornameInputField");
		String lastname = (String)request.getParameter("nameInputField");
		
		request.setAttribute("firstname", firstname);
		request.setAttribute("lastname", lastname);
		request.setAttribute("encrypteRequest", request.getContentType());
		
		return "testFormEncodedShow";
    }
	

}
