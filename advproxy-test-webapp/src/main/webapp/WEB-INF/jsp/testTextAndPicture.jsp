<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" isThreadSafe="true" />

<html>
	<head>
		<title>Test Simple Text and Image</title>		
	</head>
	<body>
	
	<div id="testTextAndPicture">
		<div>Hier ist Text und darunter ist ein Bild.</div>
		<div>
			<c:url var="imgUrl" value="/img/bild.jpg" />
			<img id="testImage" src="${imgUrl}" width="300" height="300" border="1"
				style="width: 300px; height: 300px; border: 1px solid black;" />
			
			<c:url var="imgUrl2" value="/img/bildNotExisting.jpg" />
			<img id="testImage2" src="${imgUrl2}" width="300" height="300" border="1"
				style="width: 300px; height: 300px; border: 1px solid black;" />
		</div>
	</div>
	</body>
</html>

</jsp:root>
