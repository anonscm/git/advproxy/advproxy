<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" isThreadSafe="true" />

<html>
	<head>
		<title>Test Intern Redirect</title>
	</head>
	<body>
		<div id="testInternRedirectText">Herzlichen Glückwunsch, Sie wurden erfolgreich auf einen interne Seite weitergeleitet.</div>
	</body>
</html>
</jsp:root>