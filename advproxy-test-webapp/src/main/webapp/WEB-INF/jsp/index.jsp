<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" isThreadSafe="true" />

<html>
	<head>
		<title>Test Overview</title>
	</head>
	<body>
		<div id="advProxyTests">
			<c:url var="testFileUpload" value="/html/testFileUpload" />
			<a id="testFileUpload" href="${testFileUpload}">testFileUpload</a>
			<br />
			<c:url var="testUUIDCookie" value="/html/testUUIDCookie" />
			<a id="testUUIDCookie" href="${testUUIDCookie}">testUUIDCookie</a>
			<br />
			<c:url var="testTextAndPicture" value="/html/testTextAndPicture" />
			<a id="testTextAndPicture" href="${testTextAndPicture}">testTextAndPicture</a>
	        <br />
			<c:url var="testPostFormResource" value="/html/testPostFormResource" />
			<a id="testPostFormResource" href="${testPostFormResource}">testPostFormResource</a>
			<br />
			<c:url var="testResourcePostForm" value="/html/testResourcePostForm" />
			<a id="testResourcePostForm" href="${testResourcePostForm}">testResourcePostForm</a>
			<br />
			<c:url var="testEncodingISO885915" value="/html/testEncodingISO885915" />
			<a id="testEncodingISO885915" href="${testEncodingISO885915}">testEncodingISO885915</a>
			<br />
			<c:url var="testCookie" value="/html/testCookie" />
			<a id="testCookie" href="${testCookie}">testCookie</a>
			<br />
			<c:url var="testFormEncoded" value="/html/testFormEncoded" />
			<a id="testFormEncoded" href="${testFormEncoded}">testFormEncoded</a>
			<br />
			<c:url var="testMethod" value="/html/testMethod" />
			<a id="testMethod" href="${testMethod}">testMethod</a>
			<br />
			<c:url var="testSessionUrl" value="/html/testSessionUrl" />
			<a id="testSessionUrl" href="${testSessionUrl}">testSessionUrl</a>
			<br />
			<c:url var="testJavaScript" value="/html/testJavaScript" />
			<a id="testJavaScript" href="${testJavaScript}">testJavaScript</a>
			<br />
			<c:url var="testBreak" value="/html/testBreak" />
			<a id="testBreak" href="${testBreak}">testBreak</a>
			<br />
			<c:url var="testHTMLElements" value="/html/testHTMLElements" />
			<a id="testHTMLElements" href="${testHTMLElements}">testHTMLElements</a> (Anchor and MailTo)
			<br />
			<c:url var="testFileDownloads" value="/html/testFileDownloads" />
	        <a id="testFileDownloads" href="${testFileDownloads}">testFileDownloads</a>
	        <br />
			<c:url var="testRedirects" value="/html/testRedirects" />
	        <a id="testRedirects" href="${testRedirects}">testRedirects</a>
	        <br />
	        <c:url var="testStatusCode400" value="/html/testStatusCode400" />
	        <a id="testStatusCode400" href="${testStatusCode400}">testStatusCode400</a>
	        <br />
	        <c:url var="testUsrAgent" value="/html/testUsrAgent" />
	        <a id="testUsrAgent" href="${testUsrAgent}">testUsrAgent</a>
	        <br />
		</div>
	</body>
</html>

</jsp:root>
