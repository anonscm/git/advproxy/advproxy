<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" isThreadSafe="true" />

<html>
	<head>
		<title>Test Cookie</title>
	</head>
	<body>
		<div id="testCookie">
			Cookies wurden gesetzt.  
			<c:url var="showTestCookie" value="/html/showTestCookie" />
			<a id="showTestCookie" href="${showTestCookie}">Hier</a> werden sie angezeigt.
		</div>
	</body>
</html>

</jsp:root>