<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" isThreadSafe="true" />

<html>
	<head>
		<title>Test FileUpload</title>
	</head>
	<body>
	
    <c:if test="${fileUploadSuccessful}">
        <div name="uploadSuccessful" class="portlet-msg-success">File successfully uploaded.</div>
    </c:if>
    <p>Informations about just uploaded form:</p>
    <table border="1">
        <thead>
            <tr>
                <th align="center" width="30%">Form field name</th>
                <th align="center">Description</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${fileUploadInformations}" var="currentInfo" varStatus="infoIndex">
                <tr>
                    <td>${currentInfo.fieldName}</td>
                    <td name="uploaded_${currentInfo.fieldName}">${currentInfo.description}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
 
	</body>
</html>
   
</jsp:root>
    