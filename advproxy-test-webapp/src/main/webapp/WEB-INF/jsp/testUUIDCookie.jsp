<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" isThreadSafe="true" />

<html>
	<head>
		<title>Test UuidCookie</title>
	</head>
	<body>
		<div id="testUuidCookie">
	    	<div>userUUID cookie: <span id="hasUuidCookie">${uuidCookie}</span></div>
	    	<div>userUUID value: <span id="uuidCookieValue">${uuidCookieValue}</span></div>
	    	<div>sessionId: <span id="uuidCookieValue">${sessionId}</span></div>
	    	<div>
	    		allRequestCookies:<br/>
	    		<span id="allRequestCookies">${allRequestCookies}</span>
	    	</div>
	    	<div>
		    	<br/>
		    	and now as request url (rendered image):<br/>
		    	
	    		<!-- not absolute url here, we want call the controller again! -->
				<c:url var="imgUrl" value="testUUIDCookieImageResource" />
				<img id="testImage" src="${imgUrl}" width="740" height="500" border="1"
					style="width: 740px; height: 500px; border: 1px solid black;" />
				<br/><br/>
				img url: ${imgUrl}
	    	</div>
		</div>
	</body>
</html>

</jsp:root>