<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" isThreadSafe="true" />

<html>
	<head>
		<title>Test Session Url</title>
	</head>
	<body>
		<div id="testSessionUrl">
		
			<p>
				Diese Testseite enthält einen Link und ein Formular mit einer fiktive Session-Information in der URL. 
				Die Session-Information ist korrekt zu encodieren und darf dabei nicht verloren gehen.
			</p> 
			
			<c:url var="testSessionUrlResult" value="/html/testSessionUrlResult" />
			
			<a id="testSessionUrlLink" href="${testSessionUrlResult};kfmsession=qayxsw1234edcvfr">testLink</a>
			
			<form id="testSessionUrlForm" action="${testSessionUrlResult};kfmsession=qayxsw1234edcvfr" >
				<input id="testSessionUrlFormButton" name="submitButton" value="submit" type="submit" />
			</form>
			
		</div>
	</body>
</html>

</jsp:root>