<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Test Break</title>		
	</head>
	<body>
	
	Tests the different kinds of empty tags
	
	<div id="testBreakDiv">
		Test &#60;br&#62;
   		<br>	
    	Test &#60;br/&#62;
		<br/>
		Test &#60;br /&#62;
		<br />
		Test &#60;br&#62;&#60;br/&#62;
		<br></br>
	</div>
	</body>
</html>
