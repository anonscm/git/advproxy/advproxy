<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" isThreadSafe="true" />

<html>
    <head>
        <title>testFileDownloads</title>
    </head>
    <body>
    
    <h1>Test File Download</h1>
    	<div id="testFileDownload">
	        <c:url var="downloadFile" value="/html/testFileDownload">
	        	<c:param name="action" value="download" />
	        </c:url>
	        <a id="downloadFile" href="${downloadFile}">Click here to download a pdf test file.</a>
        </div>
        
        
	<h1>Test File Download for ResourceUrl</h1>
     	<div id="testFileDownloadForResourceUrl">
	        <c:url var="downloadFileForResourceUrl" value="/html/testFileDownloadForResourceUrl">
	        	<c:param name="action" value="download" />
	        </c:url>
	        <a id="downloadFileForResourceUrl" href="${downloadFileForResourceUrl}">Click here to download a pdf test file.</a>
        </div>      
       
       
      <h1>Test File Download Post</h1>     
      
          Press the Button an try to download a file.
    	
    	<c:url var="formUrl" value="/html/testFileDownloadPost" />
		<form action="${formUrl}" method="post" enctype="multipart/form-data">
	    	<div>
				<input id="downloadPostSubmitButton" name="submitButton" value="postDownload" type="submit"></input>
			</div>
    	</form>
        
    </body>
</html>

</jsp:root>