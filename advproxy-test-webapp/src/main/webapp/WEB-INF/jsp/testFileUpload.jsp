<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" isThreadSafe="true" />

<html>
	<head>
		<title>Test FileUpload</title>
	</head>
	<body>
		<div id="testFileUpload">
			<c:if test="${fn:length(fileUploadInformations) > 0}">
			   <jsp:include page="justUploaded.jsp" />
			</c:if>
			<c:url var="formUrl" value="/html/testFileUpload" />
			<form action="${formUrl}" method="post" enctype="multipart/form-data" accept-charset="ISO-8859-1" name="formular">
			    <div>
			       <label for="nameInputField">Name: </label>
			       <input id="nameInputField" name="nameInputField"></input>
			    </div>
			    <div>
		           <label for="vornameInputField">Vorname: </label>
		           <input id="vornameInputField" name="vornameInputField"></input>
			    </div>
				<div>
				  <label for="dateiFileInput">Wählen Sie eine Datei von Ihrem Rechner aus: </label>
				  <input id="dateiFileInput" name="datei" type="file" size="50"></input>
				</div>
				<div>
					<label for="beschreibungTextArea">Beschreibung:</label>
					<textarea id="beschreibungTextArea" name="beschreibungTextArea"
						cols="50" rows="10">Geben Sie hier Text ein ...</textarea>
				</div>
				<div>
					<input id="fileUploadSubmitButton" name="submitButton" value="Upload file" type="submit"></input>
				</div>
			</form>
		</div>
		
		<div id="testAcceptCharsetInForm">
			<form action="dummy" method="get" name="testFormNoCharset">
				Just a little useless form
			</form>
		</div>
	</body>
</html>

</jsp:root>