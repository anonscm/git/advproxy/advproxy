<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" isThreadSafe="true" />

<html>
	<head>
		<title>Test HTMLElements (Anchor and MailTo)</title>		
	</head>
	<body>
	
	<h1>Test Anchor</h1>
	
	<div id="testAnchorDiv">
		<h1 id="beginning">Anfang</h1>

			Lorem ipsum dolor sit amet, 
			consectetuer adipiscing elit. 
			Nam cursus. Morbi ut mi. 
			Nullam enim leo, egestas 
			id, condimentum at, laoreet 
			mattis, massa. Sed eleifend 
			nonummy diam. Praesent mauris ante, 
			elementum et, bibendum at, posuere 
			sit amet, nibh. Duis tincidunt lectus 
			quis dui viverra vestibulum. 
			Suspendisse vulputate aliquam dui. 
			Nulla elementum dui ut augue. 
			Aliquam vehicula mi at mauris. 
			Maecenas placerat, nisl at consequat 
			rhoncus, sem nunc gravida justo, quis 
			eleifend arcu velit quis lacus. 
			Morbi magna magna, tincidunt a, mattis non, 
			imperdiet vitae, tellus. Sed odio est, 
			auctor ac, sollicitudin in, consequat vitae, 
			orci. Fusce id felis. Vivamus sollicitudin 
			metus eget eros.
			
		<p>
			<a id="anchor1" href="#beginning">Seitenanfang - Test1</a>
		</p>
			
		<p>
			<c:url var="testAnchor" value="/html/testHTMLElements" />
			<a id="anchor2" href="${testAnchor}#beginning">Seitenanfang - Test2</a>
		</p>
		
		<h1> Test für Anchor-Name </h1>
		
		<a name="beginningAnchorName">Anfangslink</a>
			Lorem ipsum dolor sit amet, 
			consectetuer adipiscing elit. 
			Nam cursus. Morbi ut mi. 
			Nullam enim leo, egestas 
			id, condimentum at, laoreet 
			mattis, massa. Sed eleifend 
			nonummy diam. Praesent mauris ante, 
			elementum et, bibendum at, posuere 
			sit amet, nibh. Duis tincidunt lectus 
			quis dui viverra vestibulum. 
			Suspendisse vulputate aliquam dui. 
			Nulla elementum dui ut augue. 
			Aliquam vehicula mi at mauris. 
			Maecenas placerat, nisl at consequat 
			rhoncus, sem nunc gravida justo, quis 
			eleifend arcu velit quis lacus. 
			Morbi magna magna, tincidunt a, mattis non, 
			imperdiet vitae, tellus. Sed odio est, 
			auctor ac, sollicitudin in, consequat vitae, 
			orci. Fusce id felis. Vivamus sollicitudin 
			metus eget eros.
			
		
		<p>
			<a id="anchor1name" href="#beginningAnchorName">Anfangslink - Test1</a>
		</p>
			
		<p>
			<c:url var="testAnchorName" value="/html/testHTMLElements" />
			<a id="anchor2name" href="${testAnchorName}#beginningAnchorName">Anfangslink - Test2</a>
		</p>
	</div>
	
	
	<h1> Test MailTo </h1>	

	<div id="testMailDiv">
		<div>Hier soll man eine Mail versenden können.</div>
		<div>
			This is an email link:
			<a id="testMailLink" href="mailto:someone@example.com?Subject=Hello%20again">
				Send Mail</a>
		</div>
	</div>
	</body>
</html>

</jsp:root>
