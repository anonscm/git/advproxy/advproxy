<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" isThreadSafe="true" />

<html>
	<head>
		<title>Test Javascript</title>	

		<script id ="headFileInclude" charset="UTF-8" type="text/javascript" src="../resources/jsSnippetOfHead.js"> ; </script>

		<script id="headSnippetInclude" charset="UTF-8" type="text/javascript">
			function show_text_head() {
			document.write("HEAD-included JavaScript-Snippet successfully!");
			}; 
		</script>

	</head>
	<body>
	<div id="bodyDiv">
		<div id="beginDiv">
			Hier ist der Anfang der Webapplication.
		</div>
	
			<div id="testJavaScript">
	 			<script id="bodySnippetInclude" charset="UTF-8" type="text/javascript">
					function show_text_body() {
					document.write("BODY-included JavaScript-Snippet successfully!");
				}; 
				</script>
			</div>
		
			<div id="textDiv">
				This is text! <br/>
			</div>

			<div id="bodyFileDiv">			
				<script id="bodyFileInclude" charset="UTF-8" type="text/javascript" src="../resources/jsSnippetOfBody.js"> ; </script>
			</div>
		
			<div id="endDiv">		
				Hier ist das Ende der Webapplication, danach sollte nichts mehr stehen.<br/>
			</div>
		</div>
	</body>
</html>

</jsp:root>