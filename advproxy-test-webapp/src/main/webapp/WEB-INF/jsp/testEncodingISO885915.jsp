<?xml version="1.0" encoding="ISO-8859-15" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
>
<jsp:directive.page language="java" contentType="text/html; charset=ISO-8859-15"
    pageEncoding="ISO-8859-15" isELIgnored="false" isThreadSafe="true" />

<html>
	<head>
		<title>Test Encoding ISO8859-15</title>
	</head>
	<body>
		<div id="testEncodingISO885915">
	    	<span id="testString"><![CDATA[!"�$%&/()=?�*��'@�<>]]></span>
		</div>
	</body>
</html>

</jsp:root>