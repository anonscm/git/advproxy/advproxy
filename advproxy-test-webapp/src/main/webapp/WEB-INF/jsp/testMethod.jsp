<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" isThreadSafe="true" />

<html>
	<head>
		<title>Test Method</title>
	</head>
	<body>
		<div id="testMethodUrl">
		
			<c:url var="testUrl" value="/html" />
			
			<img id="testImgUrl" src="${testUrl}/testMethod.jpg" width="30" height="30" />
			
			<a id="testLinkUrl" href="${testUrl}/testMethod.jpg">testImgUrl</a>
			
			<script  id="testJsUrl" src="${testUrl}/testMethod.js" type="text/javascript"></script>

		</div>
		
	</body>
</html>

</jsp:root>