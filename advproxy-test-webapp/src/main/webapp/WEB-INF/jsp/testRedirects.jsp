<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" isThreadSafe="true" />

<html>
	<head>
		<title>Redirect Tests</title>		
	</head>
	<body>
	
	<h2>Simple Redirects</h2>	
	
		<h3>Test Simple Intern Get Redirect</h3>
		Wenn Sie auf diesen Link klicken werden Sie automatisch auf eine interne Seite weiter geleitet:
		
		<div id="simpleInternGet">
			<c:url var="testSimpleInternGETRedirectPage" value="/html/testSimpleInternGETRedirectPage" />
			<a id="testSimpleInternGETRedirectPage" href="${testSimpleInternGETRedirectPage}">testSimpleInternGETRedirectPage</a>
		</div>
		
		<h3>Test Simple Intern Post Redirect</h3>
		Hier klicken führt zu einem redirect auf eine interne Seite:
	
		<form action="postForm" method="post">
			<div id="simpleInternPost">
				<input id="internPostFormSubmitButton" name="submitButton" value="post form" type="submit"></input>
			</div>
		</form>
		
		<h3>Test Simple Extern Get Redirect</h3>
		Wenn Sie auf diesen Link klicken werden Sie automatisch nach ${redirectTarget} weitergeleitet:

		<div id="simpleExternGet">
			<c:url var="testSimpleGETRedirectLink" value="/html/testSimpleGETRedirectLink" />
			<a id="testSimpleGETRedirectLink" href="${testSimpleGETRedirectLink}">do_redirect</a>
		</div>	
		
		
	<h2>Linked Redirects</h2>	
		
		<h3>Test Linked Get Redirect</h3>

		Wenn Sie auf diesen Link klicken werden Sie automatisch auf eine interne Seite weitergeleitet:
		
		<div id="linkedInternGet">
			<c:url var="testLinkedGETRedirectPage" value="/html/testLinkedGETRedirectPage" />
			<a id="testLinkedGETRedirectPage" href="${testLinkedGETRedirectPage}">testLinkedGETRedirectPage</a>
		</div>
		
		<h3>Test Post -> Get Intern Redirect</h3>		

		Hier klicken führt zu einem verketteten redirect auf eine interne Seite:
		
		<form action="linkedPostGetForm" method="post">
			<div>
				<input id="postGetFormSubmitButton" name="submitButton" value="post form" type="submit"></input>
			</div>
		</form>
	</body>
</html>

</jsp:root>
