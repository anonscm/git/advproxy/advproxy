<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" isThreadSafe="true" />

<html>
	<head>
		<title>Test Content Type "application/x-www-form-urlencoded" </title>
	</head>
	<body>
		<div id="testFormEncoded">
			<c:url var="formUrl" value="/html/testFormEncodedShow" >
			</c:url>
			<form action="${formUrl}" method="post" enctype="application/x-www-form-urlencoded">
			    <div>
			       <label for="nameInputField">Name: </label>
			       <input id="nameInputField" name="nameInputField"></input>
			    </div>
			    <div>
		           <label for="vornameInputField">Vorname: </label>
		           <input id="vornameInputField" name="vornameInputField"></input>
			    </div>
				<div>
					<input id="formSubmitButton" name="submitButton" value="Submit" type="submit"></input>
				</div>
			</form>
		</div>
	</body>
</html>
</jsp:root>