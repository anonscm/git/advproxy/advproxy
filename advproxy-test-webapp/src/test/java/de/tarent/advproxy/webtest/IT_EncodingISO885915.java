package de.tarent.advproxy.webtest;

import de.tarent.advproxy.test.AbstractEncodingISO885915Test;

public class IT_EncodingISO885915 extends AbstractEncodingISO885915Test {
	public IT_EncodingISO885915() {
		super("ISO-8859-15");
	}
}
