package de.tarent.advproxy.webtest;

import static org.junit.Assert.*;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.junit.Test;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;

import de.tarent.advproxy.test.AbstractRedirectsTest;

public class IT_Redirects extends AbstractRedirectsTest{
	
	/**
	 * Tests a simple redirect with a get-request which calls an intern page  
	 */
	@Test
	public void testSimpleGetRedirectWithHttpStatusCodeNegative() throws FailingHttpStatusCodeException, IOException{
		getTestCasePage().getWebClient().setRedirectEnabled(false);

		try {
			getTestCasePage().getElementById("testSimpleInternGETRedirectPage").click();
			fail("Expected exception");
		} catch (FailingHttpStatusCodeException e) {
			assertEquals(HttpServletResponse.SC_FOUND, e.getStatusCode());
		}
		
	}
	
	/**
	 * Tests a linked redirect with a get-request which calls an intern page  
	 */
	@Test
	public void testLinkedGetRedirectWithHttpStatusCodeNegative() throws FailingHttpStatusCodeException, IOException {

		getTestCasePage().getWebClient().setRedirectEnabled(false);

		try {
			getTestCasePage().getElementById("testLinkedGETRedirectPage").click();
			fail("Expected exception");
		} catch (FailingHttpStatusCodeException e) {
			assertEquals(HttpServletResponse.SC_FOUND, e.getStatusCode());
		}
	}
	
	/**
	 * Tests a linked redirect with a post-get-request-combination which calls an intern page  (POST -> GET)
	 */
	@Test
	public void testLinkedPostGetRedirectWithHttpStatusCodeNegative() throws FailingHttpStatusCodeException, IOException {
		getTestCasePage().getWebClient().setRedirectEnabled(false);
		
		try {		
			getTestCasePage().getElementById("postGetFormSubmitButton").click();
			fail("Expected exception");
		} catch (FailingHttpStatusCodeException e) {
			assertEquals(HttpServletResponse.SC_FOUND, e.getStatusCode());
		}
	}
	
	/**
	 * Tests a simple redirect with a post-request which calls an intern page  
	 */
	@Test
	public void testSimplePostRedirectWithHttpStatusCodeNegative() throws FailingHttpStatusCodeException, IOException{
		getTestCasePage().getWebClient().setRedirectEnabled(false);
		try {
			getTestCasePage().getElementById("internPostFormSubmitButton").click();
			fail("Expected exception");
		} catch (FailingHttpStatusCodeException e) {
			assertEquals(HttpServletResponse.SC_FOUND, e.getStatusCode());
		}
	}

}
