package de.tarent.advproxy.webtest;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import de.tarent.advproxy.test.AbstractFileUploadTest;

public class IT_FileUpload extends AbstractFileUploadTest {
	
	@Test
	/**
	 * tests if the accepted-charset is set to ISO-8859-1 in the formular
	 */
	public void test_acceptedCharset() throws FailingHttpStatusCodeException, IOException {
		
		HtmlPage fileUploadPage = getTestCasePage();

		// checks if the right accepted-charset is set in a form with a accept-charset
		HtmlForm form = fileUploadPage.getFormByName("formular");
		String acceptCharset = form.getAttribute("accept-charset");
		assertEquals("ISO-8859-1", acceptCharset);
		
	}
	
}
